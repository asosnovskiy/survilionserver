-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1:3306
-- Generation Time: Dec 06, 2013 at 02:31 AM
-- Server version: 5.5.27-log
-- PHP Version: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `survilion`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `social_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `vk_id` (`social_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `social_id`) VALUES
(2, 4353466),
(1, 629791041),
(4, 1987884545);

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE IF NOT EXISTS `characters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `acc_id` bigint(20) unsigned NOT NULL,
  `name` varchar(64) NOT NULL DEFAULT '',
  `sex` bit(1) NOT NULL DEFAULT b'0',
  `money` int(10) unsigned NOT NULL DEFAULT '0',
  `island_id` bigint(20) unsigned NOT NULL,
  `selected` tinyint(1) NOT NULL DEFAULT '0',
  `hp` float NOT NULL DEFAULT '1',
  `hunger` float NOT NULL DEFAULT '1',
  `drought` float NOT NULL DEFAULT '1',
  `tiredness` float NOT NULL DEFAULT '1',
  `pos_x` float NOT NULL,
  `pos_y` float NOT NULL,
  `pos_z` float NOT NULL,
  `rot_y` float NOT NULL,
  `used_item` int(11) NOT NULL DEFAULT '0',
  `inventory` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `acc_id` (`acc_id`),
  KEY `island_id` (`island_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `acc_id`, `name`, `sex`, `money`, `island_id`, `selected`, `hp`, `hunger`, `drought`, `tiredness`, `pos_x`, `pos_y`, `pos_z`, `rot_y`, `used_item`, `inventory`) VALUES
(6, 1, 'masha', b'0', 1045, 1, 1, 1, 0.527007, 0.527007, 0.527007, 329.554, 44.0056, 362.261, 295.834, 0, '{}'),
(8, 4, 'afrokick', b'0', 1005, 5, 1, 0, 0, 0, 0, 399.062, 46.7357, 359.943, 19.1974, 0, '{}'),
(9, 1, 'denzar', b'0', 1001, 7, 0, 0, 0, 0, 0, 367.981, 46.9889, 360.586, 92.0702, 0, '{}');

-- --------------------------------------------------------

--
-- Table structure for table `islands`
--

CREATE TABLE IF NOT EXISTS `islands` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `islands`
--

INSERT INTO `islands` (`id`, `name`, `data`) VALUES
(1, 'My Island', '{}'),
(2, 'Island of name masha', ''),
(3, 'Island of name masha', ''),
(4, 'Island of name masha', ''),
(5, 'Island of name afrokick', ''),
(6, 'Island of name afrokick', ''),
(7, 'Island of name denzar', '');

-- --------------------------------------------------------

--
-- Table structure for table `items_configurations`
--

CREATE TABLE IF NOT EXISTS `items_configurations` (
  `type` int(11) NOT NULL,
  `multicount` tinyint(1) NOT NULL,
  `dur` int(11) NOT NULL,
  `mindmg` float NOT NULL,
  `maxdmg` float NOT NULL,
  `speed` int(11) NOT NULL,
  `dist` float NOT NULL,
  `weight` int(11) NOT NULL,
  `reghp` float NOT NULL,
  `regtiredness` float NOT NULL,
  `reghunger` float NOT NULL,
  `regdrought` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mining_configurations`
--

CREATE TABLE IF NOT EXISTS `mining_configurations` (
  `type` int(11) NOT NULL,
  `allow_instruments` text NOT NULL,
  `drop` text NOT NULL,
  PRIMARY KEY (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mining_configurations`
--

INSERT INTO `mining_configurations` (`type`, `allow_instruments`, `drop`) VALUES
(2000, '{"ids":[804,809,899]}', '{"drop":[{"type":1,"chance":3000,"maxdropcount":3,"maxcount":6,"infinity":0,"fruit":0},{"type":2,"chance":3000,"maxdropcount":3,"maxcount":4,"infinity":0,"fruit":0},{"type":3,"chance":4000,"maxdropcount":2,"maxcount":7,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0},{"type":301,"chance":10000,"maxdropcount":3,"maxcount":3,"infinity":0,"fruit":1}]}'),
(2001, '{"ids":[]}', '{"drop":[{"type":5,"chance":2400,"maxdropcount":4,"maxcount":12,"infinity":0,"fruit":0},{"type":2,"chance":2500,"maxdropcount":3,"maxcount":10,"infinity":0,"fruit":0},{"type":3,"chance":50000,"maxdropcount":3,"maxcount":10,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":6,"maxcount":6,"infinity":0,"fruit":0},{"type":405,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2002, '{"ids":[804,809,899]}', '{"drop":[{"type":16,"chance":10000,"maxdropcount":2,"maxcount":5,"infinity":0,"fruit":0}]}'),
(2003, '{"ids":[804,809,899]}', '{"drop":[{"type":1,"chance":6000,"maxdropcount":3,"maxcount":6,"infinity":0,"fruit":0},{"type":3,"chance":4000,"maxdropcount":2,"maxcount":5,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2004, '{"ids":[]}', '{"drop":[{"type":1,"chance":6000,"maxdropcount":10,"maxcount":30,"infinity":0,"fruit":0},{"type":2,"chance":5000,"maxdropcount":10,"maxcount":20,"infinity":0,"fruit":0},{"type":3,"chance":4000,"maxdropcount":5,"maxcount":20,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":2,"maxcount":5,"infinity":0,"fruit":0}]}'),
(2005, '{"ids":[899]}', '{"drop":[{"type":1,"chance":9900,"maxdropcount":3,"maxcount":5,"infinity":0,"fruit":0},{"type":405,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2006, '{"ids":[899]}', '{"drop":[{"type":1,"chance":9900,"maxdropcount":3,"maxcount":7,"infinity":0,"fruit":0},{"type":406,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2007, '{"ids":[800,807,808,801]}', '{"drop":[{"type":6,"chance":6500,"maxdropcount":2,"maxcount":30,"infinity":0,"fruit":0},{"type":7,"chance":3000,"maxdropcount":3,"maxcount":20,"infinity":0,"fruit":0},{"type":408,"chance":0,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0},{"type":8,"chance":500,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0}]}'),
(2008, '{"ids":[]}', '{"drop":[]}'),
(2009, '{"ids":[]}', '{"drop":[]}'),
(2011, '{"ids":[804,809,899]}', '{"drop":[{"type":1,"chance":5000,"maxdropcount":4,"maxcount":15,"infinity":0,"fruit":0},{"type":405,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0},{"type":3,"chance":4900,"maxdropcount":4,"maxcount":10,"infinity":0,"fruit":0}]}'),
(2012, '{"ids":[804,809,899]}', '{"drop":[{"type":1,"chance":3400,"maxdropcount":5,"maxcount":20,"infinity":0,"fruit":0},{"type":2,"chance":2500,"maxdropcount":3,"maxcount":10,"infinity":0,"fruit":0},{"type":3,"chance":4000,"maxdropcount":4,"maxcount":12,"infinity":0,"fruit":0},{"type":405,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":1,"maxcount":5,"infinity":0,"fruit":0}]}'),
(2013, '{"ids":[804,809,899]}', '{"drop":[{"type":405,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0},{"type":2,"chance":2900,"maxdropcount":3,"maxcount":8,"infinity":0,"fruit":0},{"type":3,"chance":7000,"maxdropcount":5,"maxcount":20,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0}]}'),
(2014, '{"ids":[]}', '{"drop":[]}'),
(2015, '{"ids":[899]}', '{"drop":[{"type":1,"chance":9900,"maxdropcount":3,"maxcount":3,"infinity":0,"fruit":0},{"type":406,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2016, '{"ids":[899]}', '{"drop":[{"type":1,"chance":9900,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0},{"type":406,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2017, '{"ids":[899]}', '{"drop":[{"type":1,"chance":9900,"maxdropcount":3,"maxcount":5,"infinity":0,"fruit":0},{"type":405,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2018, '{"ids":[800,807,808,801]}', '{"drop":[{"type":6,"chance":6500,"maxdropcount":2,"maxcount":30,"infinity":0,"fruit":0},{"type":7,"chance":3000,"maxdropcount":3,"maxcount":20,"infinity":0,"fruit":0},{"type":408,"chance":0,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0},{"type":8,"chance":500,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0}]}'),
(2019, '{"ids":[800,807,808,801]}', '{"drop":[{"type":6,"chance":6500,"maxdropcount":2,"maxcount":30,"infinity":0,"fruit":0},{"type":7,"chance":3000,"maxdropcount":3,"maxcount":20,"infinity":0,"fruit":0},{"type":408,"chance":0,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0},{"type":8,"chance":500,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0}]}'),
(2020, '{"ids":[899,804,809]}', '{"drop":[{"type":1,"chance":3000,"maxdropcount":2,"maxcount":7,"infinity":0,"fruit":0},{"type":2,"chance":2000,"maxdropcount":2,"maxcount":5,"infinity":0,"fruit":0},{"type":3,"chance":5000,"maxdropcount":2,"maxcount":7,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0},{"type":300,"chance":10000,"maxdropcount":4,"maxcount":4,"infinity":0,"fruit":1}]}'),
(2021, '{"ids":[804,809,899]}', '{"drop":[{"type":1,"chance":3000,"maxdropcount":3,"maxcount":6,"infinity":0,"fruit":0},{"type":2,"chance":3000,"maxdropcount":3,"maxcount":4,"infinity":0,"fruit":0},{"type":3,"chance":4000,"maxdropcount":2,"maxcount":7,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":1,"maxcount":3,"infinity":0,"fruit":0},{"type":301,"chance":10000,"maxdropcount":3,"maxcount":3,"infinity":0,"fruit":1}]}'),
(2022, '{"ids":[899]}', '{"drop":[{"type":302,"chance":9900,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":1},{"type":407,"chance":100,"maxdropcount":1,"maxcount":1,"infinity":0,"fruit":0}]}'),
(2500, '{"ids":[]}', '{"drop":[]}'),
(2501, '{"ids":[]}', '{"drop":[{"type":1,"chance":6000,"maxdropcount":10,"maxcount":30,"infinity":0,"fruit":0},{"type":2,"chance":5000,"maxdropcount":10,"maxcount":20,"infinity":0,"fruit":0},{"type":3,"chance":4000,"maxdropcount":5,"maxcount":20,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":2,"maxcount":5,"infinity":0,"fruit":0}]}'),
(2502, '{"ids":[]}', '{"drop":[{"type":1,"chance":6000,"maxdropcount":10,"maxcount":30,"infinity":0,"fruit":0},{"type":2,"chance":5000,"maxdropcount":10,"maxcount":20,"infinity":0,"fruit":0},{"type":3,"chance":4000,"maxdropcount":5,"maxcount":20,"infinity":0,"fruit":0},{"type":4,"chance":0,"maxdropcount":2,"maxcount":5,"infinity":0,"fruit":0}]}');

-- --------------------------------------------------------

--
-- Table structure for table `worlds`
--

CREATE TABLE IF NOT EXISTS `worlds` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `world_time` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `worlds`
--

INSERT INTO `worlds` (`id`, `name`, `world_time`) VALUES
(1, 'My World', 6931184231.468872);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `characters`
--
ALTER TABLE `characters`
  ADD CONSTRAINT `characters_ibfk_1` FOREIGN KEY (`acc_id`) REFERENCES `accounts` (`id`),
  ADD CONSTRAINT `characters_ibfk_2` FOREIGN KEY (`island_id`) REFERENCES `islands` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
