﻿namespace SurvCommon
{
    /// <summary>
    /// Различные ошибки
    /// </summary>
    public enum ReturnCode : byte
    {
        Ok = 0,
        OldVersion,
        MultiPlatform,
        OperationNotSupported,
        OperationInvalid,
        DontHaveRequiredInstrument,
        DontHaveRequiredResources,
        AccessDenied,
        DontHaveFreeSpace,
        DontHaveRequiredBuilding,
        DontHaveRequiredSkill,
        DontHaveEnoughtMoney,
        NameAlreadyUse,
    }
}
