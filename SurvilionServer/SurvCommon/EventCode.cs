﻿namespace SurvCommon
{
    public enum EventCode : byte
    {
        PlayerJoin,
        PlayerExit,

        ItemSpowned,
        ItemPicked,
        ItemRemoved,
        ItemMoved,

        WorldTime,

        ChatHistory,
        ChatMessage,

        //inventory
        InventoryItems,
        EquippedItems,
        //learning
        ObjLearned,
        //other
        PlayerDie,
        PlayerAttack,
        PlayerEquippedItems,

        BuildingProccessInfoUpdate,

        Weather,
        BalanceUpdated,
        SkillsList,

        CharacterParameters,
    }
}
