﻿using System.Linq;

namespace SurvCommon.Helpers
{
    public class StringFormater
    {
        public static bool CheckChatMessage(ref string message)
        {
            if (string.IsNullOrEmpty(message))
                return false;

            if (message.Length > GameConfig.MaxChatMessageLength)
                message = message.Take(GameConfig.MaxChatMessageLength).ToString().Trim();

            message = message.Replace(GameConfig.ChatDevider, "");

            return true;
        }
    }
}
