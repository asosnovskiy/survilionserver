﻿using System;

namespace SurvCommon.Helpers
{
    public class RandomHelper
    {
        private readonly static Random _random = new Random();

        public static int Next(int min, int max)
        {
            return _random.Next(min, max);
        }

        /// <summary>
        /// 0.0 - 1.0
        /// </summary>
        /// <returns></returns>
        public static double Next()
        {
            return _random.NextDouble();
        }

        public static float Next(float min, float max)
        {
            var rand = (float)_random.NextDouble();
            return min + (max - min)*rand;
        }
    }
}
