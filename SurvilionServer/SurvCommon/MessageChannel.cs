﻿namespace SurvCommon
{
    public enum MessageChannel: byte
    {
        Default = 0,    //reliable
        Unreliable = 1,
        Move = 2,
        LargeData = 3,
    }
}
