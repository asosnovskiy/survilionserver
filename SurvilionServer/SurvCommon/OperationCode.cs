﻿namespace SurvCommon
{
    public enum OperationCode : byte
    {
        Login = 10,
        GetCharacterList,
        CreateCharacter,
        SelectCharacter,
        DeleteCharacter,
        ByeCharacter,
        GetCharacterInfo,  //parameters, money, and other
        GetRanking,

        LoadLevel,
        ExitLevel,
        LevelLoaded,
        Move,

        ChatMessage,

        ItemDropFromInventory,
        ItemDropFromBuild,
        ItemPick,
        ItemEquip,
        ItemUnequip,
        ItemUse,

        LearnSkill,
        CraftItem,

        PlayerMine,
        PlayerAttack,

        BuildBuilding,
        BuilingHandle,
        DestroyBuilding,
        SpawnItem,
    }
}
