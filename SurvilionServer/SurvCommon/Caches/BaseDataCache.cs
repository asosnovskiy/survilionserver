﻿using System.Collections.Generic;

namespace SurvCommon.Caches
{
    public class BaseDataCache<TKey,TValue> where TValue:class 
    {
        public static readonly Dictionary<TKey, TValue> Items = new Dictionary<TKey, TValue>();

        public static void Add(TKey key, TValue data)
        {
            Items.Add(key, data);
        }

        public static TValue GetItem(TKey type)
        {
            if (Items.ContainsKey(type))
                return Items[type];

            return null;
        }
    }
}
