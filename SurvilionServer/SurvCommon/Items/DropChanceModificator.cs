﻿using System;

namespace SurvCommon.Items
{
    [Serializable]
    public class DropChanceModificator
    {
        public ObjectsDic Resource { get; set; }
        public int Chance { get; set; }
        public int Count { get; set; }
    }
}
