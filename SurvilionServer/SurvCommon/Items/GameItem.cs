﻿using System;
using SurvCommon.Caches;
using SurvCommon.Serialization;

namespace SurvCommon.Items
{
    public class GameItem
    {
        public long Id { get; private set; }
        public ObjectsDic Type { get; private set; }

        public GameItemData Data{get { return GameItemDataCache.GetItem(Type); }}

        public int Count { get; private set; }
        public int Durability { get; private set; }

        public GameItem(long id, ObjectsDic type, int count, int dur)
        {
            Id = id;
            Type = type;
            Count = count;
            Durability = dur;
        }

        public InventoryItem BuildInventoryItem(short slot)
        {
            return new InventoryItem
                {
                    Id = Id,
                    Type = (short) Type,
                    Count = Count,
                    Durability = Durability,
                    Slot = slot
                };
        }

        public void AdjCount(int amount)
        {
            if (Count + amount < 0)
            {
                throw new ArgumentException("count of item less then 0!!!");
            }

            Count += amount;
        }
    }
}
