﻿using System;
using System.Collections.Generic;
using JsonFx.Json;

namespace SurvCommon.Items
{
    public enum ItemSlot
    {
        None,			// First element MUST be 'None'
        Helm,			// All the following elements are yours to customize -- edit, add or remove as you desire
        Body,
        Pants,
        Boots,
        Glovers,
        Weapon,
        _LastDoNotUse
    }

    /// <summary>
    /// All config properties for items
    /// </summary>
    [Serializable]
    public class GameItemData
    {
        public virtual int Id { get; set; }
        public virtual bool IsMulticount { get; set; }
        public virtual int MaxDurability { get; set; }

        public virtual float MinDmg { get; set; }
        public virtual float MaxDmg { get; set; }

        public virtual int Speed { get; set; }
        public virtual float Distance { get; set; }

        public virtual float RegenHealth { get; set; }
        public virtual float RegenTiredness { get; set; }
        public virtual float RegenHunger { get; set; }
        public virtual float RegenDrought { get; set; }

        public virtual short Slot { get; set; }

        public virtual string Modificators { get; set; }
        //TODO: for effect, need user List<EffectType>
        //public readonly int RegenEffects{ get; set; }

        public virtual ObjectsDic Type { get { return (ObjectsDic)Id; } }
        public virtual ItemSlot ItemSlot { get { return (ItemSlot)Slot; } }

        public virtual bool CanUse
        {
            get
            {
                return RegenHealth != 0f ||
                       RegenHunger != 0f ||
                       RegenDrought != 0f ||
                       RegenTiredness != 0f;
            }
        }

        private List<DropChanceModificator> _cachedModificators;

        public virtual List<DropChanceModificator> LoadModificators()
        {
            if (_cachedModificators != null)
                return _cachedModificators;

            var json = JsonReader.Deserialize(Modificators) as IDictionary<string, object>;

            var modificators = (object[])json["items"];
            _cachedModificators = new List<DropChanceModificator>(modificators.Length);

            foreach (IDictionary<string, object> item in modificators)
            {
                _cachedModificators.Add(new DropChanceModificator
                    {
                        Resource = (ObjectsDic)(int)item["type"],
                        Chance = (int)item["chance"],
                        Count = (int)item["count"]
                    });
            }

            return _cachedModificators;
        }
    }
}
