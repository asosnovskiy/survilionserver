﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using JsonFx.Json;

namespace SurvCommon.Items
{
    [Serializable]
    public class SkillData
    {
        public virtual int Id { get; set; }
        public virtual string Data { get; set; }

        [XmlIgnore]
        public virtual SkillType Type { get { return (SkillType)Id; } }

        [XmlIgnore]
        private int _reqPoints;
        [XmlIgnore]
        public virtual int ReqPoints { get { return _reqPoints; } }
        [XmlIgnore]
        private List<SkillType> _reqSkills;
        [XmlIgnore]
        public virtual List<SkillType> ReqSkills { get { return _reqSkills; } }

        public virtual void ParseSkillData()
        {
            var json = JsonReader.Deserialize(Data) as IDictionary<string, object>;

            _reqPoints = (int)json["reqPoints"];

            _reqSkills = new List<SkillType>();

            var reqSkills = json["reqSkills"] as int[];

            if (reqSkills != null)
                foreach (var item in reqSkills)
                {
                    _reqSkills.Add((SkillType)item);
                }
        }
    }
}
