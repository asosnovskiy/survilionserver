﻿using System;
using System.Collections.Generic;
using System.Linq;
using JsonFx.Json;

namespace SurvCommon.Items
{
    /// <summary>
    /// Settings for building build
    /// </summary>
    [Serializable]
    public class BuildData
    {
        public virtual int Id { get; set; }
        public virtual string Data { get; set; }

        private ReqData _reqData;

        public virtual ObjectsDic Type { get { return (ObjectsDic)Id; } }

        public virtual ReqData GetReqData()
        {
            if (_reqData != null)
                return _reqData;

            var json = JsonReader.Deserialize(Data) as IDictionary<string, object>;
            var reqResources = (object[])json["reqResources"];
            var resources = new Dictionary<ObjectsDic, int>(reqResources.Length);

            foreach (IDictionary<string, object> item in reqResources)
            {
                resources.Add((ObjectsDic)(int)item["type"], (int)item["count"]);
            }

            var reqSkills = json["reqSkills"] as int[];
            var skills = new List<SkillType>();
            if (reqSkills != null)
                skills.AddRange(reqSkills.Select(item => (SkillType) item));

            _reqData = new ReqData
                {
                    ReqBuild = (ObjectsDic)(int)json["reqBuild"],
                    ReqResources = resources,
                    ReqSkills = skills
                };

            return _reqData;
        }
    }

    public class ReqData
    {
        public ObjectsDic ReqBuild;
        public Dictionary<ObjectsDic, int> ReqResources;
        public List<SkillType> ReqSkills;
    }
}
