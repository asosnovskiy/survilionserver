﻿using System;

namespace SurvCommon
{
    [Serializable]
    public enum SkillType : byte
    {
        None,

        Wildman,
        Chopper,           //рубить дерево
        Digger,        //копатель
        Stonemason,     //каменщик

        Cook,           //повар
        Hunter,         //охотник
        Fishing,        //рыбачество
        Collector,      //собиратель
        Craftsman,      //ремесленник

        Miller,         //мельник
        Herbalist,      //травник
        Farmer,         //фермер
        Potter,         //гончар
        Miner,           //добывать руду(киркой)

        Tailor,         //портной
        Carpenter,      //плотник
        Blacksmith,         //кузнец
        Boatman,        //лодочник
        Warrior,        //воин
        Grazier,        //животновод
        Baker,          //пекарь
        Dux,            //вождь
        CheeseMaker,    //сыродел
        Skinner,        //кожевник
        Healer,         //лекарь
        Beekeeper,      //пчеловод
        Glazier,            //стекольщик
        Mechanic,       //механик
        Jeweler,        //ювелир
        SteelMaker,     //сталевар
        Alchemist,      //алхимик
        Brewer,         //пивовар
        LawMaker,       //законодатель
        Scientist,      //ученый
    }
}
