﻿namespace SurvCommon
{
    public enum GameParameterKeys : byte
    {
        ErrorCode = 0,
        SocialId,
        GameVersion,

        CharacterList,
        CharacterInfo,

        CharacterId,
        CharacterName,
        CharacterSex,
        CharacterMoney,

        IslandId,

        //hp,hunger,drought,tiredness
        CharacterParameters = 10,

        Position,
        Rotation,
        StaticData,
        ItemId = 14,
        ItemType,
        ItemData,
        WorldTime,

        ChatMessage,
        UserGroup,
        DynamicData,
        StaticItems,
        UsedItem,
        InventoryItems,
        ItemCount,
        ResourceType,
        ResourceCount
    }
}
