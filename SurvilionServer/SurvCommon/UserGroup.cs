﻿namespace SurvCommon
{
    public enum UserGroup: byte
    {
        Normal = 0,
        Moderator = 1,
        Admin = 2,
        Owner = 3,
    }
}