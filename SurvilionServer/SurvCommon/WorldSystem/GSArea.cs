﻿using System.Collections.Generic;
using SurvCommon.Helpers;
using UnityEngine;

namespace SurvCommon.WorldSystem
{
    public class GSArea
    {
        #region Properties
        public string Name { get; private set; } // Имя области.
        public GSPolygon Polygon { get; private set; }
        public Dictionary<ObjectsDic, int> CommonCountObjects { get; private set; } // Ключ (ObjectsDic) - тип объекта, значение (int) - максимальное количество объектов для генерации в области.
        public Vector2 Center // Центр области.
        {
            get
            {
                float x = 0, y = 0;
                for (int i = 0; i < Polygon.Points.Count; i++)
                {
                    Vector2 point = Polygon.Points[i];
                    x += point.x;
                    y += point.y;
                }
                if (Polygon.Points.Count != 0)
                {
                    x /= Polygon.Points.Count;
                    y /= Polygon.Points.Count;
                }
                return new Vector2(x, y);
            }
        }
        private Vector2 RandomPoint { get { return new Vector2(RandomHelper.Next(rangeX.x, rangeX.y), RandomHelper.Next(rangeY.x, rangeY.y)); } } // Случайная точка в пределах;
        #endregion

        #region Fields
        private Vector2 rangeX, rangeY; // Vector2.x - min, Vector2.y - max
        private Dictionary<ObjectsDic, int> CurrentCountObjects;
        #endregion

        #region Constructor
        public GSArea(string name, List<Vector2> points, Dictionary<ObjectsDic, int> commonCountObjects)
        {
            this.Name = name;
            this.Polygon = new GSPolygon(points);
            this.CommonCountObjects = commonCountObjects;
            // Поиск пределов для случайных чисел.
            float[] xs = new float[this.Polygon.Points.Count], ys = new float[this.Polygon.Points.Count];
            for (int i = 0; i < this.Polygon.Points.Count; i++) { xs[i] = this.Polygon.Points[i].x; ys[i] = this.Polygon.Points[i].y; }
            this.rangeX = new Vector2(Mathf.Min(xs), Mathf.Max(xs));
            this.rangeY = new Vector2(Mathf.Min(ys), Mathf.Max(ys));
            this.SetCurrentCountObjects();
        }
        #endregion

        #region Methods
        private void SetCurrentCountObjects()
        {
            CurrentCountObjects = new Dictionary<ObjectsDic, int>();
            foreach (ObjectsDic objDic in CommonCountObjects.Keys)
                CurrentCountObjects.Add(objDic, 0);
        }

        public List<GSObject> Generate(int attempt) // attempt - количество неудачных попыток при исчерпании которых генерация заканчивается.
        {
            var result = new List<GSObject>();
            int currentAttempt = 0;
            foreach (ObjectsDic objDic in CommonCountObjects.Keys)
            {
                int commonCount = CommonCountObjects[objDic];
                while (commonCount > CurrentCountObjects[objDic])
                {
                    Vector2 point = RandomPoint;
                    if (Polygon.IsEnter(point))
                    {
                        var obj = new GSSimpleObject(objDic, point);
                        if (GSGrid.AddObject(obj))
                        {
                            result.Add(obj);
                            CurrentCountObjects[objDic]++;
                            currentAttempt = 0;
                        }
                        else if (currentAttempt > attempt)
                            return result;
                    }
                }
            }
            return result;
        }
        #endregion

        #region Override methods
        public override string ToString()
        {
            return Name + "[" + Polygon.Points.Count + "]";
        }
        #endregion
    }
}
