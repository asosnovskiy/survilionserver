﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SurvCommon.WorldSystem
{
    /// <summary>
    /// Сетка террейна.
    /// </summary>
    public static class GSGrid
    {
        public static GSCell[,] Cells { get; private set; }

        public static void Reconstruct()
        {
            int widthElements = (int)(GSData.TerrainRect.xMax / GSData.Divider);
            int heightElements = (int)(GSData.TerrainRect.yMax / GSData.Divider);
            Cells = new GSCell[widthElements, heightElements];
            float xStep = GSData.TerrainRect.xMax / widthElements;
            float yStep = GSData.TerrainRect.yMax / heightElements;
            Vector2 cursor = Vector2.zero;
            for (int i = 0; i < heightElements; i++)
            {
                cursor = new Vector2(0, i * yStep);
                for (int j = 0; j < widthElements; j++)
                    Cells[j, i] = new GSCell(new GSRect(new Vector2(cursor.x, cursor.y + yStep), new Vector2(cursor.x += xStep, cursor.y)));
            }
        }

        // Возвращает истинну, если объект установлен.
        public static bool AddObject(GSObject obj)
        {
            var cells = Find(obj);
            if (cells.SelectMany(cell => cell.Objects.Values).Any(element => element.IsIntersect(obj)))
            {
                return false;
            }
            foreach (var cell in cells)
                cell.Objects.Add(obj.Id, obj);
            return true;
        }

        // Возвращает истинну, если объект установлен.
        public static bool AddObject(GSObject obj, bool ignoreCollision) // ignoreCollision - если истина, то добавляет даже при столкновений.
        {
            var cells = Find(obj);
            if (!ignoreCollision)
                if (cells.SelectMany(cell => cell.Objects.Values).Any(element => element.IsIntersect(obj)))
                {
                    return false;
                }
            foreach (var cell in cells)
                cell.Objects.Add(obj.Id, obj);
            return true;
        }

        public static bool RemoveObject(GSObject obj)
        {
            var cells = Find(obj);
            foreach (var cell in cells)
                cell.Objects.Remove(obj.Id);
            return true;
        }

        public static GSCell Find(Vector2 point)
        {
            return Find(point.x, point.y);
        }

        public static GSCell Find(float x, float y)
        {
            if (!GSData.TerrainRect.Contains(new Vector2(x, y)))
                return null;
            int i = (int)(x / GSData.Divider);
            int j = (int)(y / GSData.Divider);
            return Cells[i, j];
        }

        public static List<GSCell> Find(GSObject obj)
        {
            var result = new List<GSCell>();
            if (obj.GSType == GSObjectType.Simple)
            {
                var s_obj = obj as GSSimpleObject;
                GSCell cell1 = Find(s_obj.Rect.p1), cell2 = Find(s_obj.Rect.p1.x, s_obj.Rect.p3.y), cell3 = Find(s_obj.Rect.p3), cell4 = Find(s_obj.Rect.p3.x, s_obj.Rect.p1.y);
                if (cell1 != null)
                    result.Add(cell1);
                if (cell2 != null && cell2 != cell1)
                    result.Add(cell2);
                if (cell3 != null && cell3 != cell1 && cell3 != cell2)
                    result.Add(cell3);
                if (cell4 != null && cell4 != cell1 && cell4 != cell2 && cell4 != cell3)
                    result.Add(cell4);
                return result;
            }

            var c_obj = obj as GSComplexObject;
            c_obj.Polygon.Points.ForEach(x =>
            {
                GSCell cell = GSGrid.Find(x);
                if (cell != null && !result.Contains(cell))
                    result.Add(cell);
            });
            return result;
        }

    }
}
