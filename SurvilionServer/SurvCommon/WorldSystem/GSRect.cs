﻿using System.Linq;
using UnityEngine;

namespace SurvCommon.WorldSystem
{
    /// <summary>
    /// Прямоугольник сетки.
    /// </summary>
    public class GSRect
    {
        public Vector2 p1;
        public Vector2 p3;

        public GSRect(Vector2 p1, Vector2 p3)
        {
            this.p1 = p1;
            this.p3 = p3;
        }

        public bool IsEnter(Vector2 point) { return IsEnter(point.x, point.y); }
        public bool IsEnter(float x, float y)
        {
            if (x >= p1.x && y <= p1.y && x <= p3.x && y >= p3.y)
                return true;
            return false;
        }
        public bool IsEnter(GSPolygon polygon)
        {
            if (polygon.IsEnter(p1) || polygon.IsEnter(p3) || polygon.IsEnter(p1.x, p3.y) || polygon.IsEnter(p3.x, p1.y))
                return true;

            return polygon.Points.Any(IsEnter);
        }
        public bool IsEnter(GSRect rect)
        {
            if (IsEnter(rect.p1) || IsEnter(new Vector2(rect.p3.x, rect.p1.y)) || IsEnter(rect.p3) || IsEnter(new Vector2(rect.p1.x, rect.p3.y)))
                return true;
            if (rect.IsEnter(p1) || rect.IsEnter(new Vector2(p3.x, p1.y)) || rect.IsEnter(p3) || rect.IsEnter(new Vector2(p1.x, p3.y)))
                return true;
            return false;
        }

        public void Draw()
        {
            Gizmos.DrawLine(new Vector3(p1.x, 0, p1.y), new Vector3(p1.x, 0, p3.y));
            Gizmos.DrawLine(new Vector3(p3.x, 0, p3.y), new Vector3(p1.x, 0, p3.y));
            Gizmos.DrawLine(new Vector3(p3.x, 0, p3.y), new Vector3(p3.x, 0, p1.y));
            Gizmos.DrawLine(new Vector3(p1.x, 0, p1.y), new Vector3(p3.x, 0, p1.y));
        }
    }
}
