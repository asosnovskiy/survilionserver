﻿using System.Collections.Generic;
using System.Xml;
using SurvCommon.Extensions;
using UnityEngine;

namespace SurvCommon.WorldSystem
{
    public static class GSData
    {
        public static readonly List<GSArea> Areas;
        public static Rect TerrainRect { get; private set; }
        public static readonly Dictionary<ObjectsDic, Vector2> SizeOfObjects;
        public static float Divider { get; private set; }

        static GSData()
        {
            Areas = new List<GSArea>(32);
            SizeOfObjects = new Dictionary<ObjectsDic, Vector2>(32);
        }

        /// <summary>
        /// Загрузка регионов и создание сетки.
        /// </summary>
        public static void LoadAreas(XmlDocument document)
        {
            Areas.Clear();

            foreach (XmlElement globalSpace in document.GetElementsByTagName("GlobalSpace"))
            {
                TerrainRect = new Rect(globalSpace.Attributes["left"].InnerText.AsFloat(),
                    globalSpace.Attributes["top"].InnerText.AsFloat(),
                    globalSpace.Attributes["width"].InnerText.AsFloat(),
                    globalSpace.Attributes["height"].InnerText.AsFloat());
                Divider = globalSpace.Attributes["divider"].InnerText.AsFloat();
            }

            GSGrid.Reconstruct();
            foreach (XmlElement element in document.GetElementsByTagName("Area"))
            {
                if (element.Attributes["name"] == null)
                    continue;
                var points = new List<Vector2>();
                XmlElement pointsXML = element["Points"];
                foreach (XmlElement p in pointsXML.GetElementsByTagName("Point"))
                    points.Add(new Vector2(p.Attributes["x"].InnerText.AsFloat(), p.Attributes["y"].InnerText.AsFloat()));
                var cco = new Dictionary<ObjectsDic, int>();
                var objectsXML = element["Objects"];
                foreach (XmlElement p in objectsXML.GetElementsByTagName("Object"))
                    cco.Add((ObjectsDic)p.Attributes["id"].InnerText.AsInt(), p.Attributes["count"].InnerText.AsInt());
                Areas.Add(new GSArea(element.Attributes["name"].InnerText, points, cco));
            }
        }

        /// <summary>
        /// Загрузка размеров объектов для перечесления ObjectsDic.
        /// </summary>
        public static void LoadSizes(XmlDocument document)
        {
            SizeOfObjects.Clear();

            foreach (XmlElement element in document.GetElementsByTagName("Object"))
            {
                if (element.Attributes["id"] == null) 
                    continue;

                var size = new Vector2(element.Attributes["x"].InnerText.AsFloat(), element.Attributes["y"].InnerText.AsFloat());
                SizeOfObjects.Add((ObjectsDic)element.Attributes["id"].InnerText.AsInt(), size);
            }
        }

        #region Instruments


        #endregion

    }
}
