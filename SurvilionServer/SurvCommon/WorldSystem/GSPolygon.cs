﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace SurvCommon.WorldSystem
{
    public class GSPolygon
    {
        public List<Vector2> Points { get; private set; } // Точки многоугольника. Отсчет сторон: (0<->1),(1<->2),(2<->3),(n<->(n + 1)) и в конце замыкаем ((n + 1)<->0). Где (n + 1) = (Points.Count - 1)

        public GSPolygon(List<Vector2> points)
        {
            this.Points = points;
        }

        #region IsEnter
        public bool IsEnter(Vector3 point) { return IsEnter(point.x, point.z); }
        public bool IsEnter(Vector2 point) { return IsEnter(point.x, point.y); }
        public bool IsEnter(GSRect rect)
        {
            if (IsEnter(rect.p1) || IsEnter(rect.p3) || IsEnter(rect.p1.x, rect.p3.y) || IsEnter(rect.p3.x, rect.p1.y))
                return true;
            return Points.Any(rect.IsEnter);
        }
        public bool IsEnter(GSPolygon polygon)
        {
            if (polygon.Points.Any(IsEnter))
            {
                return true;
            }

            return Points.Any(polygon.IsEnter);
        }
        public bool IsEnter(float x, float y)
        {
            bool c = false;
            for (int i = 0, j = Points.Count - 1; i < Points.Count; j = i++)
            {
                Vector2 i_v2 = Points[i], j_v2 = Points[j];
                if ((((i_v2.y <= y) && (y < j_v2.y)) || ((j_v2.y <= y) && (y < i_v2.y))) && (x < (j_v2.x - i_v2.x) * (y - i_v2.y) / (j_v2.y - i_v2.y) + i_v2.x))
                    c = !c;
            }
            return c;
        }
        #endregion

        public void Draw()
        {
#if UNITY_EDITOR
            for (int i = 0; i < Points.Count; i++)
            {
                Vector2 s = Points[i];
                Vector2 e = Points[i == Points.Count - 1 ? 0 : i + 1];
                Gizmos.DrawLine(new Vector3(s.x, TerrainManager.Terrain.SampleHeight(new Vector3(s.x, 0, s.y)) + 1f, s.y), new Vector3(e.x, TerrainManager.Terrain.SampleHeight(new Vector3(e.x, 0, e.y)) + 1f, e.y));
            }
#endif
        }

    }
}
