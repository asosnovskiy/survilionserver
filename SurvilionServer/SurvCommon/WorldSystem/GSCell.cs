﻿using System.Collections.Generic;

namespace SurvCommon.WorldSystem
{
    public class GSCell
    {
        public GSRect Rect { get; private set; }
        public readonly Dictionary<long, GSObject> Objects;

        public GSCell(GSRect rect)
        {
            Rect = rect;
            Objects = new Dictionary<long, GSObject>();
        }
    }
}
