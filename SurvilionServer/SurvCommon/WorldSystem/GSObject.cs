﻿using System.Threading;
using UnityEngine;

namespace SurvCommon.WorldSystem
{
    public abstract class GSObject
    {
        private static long _counter;

        private readonly long _id;
        private readonly ObjectsDic _type;
        private Vector2 _position;
        private readonly GSObjectType _gsType;

        public long Id { get { return _id; } }
        public ObjectsDic Type { get { return _type; } }
        public Vector2 Position
        {
            get
            {
                return _position;
            }
            set
            {
                _position = value;
                RecalculateCollisionArea();
            }
        }

        public GSObjectType GSType { get { return _gsType; } }

        protected GSObject(Vector2 position, ObjectsDic type, GSObjectType gsType)
        {
            Interlocked.Increment(ref _counter);
            _id = _counter;
            _type = type;
            _position = position;

            _gsType = gsType;
        }

        protected virtual void RecalculateCollisionArea()
        {
            
        }

        public abstract bool IsIntersect(GSObject obj); // Пересекает ли этот объект другой объект.
        public abstract void Draw();
        public void Create(Terrain terrain)
        {
            GameObject.Instantiate(Resources.Load("MapObjects/" + Type.ToString()), new Vector3(Position.x, terrain.SampleHeight(new Vector3(Position.x, 0, Position.y)), Position.y), Quaternion.identity);
        }

        public override string ToString()
        {
            return "GSObject: " + Id;
        }
    }
}
