﻿using UnityEngine;

namespace SurvCommon.WorldSystem
{
    public class GSSimpleObject : GSObject
    {
        public GSRect Rect { get; private set; }
        public GSSimpleObject(ObjectsDic obj, Vector2 position)
            : base(position, obj, GSObjectType.Simple)
        {
            RecalculateCollisionArea();
        }

        protected override void RecalculateCollisionArea()
        {
            base.RecalculateCollisionArea();

            Vector2 size;
            if (!GSData.SizeOfObjects.TryGetValue(Type, out size))
            {
                size = Vector2.one;
            }

            Rect = new GSRect(new Vector2(Position.x - (size.x / 2), Position.y + (size.y / 2)),
                new Vector2(Position.x + (size.x / 2), Position.y - (size.y / 2)));
        }

        public override bool IsIntersect(GSObject obj)
        {
            if (obj.GSType == GSObjectType.Simple)
                return (obj as GSSimpleObject).Rect.IsEnter(Rect);
            return (obj as GSComplexObject).Polygon.IsEnter(Rect);
        }

        public override void Draw()
        {
            Rect.Draw();
        }

        public override string ToString()
        {
            return "GSSimpleObject: " + Id;
        }
    }
}
