﻿using System.Collections.Generic;
using UnityEngine;

namespace SurvCommon.WorldSystem
{
    public class GSComplexObject : GSObject
    {
        public GSPolygon Polygon { get; private set; }

        public GSComplexObject(Vector2 point, ObjectsDic obj, List<Vector2> points)
            : base(point, obj, GSObjectType.Complex)
        {
            this.Polygon = new GSPolygon(points);
        }

        public override bool IsIntersect(GSObject obj)
        {
            if (obj.GSType == GSObjectType.Simple)
                return Polygon.IsEnter((obj as GSSimpleObject).Rect);
            return Polygon.IsEnter((obj as GSComplexObject).Polygon);
        }

        public override void Draw()
        {
            Polygon.Draw();
        }

        public override string ToString()
        {
            return "GSComplexObject: " + Id;
        }
    }
}
