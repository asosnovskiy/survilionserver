﻿using System.IO;
using System.Text;

namespace SurvCommon.Network
{
    public class Packet
    {
        private readonly MemoryStream _mMemBuffer;
        private readonly BinaryWriter _mMemWriter;
        private readonly BinaryReader _memReader;

        public Packet()
        {
            _mMemBuffer = new MemoryStream();
            _mMemWriter = new BinaryWriter(_mMemBuffer);
        }

        public Packet(int count)
        {
            _mMemBuffer = new MemoryStream(count);
            _mMemWriter = new BinaryWriter(_mMemBuffer);
        }

        public Packet(byte[] mas)
        {
            _mMemBuffer = new MemoryStream();
            _memReader = new BinaryReader(_mMemBuffer);
            _mMemBuffer.Seek(0, SeekOrigin.Begin);
            _mMemBuffer.Write(mas, 0, mas.Length);
            _mMemBuffer.Seek(0, SeekOrigin.Begin);
        }

        public byte[] Buffer
        {
            get { return _mMemBuffer.ToArray(); }
        }

        /// Regular "add" functions
        public void Addbyte(byte b)
        {
            _mMemWriter.Write(b);
        }
        public void Addbool(bool b)
        {
            _mMemWriter.Write(b);
        }
        public void Addint32(int i)
        {

            _mMemWriter.Write(i);
        }
        public void AddUint32(uint i)
        {
            Addint32((int) i);
        }
        public void Addfloat(float f)
        {
            _mMemWriter.Write(f);
        }
        public void Addfloat(double d)
        {
            _mMemWriter.Write((float) d);
        }
        public void Addint16(short s)
        {
            _mMemWriter.Write(s);
        }
        public void Addint64(long l)
        {
            _mMemWriter.Write(l);
        }
        public void AddUint64(ulong l)
        {
            _mMemWriter.Write(l);
        }
        public void Addstring(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            Addint16((short) bytes.Length);
            for (int i = 0; i < bytes.Length; i++)
                Addbyte(bytes[i]);
        }
        public void Addbytes(params byte[] bytes)
        {
            _mMemWriter.Write(bytes);
        }

        /// "Read" functions
        public int Readint32()
        {
            return _memReader.ReadInt32();
        }
        public string Readstring()
        {
            var dwLength = Readint16();
            var stringBytes = _memReader.ReadBytes(dwLength);
            return Encoding.UTF8.GetString(stringBytes);
        }
        public float Readfloat()
        {
            return _memReader.ReadSingle();
        }
        public short Readint16()
        {
            return _memReader.ReadInt16();
        }
        public byte Readbyte()
        {
            return _memReader.ReadByte();
        }
        public bool Readbool()
        {
            return _memReader.ReadBoolean();
        }
        public long Readint64()
        {
            return _memReader.ReadInt64();
        }

        public ulong ReadUint16()
        {
            return _memReader.ReadUInt16();
        }
        public ulong ReadUint32()
        {
            return _memReader.ReadUInt32();
        }
        public ulong ReadUint64()
        {
            return _memReader.ReadUInt64();
        }
    }
}