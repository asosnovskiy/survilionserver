﻿using System.Globalization;
using UnityEngine;

namespace SurvCommon.Extensions
{
    public static class Extensions
    {
        public static float AsFloat(this string s)
        {
            return float.Parse(s.Replace(",", "."), CultureInfo.InvariantCulture);
        }

        public static int AsInt(this string s)
        {
            return int.Parse(s);
        }

        public static Vector2 ToVector2(this float[] mas)
        {
            if (mas == null)
                return Vector2.zero;
            return new Vector2(mas[0], mas[1]);
        }

        public static float[] ToArray(this Vector2 v)
        {
            return new[] { v.x, v.y };
        }

        public static float[] ToArray(this Vector3 v)
        {
            return new[] {v.x, v.z};
        }

        public static bool IsMining(this ObjectsDic item)
        {
            var num = (int)item;
            return num >= 2000 && num < 2500;
        }

        public static bool IsBuilding(this ObjectsDic item)
        {
            var num = (int)item;
            return num >= 1000 && num < 2000;
        }

        public static bool IsPicked(this ObjectsDic item)
        {
            var num = (int)item;
            return num >= 1 && num < 999;
        }

        public static bool IsAnimal(this ObjectsDic item)
        {
            var num = (int)item;
            return num >= 6000 && num < 6500;
        }

        public static bool IsPlayer(this ObjectsDic item)
        {
            var num = (int)item;
            return num >= 5000 && num < 5500;
        }

        public static Rect IntersectWith(this Rect current, Rect other)
        {
            var currentMin = new Vector2(current.xMin, current.yMin);
            var currentMax = new Vector2(current.xMax, current.yMax);
            var otherMin = new Vector2(other.xMin, other.yMin);
            var otherMax = new Vector2(other.xMax, other.yMax);

            var min = Vector2.Max(currentMin, otherMin);
            var max = Vector2.Min(currentMax, otherMax);

            return new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
        }
    }
}
