﻿using System;

namespace SurvCommon
{
    //
    //Список итемов
    //

    [Serializable]
    public enum ObjectsDic
    {
        None = 0,                   //вещь неопределена

        #region resources
        //1 уровень ресурсов (сразу при добыче)
        Leaf,       //лист
        Bark,       //кора дерева
        Branch,     //ветка
        Log,        //бревно
        Liana,      //лиана

        Stone,      //камень
        Pebble,     //галька
        Flint,      //кремень

        IronOre,       //железная руда
        Coal,       //уголь
        Clay,       //глина
        Sand,       //песок
        Worm,       //Червь(земля, деревья)
        CopperOre,  //медная руда
        GoldOre,    //золотая руда
        BambooBranch,   //бамбуковая ветвь

        //от животных
        Skin = 100,       //шкура
        Meat,       //мясо
        Bone,       //Кость
        Claw,       //Коготь
        Feather,    //перо
        Wool,       //шерсть
        Egg,        //яйцо
        Fish,       //рыба
        Canine,     //клык

        //от растений
        Cotton = 200,     //хлопок
        Tobacco,    //табак?
        Hemp,       //Конопля?

        //фрукты ягоды
        Banana = 300,     //банан
        Coco,       //Кокос
        MushroomResource,   //гриб


        //диковины
        Pearl = 400,      //жемчужина
        Scale,      //чешуя
        Butterfly,  //бабочка
        Bee,        //пчела
        Shell,      //ракушка
        Spider,     //паук
        Bug,        //жук
        StrangeMushroom,    //странный гриб
        Jewel,      //Драгоценный камень
        GoldNugget, //золотой самородок
        #endregion

        #region craft
        //2 уровень - крафт
        Leather = 500,    //кожа(крафт)
        Brick,              //кирпич
        Bucket,             //ведро
        Board,              //Доска
        Rope,               //веревка
        Spike,              //гвоздь
        Glass,              //стекло
        Hook,               //крючок

        IronBar = 600,      //железный слиток
        CupperBar = 601,    //медный слиток
        GoldBar = 602,      //золотой слиток

        FriedMeat = 700,    //жареное мясо
        FriedFish = 701,    //жареная рыба
        #endregion

        #region instruments
        StoneHammer = 800,       //каменный молоток
        StonePick = 801,         //каменная кирка
        Saw = 802,          //железная пила
        Rod = 803,          //удочка
        StoneAxe = 804,          //каменная топор
        Loupe = 805,        //лупа
        WoodenShovel = 806,       //деревянная лопата
        IronHammer,
        IronPick,
        IronAxe,
        IronShovel,

        //weapon
        Sling = 850,            //рогатка
        Bow,                    //лук

        //ammunitions
        StoneArrow = 870,       //каменная стрела
        IronArrow,              //железная стрела
        PoisonArrow,            //отравленная стрела

        
        #endregion

        Hands = 999,        //руки

        #region builds
        Campfire = 1000,       //костер
        Tent,           //шалаш
        Hut,            //хижина
        Storage,        //Склад
        Cellar,         //Погреб
        Workbench,      //Верстак
        Stithy,         //наковальня
        Oven,        //Обычная печь
        Well,           //Колодец
        Box,            //Ящик
        Forge,          //Печь для плавки руды
        Lab,            //Лаборатория
        Crate,          //Ящик
        Target,         //Мишень
        #endregion

        #region mining objs
        Palm = 2000,    //пальма         //сейба
        Banyan,
        Bamboo,
        Smallpalm,
        Lakebush,
        Bush_1,
        Fern,
        Rock_1,
        Grass_1,
        Grass_2,
        Smallpalm_2,
        Leaftree_1,
        Leaftree_2,
        Noleaftree,
        Lotus,
        Plant_1,
        Plant_2,
        Bush_2,
        Rock_2,
        Rock_3,
        Bananapalm,
        Palmcoast,
        Mushroom,
        #endregion

        #region sea objs
        Starfish = 2500,
        Shell_1,
        Shell_2,
        Pebbles,
        #endregion

        #region players & npc
        PlayerPref = 5000,

        Npc = 5500,
        #endregion

        #region animals
        Crab = 6000,
        Leo,
        #endregion
    }
}