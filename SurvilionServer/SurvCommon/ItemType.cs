﻿namespace SurvCommon
{
    public enum ItemType:byte
    {
        Player = 1,
        Npc,
        Animal,
        Static,
        Dynamic
    }
}
