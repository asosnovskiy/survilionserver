﻿using System;

namespace SurvCommon.Serialization
{
    [Serializable]
    public class MapObjectItem
    {
        public long Id { get; set; }
        public short Type { get; set; }
        public float[] Position { get; set; }
        public float Rotation { get; set; }
    }
}
