﻿using System;

namespace SurvCommon.Serialization
{
    [Serializable]
    public class InventoryItem
    {
        public long Id { get; set; }
        public short Type { get; set; }
        public int Count { get; set; }
        public int Durability { get; set; }
        public short Slot { get; set; }
    }
}
