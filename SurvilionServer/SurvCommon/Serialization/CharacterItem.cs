﻿using System;

namespace SurvCommon.Serialization
{
    [Serializable]
    public class CharacterItem
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
        public int DieTime { get; set; }
    }
}
