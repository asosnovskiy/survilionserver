﻿using System;

namespace SurvCommon.Serialization
{
    [Serializable]
    public class CharacterInfo
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Money { get; set; }

        public float Hp { get; set; }
        public float Hunger { get; set; }
        public float Drought { get; set; }
        public float Tiredness { get; set; }

        public long IslandId { get; set; }

        public float PosX { get; set; }
        public float PosY { get; set; }
        public float PosZ { get; set; }

        public float RotY { get; set; }
        //effects
    }
}
