﻿namespace SurvCommon
{
    public class GameConfig
    {
        public readonly static long WorldMsInRealSecond = 60000;

        public static readonly int MaxChatMessageLength = 255;


        public static readonly string ChatDevider = ":&";

        public const float RegWater = 0.001f; //возрастание жажды за одну секунду
        public const float RegEat = 0.001f;//возрастание голода за одну секунду
        public const float RegTiredness = 0.001f;//вохрастание усталости за одну секунду
        public const float DamageFromHunger = 0.01f;
        public const float DamageFromDrought = 0.01f;
        public const float DamageFromTiredness = 0.01f;

        public readonly static int MAX_DROP_CHANCE = 10000;

        public const int InventoryCount = 20;

        public const int LearnedObjPoints = 3;

        public const float BuildDistanceForCraft = 30f;

        public const double DieTime = 120;

        public const int ByeCharacterPrice = 5;
    }
}
