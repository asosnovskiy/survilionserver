﻿using System.Threading;
using SurvCommon;
using SurvCommon.Items;

namespace SurvGameServer.Factories
{
    public class GameItemFactory
    {
        private static GameItemFactory _instance;
        public static GameItemFactory Instance
        {
            get
            {
                if(_instance==null)
                    _instance = new GameItemFactory();
                return _instance;
            }
        }

        private static long _id;

        public GameItem BuildGameItem(ObjectsDic type, int count, int dur)
        {
            Interlocked.Increment(ref _id);
            var item = new GameItem(_id, type, count, dur);

            return item;
        }
    }
}
