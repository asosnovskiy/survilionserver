﻿using ExitGames.Logging;
using SurvGameServer.Database;
using SurvGameServer.Database.Data;

namespace SurvGameServer.Providers
{
    public class AccountProvider
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Check account on DB and create new, if no exists
        /// </summary>
        /// <param name="socialId">Social ID</param>
        /// <returns>Id - unique DB's Id</returns>
        public static long Login(long socialId)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    long id = 0;

                    var account = session.QueryOver<Account>().Where(n => n.SocialId == socialId).SingleOrDefault();

                    //exists account, get data
                    if (account != null)
                    {
                        Log.InfoFormat("Load account: {0}", socialId);
                        id = account.Id;
                    }
                    else
                    {
                        Log.InfoFormat("Create new account: {0}", socialId);

                        account = new Account
                        {
                            SocialId = socialId
                        };

                        session.Save(account);
                        id = account.Id;
                    }

                    trans.Commit();

                    return id;
                }
            }
        }

        public static Account GetAccount(long id, bool lazy = false)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                if (lazy)
                    return session.Load<Account>(id);

                return session.Get<Account>(id);
            }
        }
    }
}
