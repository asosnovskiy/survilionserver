﻿using System;
using System.Collections.Generic;
using System.Linq;
using JsonFx.Json;
using SurvCommon;
using SurvCommon.Extensions;
using SurvGameServer.Caches;
using SurvGameServer.Factories;
using SurvGameServer.WorldSystem;
using UnityEngine;

namespace SurvGameServer.Database.Data
{
    public class World
    {
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
        public virtual double WorldTime { get; set; }
        public virtual string Data { get; set; }

        public virtual List<MapObject> GetMapObjects()
        {
            var mapObjects = new List<MapObject>(4096);

            if (string.IsNullOrEmpty(Data))
                return mapObjects;

            var json = JsonReader.Deserialize(Data) as IDictionary<string, object>;

            var objs = json["objs"] as object[];
            if (objs != null)
                foreach (Dictionary<string, object> item in objs)
                {
                    var objType = (ObjectsDic)(int)item["type"];
                    var objPosition = new Vector3(float.Parse(item["posX"].ToString().Replace(".", ",")),
                                                  0,
                                                  float.Parse(item["posZ"].ToString().Replace(".", ",")));
                    var objRot = float.Parse(item["rotY"].ToString().Replace(".", ","));

                    MapObject mo;

                    if (objType.IsBuilding())
                    {
                        var remainResources = new Dictionary<ObjectsDic, int>();

                        var res = item["remainResources"] as object[];
                        if (res != null)
                            foreach (Dictionary<string, object> rr in res)
                            {
                                remainResources.Add((ObjectsDic)(int)rr["type"], (int)rr["count"]);
                            }

                        long ownerId = 1;
                        if (item["ownerId"] != null)
                            ownerId = Convert.ToInt64(item["ownerId"]);

                        mo = new BuildingMapObject(objType, objPosition, objRot, remainResources, ownerId);
                    }
                    else if (objType.IsMining())
                    {
                        mo = new MiningMapObject(objType, objPosition, objRot,
                                                   MiningDataCache.GetItem(objType)
                                                                  .GetDrop()
                                                                  .Select(
                                                                      n =>
                                                                      new DropItem
                                                                      {
                                                                          ItemType = n.ItemType,
                                                                          Count = n.MaxCount
                                                                      })
                                                                  .ToList());
                    }
                    else if (objType.IsPicked())
                    {
                        var pickCount = (int)item["count"];
                        var pickDur = (int)item["dur"];

                        mo = new PickMapObject(objPosition, objRot,
                                               GameItemFactory.Instance.BuildGameItem(objType, pickCount, pickDur));
                    }
                    else
                    {
                        mo = new MapObject(objType, objPosition, objRot);
                    }

                    mapObjects.Add(mo);
                }

            return mapObjects;
        }
    }
}
