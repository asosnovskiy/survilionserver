﻿namespace SurvGameServer.Database.Data
{
    public class Account
    {
        public virtual long Id { get; set; }
        public virtual long SocialId { get; set; }
    }
}
