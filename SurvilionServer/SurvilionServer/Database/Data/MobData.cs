﻿using SurvCommon;
using SurvCommon.Helpers;

namespace SurvGameServer.Database.Data
{
    public class MobData
    {
        public virtual int Id { get; set; }

        public virtual ObjectsDic Type { get { return (ObjectsDic)Id; } }

        public virtual int Speed { get; set; }
        public virtual float MaxHp { get; set; }

        public virtual int DieTimer { get; set; }
        public virtual int RespawnTimer { get; set; }

        public virtual float AgroMinDistance { get { return 10f; } }
        public virtual float AgroMaxDistance { get { return 30f; } }

        public virtual float AttackDistance { get { return 2f; } }
        public virtual float DmgMin { get { return 0.15f; } }
        public virtual float DmgMax { get { return 0.25f; } }

        public virtual int AttackSpeed { get { return 2000; } }

        public virtual bool CanAttack { get { return DmgMin > 0f; } }

        public virtual float GetRandomDmg { get { return RandomHelper.Next(DmgMin, DmgMax); } }
    }
}
