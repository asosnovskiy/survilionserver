﻿using System.Collections.Generic;
using JsonFx.Json;
using SurvCommon;

namespace SurvGameServer.Database.Data
{
    /// <summary>
    /// Settings for Drop chance
    /// </summary>
    public class MiningData
    {
        public virtual int Id { get; set; }

        public virtual ObjectsDic Type { get { return (ObjectsDic)Id; } }

        public virtual string Drop { get; set; }
        public virtual string AllowInstruments { get; set; }

        private List<ObjectsDic> _allowInstruments;
        private List<DropItemData> _drop;

        public virtual List<ObjectsDic> GetAllowInstruments()
        {
            if (_allowInstruments != null)
                return _allowInstruments;

            _allowInstruments = new List<ObjectsDic>();

            var json = JsonReader.Deserialize(AllowInstruments) as IDictionary<string, object>;

            var instruments = json["ids"] as int[];

            if (instruments != null)
                foreach (var id in instruments)
                {
                    _allowInstruments.Add((ObjectsDic)id);
                }

            return _allowInstruments;
        }

        public virtual List<DropItemData> GetDrop()
        {
            if (_drop != null)
                return _drop;
            _drop = new List<DropItemData>();

            var json = JsonReader.Deserialize(Drop) as IDictionary<string, object>;
            /*
            [{"type":1,"chance":5000,"maxdropcount":1,"maxcount":20,"infinity":0,"fruit":0}]
             */
            var drop = (object[])json["drop"];
            foreach (Dictionary<string, object> item in drop)
            {
                _drop.Add(new DropItemData
                    {
                        ItemType = (ObjectsDic)(int)item["type"],
                        Chance = (int)item["chance"],
                        MaxDropCount = (int)item["maxdropcount"],
                        MaxCount = (int)item["maxcount"],
                        Infinity = (int)item["infinity"] == 1,
                        IsFruit = (int)item["fruit"] == 1
                    });
            }



            return _drop;
        }
    }

    public class DropItemData
    {
        public ObjectsDic ItemType { get; set; }
        public int Chance { get; set; } //шанс х / 10000
        public int MaxDropCount { get; set; }
        public int MaxCount { get; set; } //запас
        public bool Infinity { get; set; } //бесконечен ли ресурс?
        public bool IsFruit { get; set; } //плод
    }
}
