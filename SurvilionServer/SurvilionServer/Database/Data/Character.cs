﻿using System;
using System.Collections.Generic;
using System.Linq;
using JsonFx.Json;
using SurvCommon;
using SurvCommon.Items;
using SurvCommon.Serialization;
using SurvGameServer.Factories;

namespace SurvGameServer.Database.Data
{
    public class Character
    {
        public virtual long Id { get; set; }
        public virtual Account Account { get; set; }
        public virtual string Name { get; set; }

        public virtual int Money { get; set; }

        public virtual bool Selected { get; set; }

        public virtual byte Sex { get; set; }

        public virtual float Hp { get; set; }
        public virtual float Hunger { get; set; }
        public virtual float Drought { get; set; }
        public virtual float Tiredness { get; set; }

        public virtual float PosX { get; set; }
        public virtual float PosY { get; set; }
        public virtual float PosZ { get; set; }
        public virtual float RotY { get; set; }

        public virtual string Inventory { get; set; }
        public virtual string Equipped { get; set; }
        public virtual string Learning { get; set; }

        public virtual DateTime DieTime { get; set; }

        public virtual CharacterItem BuildCharacterItem()
        {
            //add 2 seconds for transfering delay.
            var dieTime = DieTime > DateTime.Now ? (int)Math.Ceiling((DieTime - DateTime.Now).TotalSeconds) + 2 : 0;

            return new CharacterItem
                {
                    Id = Id,
                    Name = Name,
                    Selected = Selected,
                    DieTime = Hp <= 0f ? dieTime : 0
                };
        }

        public virtual List<GameItem> GetInventoryItems()
        {
            var items = new List<GameItem>();
            for (int i = 0; i < GameConfig.InventoryCount; i++)
                items.Add(null);

            var json = JsonReader.Deserialize(Inventory) as IDictionary<string, object>;
            var objs = (object[])json["items"];
            foreach (IDictionary<string, object> item in objs)
            {
                var gameItem = GameItemFactory.Instance.BuildGameItem((ObjectsDic)(int)item["type"],
                                                                      (int)item["count"],
                                                                      (int)item["dur"]);

                var slot = (int)item["index"];
                items[slot] = gameItem;
            }

            return items;
        }

        public virtual Dictionary<ItemSlot, GameItem> GetEquippedItems()
        {
            var items = new Dictionary<ItemSlot, GameItem>();
            for (int i = 1; i < (int)ItemSlot._LastDoNotUse; i++)
                items.Add((ItemSlot)i, null);


            var json = JsonReader.Deserialize(Equipped) as IDictionary<string, object>;
            var objs = (object[])json["equipped"];

            foreach (IDictionary<string, object> item in objs)
            {
                var gameItem = GameItemFactory.Instance.BuildGameItem((ObjectsDic)(int)item["type"],
                                                                      1,
                                                                      (int)item["dur"]);

                var slot = (int)item["index"];
                items[(ItemSlot)slot] = gameItem;
            }

            return items;
        }

        public virtual List<ObjectsDic> GetLearnedObjs()
        {
            var json = JsonReader.Deserialize(Learning) as IDictionary<string, object>;
            var list = json["learned"] as int[];

            var objs = new List<ObjectsDic>();
            if (list != null)
                objs.AddRange(list.Select(n => (ObjectsDic) n));

            return objs;
        }

        public virtual List<SkillType> GetLearnedSkills()
        {
            var json = JsonReader.Deserialize(Learning) as IDictionary<string, object>;
            var list = json["skills"] as int[];

            var objs = new List<SkillType>();
            if (list != null)
                objs.AddRange(list.Select(n => (SkillType) n));

            return objs;
        }
    }
}
