﻿using FluentNHibernate.Mapping;
using SurvCommon.Items;

namespace SurvGameServer.Database.Mapping
{
    public class GameItemDataMap:ClassMap<GameItemData>
    {
        public GameItemDataMap()
        {
            Id(x => x.Id).Column("type");

            Map(x => x.IsMulticount).Column("multicount");
            Map(x => x.MaxDurability).Column("dur");
            Map(x => x.MinDmg).Column("mindmg");
            Map(x => x.MaxDmg).Column("maxdmg");
            Map(x => x.Speed).Column("speed");
            Map(x => x.Distance).Column("dist");
            Map(x => x.RegenHealth).Column("reghp");
            Map(x => x.RegenTiredness).Column("regtiredness");
            Map(x => x.RegenHunger).Column("reghunger");
            Map(x => x.RegenDrought).Column("regdrought");
            Map(x => x.Slot).Column("slot");
            Map(x => x.Modificators).Column("modificators");

            Table("items_configurations");
        }
    }
}
