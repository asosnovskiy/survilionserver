﻿using FluentNHibernate.Mapping;
using SurvGameServer.Database.Data;

namespace SurvGameServer.Database.Mapping
{
    public class MobDataMap:ClassMap<MobData>
    {
        public MobDataMap()
        {
            Id(x => x.Id).Column("type");
            Map(x => x.Speed).Column("speed");
            Map(x => x.MaxHp).Column("max_hp");
            Map(x => x.DieTimer).Column("die_timer");
            Map(x => x.RespawnTimer).Column("respawn_timer");

            Table("mob_configurations");
        }
    }
}
