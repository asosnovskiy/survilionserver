﻿using FluentNHibernate.Mapping;
using SurvCommon.Items;

namespace SurvGameServer.Database.Mapping
{
    public class BuildDataMap:ClassMap<BuildData>
    {
        public BuildDataMap()
        {
            Id(x => x.Id).Column("type");
            Map(x => x.Data).Column("data");

            Table("build_configurations");
        }
    }
}
