﻿using FluentNHibernate.Mapping;
using SurvGameServer.Database.Data;

namespace SurvGameServer.Database.Mapping
{
    public class CharacterMap:ClassMap<Character>
    {
        public CharacterMap()
        {
            Id(x => x.Id).Column("id");
            References(x => x.Account).Column("acc_id");
            Map(x => x.Name).Column("name");
            Map(x => x.Money).Column("money");
            Map(x => x.Sex).Column("sex");
            Map(x => x.Selected).Column("selected");

            Map(x => x.Hp).Column("hp");
            Map(x => x.Hunger).Column("hunger");
            Map(x => x.Drought).Column("drought");
            Map(x => x.Tiredness).Column("tiredness");

            Map(x => x.PosX).Column("pos_x");
            Map(x => x.PosY).Column("pos_y");
            Map(x => x.PosZ).Column("pos_z");
            Map(x => x.RotY).Column("rot_y");

            Map(x => x.Equipped).Column("equipped");
            Map(x => x.Inventory).Column("inventory");
            Map(x => x.Learning).Column("learning");
            Map(x => x.DieTime).Column("die_time");

            Table("characters");
        }
    }
}
