﻿using FluentNHibernate.Mapping;
using SurvCommon.Items;

namespace SurvGameServer.Database.Mapping
{
    public class SkillDataMap:ClassMap<SkillData>
    {
        public SkillDataMap()
        {
            Id(x => x.Id).Column("type");
            Map(x => x.Data).Column("data");

            Table("skill_configurations");
        }
    }
}
