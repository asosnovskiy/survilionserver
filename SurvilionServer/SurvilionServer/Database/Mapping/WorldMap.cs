﻿using FluentNHibernate.Mapping;
using SurvGameServer.Database.Data;

namespace SurvGameServer.Database.Mapping
{
    public class WorldMap:ClassMap<World>
    {
        public WorldMap()
        {
            Id(x => x.Id).Column("id");
            Map(x => x.Name).Column("name");
            Map(x => x.WorldTime).Column("world_time");
            Map(x => x.Data).Column("data");

            Table("worlds");
        }
    }
}
