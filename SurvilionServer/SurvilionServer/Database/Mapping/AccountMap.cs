﻿using FluentNHibernate.Mapping;
using SurvGameServer.Database.Data;

namespace SurvGameServer.Database.Mapping
{
    public class AccountMap:ClassMap<Account>
    {
        public AccountMap()
        {
            Cache.Region("Account").ReadWrite();

            Id(id => id.Id).Column("id");
            Map(x => x.SocialId).Column("social_id");

            Table("accounts");
        }
    }
}
