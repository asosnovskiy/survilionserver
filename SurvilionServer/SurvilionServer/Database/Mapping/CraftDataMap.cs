﻿using FluentNHibernate.Mapping;
using SurvCommon.Items;

namespace SurvGameServer.Database.Mapping
{
    public class CraftDataMap : ClassMap<CraftData>
    {
        public CraftDataMap()
        {
            Id(x => x.Id).Column("type");
            Map(x => x.Data).Column("data");

            Table("craft_configurations");
        }
    }
}
