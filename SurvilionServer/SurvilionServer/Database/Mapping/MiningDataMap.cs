﻿using FluentNHibernate.Mapping;
using SurvGameServer.Database.Data;

namespace SurvGameServer.Database.Mapping
{
    public class MiningDataMap:ClassMap<MiningData>
    {
        public MiningDataMap()
        {
            Id(x => x.Id).Column("type");
            Map(x => x.AllowInstruments).Column("allow_instruments");
            Map(x => x.Drop).Column("drop");

            Table("mining_configurations");
        }
    }
}
