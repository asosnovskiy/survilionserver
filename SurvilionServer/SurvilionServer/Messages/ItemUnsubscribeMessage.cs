﻿using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Network;
using SurvGameServer.Events;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class ItemUnsubscribeMessage:RegionMessage
    {
        private readonly Region _oldRegion;
        private readonly Region _newRegion;
        private readonly MapObject _mapObject;

        public Region OldRegion{get { return _oldRegion; }}

        public ItemUnsubscribeMessage(MapObject mapObject, Region oldRegion, Region newRegion)
        {
            _oldRegion = oldRegion;
            _newRegion = newRegion;
            _mapObject = mapObject;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            //если один и тот же регион
            //if (_mapObject.CurrentWorldRegion == OldRegion)
            //    return;

            lock (interestArea.SyncRoot)
            {
                //если регион изменился, но на него уже подписана область
                if (interestArea.WorldRegions.ContainsKey(_newRegion))
                    return;
            }

            var packet = new Packet(16);
            packet.Addint64(_mapObject.Id);
            packet.Addint64(-1);

            //объект перешел в другой регион, который не интересен области => удаляем объект
            var eventData = new EventData((byte)EventCode.ItemRemoved, new ItemRemoved
            {
               Ids = packet.Buffer
            });

            (interestArea as PlayerInterestArea).Peer.SendEvent(eventData,
                                        new SendParameters { Encrypted = false, ChannelId = 0, Unreliable = false });
        }

        public override void OnItemReceive(MapObject mapObject)
        {

        }
    }
}
