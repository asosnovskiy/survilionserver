﻿using ExitGames.Diagnostics.Counter;
using Photon.SocketServer;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class ItemEventMessage
    {
        public static readonly CountsPerSecondCounter CounterEventReceive = new CountsPerSecondCounter("ItemEventMessage.Receive");

        public static readonly CountsPerSecondCounter CounterEventSend = new CountsPerSecondCounter("ItemEventMessage.Send");

        private readonly IEventData eventData;

        private readonly SendParameters sendParameters;

        private readonly MapObject source;

        public ItemEventMessage(MapObject source, IEventData eventData, SendParameters sendParameters)
        {
            this.source = source;
            this.eventData = eventData;
            this.sendParameters = sendParameters;
        }

        public IEventData EventData
        {
            get
            {
                return this.eventData;
            }
        }

        public SendParameters SendParameters
        {
            get
            {
                return this.sendParameters;
            }
        }

        public MapObject Source
        {
            get
            {
                return this.source;
            }
        }
    }
}
