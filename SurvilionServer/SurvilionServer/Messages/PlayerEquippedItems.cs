﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using SurvCommon;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class PlayerEquippedItems:RegionMessage
    {
        private readonly IEventData _eventData;

        public PlayerEquippedItems(IEventData eventData)
        {
            _eventData = eventData;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            (interestArea as PlayerInterestArea).Peer.SendEvent(_eventData,
                                        new SendParameters
                                            {
                                                ChannelId = (byte) MessageChannel.Default,
                                                Encrypted = false,
                                                Unreliable = false
                                            });
        }

        public override void OnItemReceive(MapObject mapObject)
        {

        }
    }
}
