﻿using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Extensions;
using SurvCommon.Network;
using SurvGameServer.Events;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class ItemSubscribeMessage : RegionMessage
    {
        private readonly Region _oldRegion;
        private readonly MapObject _mapObject;

        public ItemSubscribeMessage(MapObject mapObject, Region oldRegion)
        {
            _mapObject = mapObject;
            _oldRegion = oldRegion;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            if (_mapObject.CurrentWorldRegion == null)
                return;

            if (_mapObject.CurrentWorldRegion == _oldRegion)
                return;

            if (_oldRegion != null)
            {
                lock (interestArea.SyncRoot)
                {
                    if (interestArea.WorldRegions.ContainsKey(_oldRegion))
                        return;
                }
            }
            var packet = new Packet(1200);
            packet.Addint64(_mapObject.Id);
            packet.Addint16((short)_mapObject.Type);
            packet.Addfloat(_mapObject.Position.x);
            packet.Addfloat(_mapObject.Position.y);
            packet.Addfloat(_mapObject.RotY);
            var spawnData = _mapObject.GetSpawnData();
            if (spawnData != null)
                packet.Addbytes(spawnData);

            packet.Addint64(-1);

            var eventData = new EventData((byte)EventCode.ItemSpowned, new ItemSpawned
                {
                    Data = packet.Buffer
                });

            (interestArea as PlayerInterestArea).Peer.SendEvent(eventData,
                                        new SendParameters { ChannelId = 0, Encrypted = false, Unreliable = false });
        }

        public override void OnItemReceive(MapObject mapObject)
        {

        }
    }
}
