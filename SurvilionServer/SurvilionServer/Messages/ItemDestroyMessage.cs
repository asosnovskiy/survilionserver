﻿using System.Collections.Generic;
using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Network;
using SurvGameServer.Events;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class ItemDestroyMessage : RegionMessage
    {
        private readonly MapObject _mapObject;

        public ItemDestroyMessage(MapObject mapObject)
        {
            _mapObject = mapObject;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            var packet = new Packet(16);
            packet.Addint64(_mapObject.Id);
            packet.Addint64(-1);

            var eventData = new EventData((byte)EventCode.ItemRemoved, new ItemRemoved
            {
                Ids = packet.Buffer
            });

            (interestArea as PlayerInterestArea).Peer.SendEvent(eventData,
                                        new SendParameters { Encrypted = false, ChannelId = 0, Unreliable = false });
        }

        public override void OnItemReceive(MapObject mapObject)
        {

        }
    }
}
