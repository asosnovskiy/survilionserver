﻿using Photon.SocketServer;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class BuildingProccessInfoUpdate:RegionMessage
    {
        private readonly IEventData _eventData;

        public BuildingProccessInfoUpdate(IEventData eventData)
        {
            _eventData = eventData;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            (interestArea as PlayerInterestArea).Peer.SendEvent(_eventData,
                                        new SendParameters {ChannelId = 0, Encrypted = false, Unreliable = false});
        }

        public override void OnItemReceive(MapObject mapObject)
        {
        }
    }
}
