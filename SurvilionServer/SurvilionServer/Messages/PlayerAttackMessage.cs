﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using SurvCommon;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class PlayerAttackMessage:RegionMessage
    {
        private readonly IEventData _eventData;

        private static readonly SendParameters _sendParameters = new SendParameters()
            {
                ChannelId = (byte) MessageChannel.Default,
                Encrypted = false,
                Unreliable = false
            };

        public PlayerAttackMessage(IEventData eventData)
        {
            _eventData = eventData;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            (interestArea as PlayerInterestArea).Peer.SendEvent(_eventData, _sendParameters);
        }

        public override void OnItemReceive(MapObject mapObject)
        {
            //throw new NotImplementedException();
        }
    }
}
