﻿using ExitGames.Logging;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public abstract class RegionMessage
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        public abstract void OnInterestAreaReceive(InterestArea interestArea);

        public abstract void OnItemReceive(MapObject mapObject);
    }
}
