﻿using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Extensions;
using SurvGameServer.Events;
using SurvGameServer.WorldSystem;
using UnityEngine;

namespace SurvGameServer.Messages
{
    public class ItemPositionMessage:RegionMessage
    {
        private readonly Vector2 Position;
        private readonly MapObject MapObject;

        public ItemPositionMessage(MapObject source, Vector2 position)
        {
            MapObject = source;
            Position = position;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            MessageCounters.CounterSend.Increment();

            var eventData = new EventData((byte) EventCode.ItemMoved, new ItemMoved
                {
                    Id = MapObject.Id,
                    Position = Position.ToArray(),
                    RotY = MapObject.RotY
                });

            (interestArea as PlayerInterestArea).Peer.SendEvent(eventData,
                                        new SendParameters
                                            {
                                                Encrypted = false,
                                                Unreliable = true,
                                                ChannelId = (byte) MessageChannel.Move
                                            });
        }

        public override void OnItemReceive(MapObject mapObject)
        {
            
        }
    }
}
