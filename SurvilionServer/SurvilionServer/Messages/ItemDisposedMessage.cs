﻿using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public sealed class ItemDisposedMessage
    {
        public readonly MapObject MapObject;

        public ItemDisposedMessage(MapObject mapObject)
        {
            MapObject = mapObject;
        }
    }
}
