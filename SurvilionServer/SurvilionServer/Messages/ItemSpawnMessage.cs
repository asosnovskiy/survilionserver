﻿using System.Collections.Generic;
using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Extensions;
using SurvGameServer.Events;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.Messages
{
    public class ItemSpawnMessage : RegionMessage
    {
        private readonly MapObject _mapObject;

        public ItemSpawnMessage(MapObject mapObject)
        {
            _mapObject = mapObject;
        }

        public override void OnInterestAreaReceive(InterestArea interestArea)
        {
            //if (_player != null)
            //    return;

            //var eventData = new EventData((byte)EventCode.ItemSpowned, new ItemSpawned
            //{
            //    Id = _mapObject.Id,
            //    Type = (short)_mapObject.Type,
            //    Position = _mapObject.Position.ToArray(),
            //    RotY = _mapObject.RotY,
            //    Data = _mapObject.GetSpawnData()
            //});

            //(interestArea as PlayerInterestArea).Peer.SendEvent(eventData,
            //                            new SendParameters { ChannelId = 0, Encrypted = false, Unreliable = false });
        }

        public override void OnItemReceive(MapObject mapObject)
        {
            //if (_player == null)
            //    return;

            //if (mapObject == _mapObject)
            //    return;

            //var eventData = new EventData((byte)EventCode.ItemSpowned, new ItemSpawned
            //{
            //    Id = mapObject.Id,
            //    Type = (short)mapObject.Type,
            //    Position = mapObject.Position.ToArray(),
            //    RotY = mapObject.RotY,
            //    Data = mapObject.GetSpawnData()
            //});

            //_player.SendEvent(eventData,
            //                    new SendParameters { ChannelId = 0, Encrypted = false, Unreliable = false });
        }
    }
}
