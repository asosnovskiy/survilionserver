﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class ItemRemoved
    {
        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public byte[] Ids { get; set; }
    }
}
