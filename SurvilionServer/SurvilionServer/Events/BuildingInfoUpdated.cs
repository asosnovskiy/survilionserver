﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class BuildingInfoUpdated
    {
        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public long BuildingId { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemData, IsOptional = false)]
        public byte[] BuildingData { get; set; }
    }
}
