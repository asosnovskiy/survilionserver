﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class ItemSpawned
    {
        //[DataMember(Code = (byte)GameParameterKeys.ItemId,IsOptional = false)]
        //public long Id { get; set; }

        //[DataMember(Code =(byte)GameParameterKeys.ItemType,IsOptional = false)]
        //public short Type { get; set; }

        //[DataMember(Code = (byte)GameParameterKeys.Position, IsOptional = false)]
        //public float[] Position { get; set; }

        //[DataMember(Code = (byte)GameParameterKeys.Rotation,IsOptional = false)]
        //public float RotY { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemData, IsOptional = true)]
        public byte[] Data { get; set; }
    }
}
