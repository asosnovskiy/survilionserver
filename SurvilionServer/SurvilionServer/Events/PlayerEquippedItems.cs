﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class PlayerEquippedItems
    {
        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public long AvatarId { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemData, IsOptional = false)]
        public byte[] Data { get; set; }
    }
}
