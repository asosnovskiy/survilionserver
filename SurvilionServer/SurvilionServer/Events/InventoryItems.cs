﻿using System.Collections;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class InventoryItems
    {
        [DataMember(Code = (byte)GameParameterKeys.InventoryItems, IsOptional = true)]
        public Hashtable Items { get; set; }
    }
}
