﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class PlayerDie
    {
        [DataMember(Code=(byte)GameParameterKeys.ItemId,IsOptional = false)]
        public long AvatarId { get; set; }
    }
}
