﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class ChatHistory
    {
        [DataMember(Code = (byte)GameParameterKeys.ChatMessage, IsOptional = false)]
        public string[] Messages { get; set; }
    }
}
