﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class ChatMessage
    {
        [DataMember(Code = (byte)GameParameterKeys.ChatMessage, IsOptional = false)]
        public string Message { get; set; }
    }
}
