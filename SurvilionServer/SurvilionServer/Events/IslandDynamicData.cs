﻿using System.Collections;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Events
{
    public class IslandDynamicData
    {
        [DataMember(Code = (byte)GameParameterKeys.DynamicData, IsOptional = false)]
        public Hashtable Data { get; set; }
    }
}
