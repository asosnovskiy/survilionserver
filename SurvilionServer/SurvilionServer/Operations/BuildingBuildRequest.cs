﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class BuildingBuildRequest:Operation
    {
        public BuildingBuildRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public long BuildingId { get; set; }
    }
}
