﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class ItemMoveRequest : Operation
    {
        public ItemMoveRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        //[DataMember(Code = (byte) GameParameterKeys.ItemId, IsOptional = false)]
        //public long Id { get; set; }

        [DataMember(Code = (byte) GameParameterKeys.Position, IsOptional = false)]
        public float[] Position { get; set; }

        [DataMember(Code = (byte) GameParameterKeys.Rotation, IsOptional = false)]
        public float RotY { get; set; }
    }
}
