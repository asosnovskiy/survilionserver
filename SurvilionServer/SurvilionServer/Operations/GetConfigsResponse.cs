﻿using System.Collections;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class GetConfigsResponse
    {
        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = true)]
        public Hashtable ItemsConfig { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemType, IsOptional = true)]
        public Hashtable BuildsConfig { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemCount, IsOptional = true)]
        public Hashtable SkillsConfig { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemData, IsOptional = true)]
        public Hashtable CraftConfig { get; set; }
    }
}
