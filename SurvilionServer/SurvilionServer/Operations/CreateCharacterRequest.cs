﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class CreateCharacterRequest:Operation
    {
        public CreateCharacterRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte)GameParameterKeys.CharacterName, IsOptional = false)]
        public string Name { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.CharacterSex, IsOptional = false)]
        public byte Sex { get; set; }
    }
}
