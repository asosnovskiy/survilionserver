﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;

namespace SurvGameServer.Operations
{
    class LoadLevelRequest:Operation
    {
        public LoadLevelRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }
    }
}
