﻿using System.Collections;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class LoadLevelResponse
    {
        //send MapData to user
        [DataMember(Code = (byte)GameParameterKeys.Position, IsOptional = false)]
        public float[] Position { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.Rotation, IsOptional = false)]
        public float RotY { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.CharacterParameters, IsOptional = false)]
        public float[] CharacterParameters { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.StaticData, IsOptional = false)]
        public string StaticData { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.StaticItems, IsOptional = true)]
        public Hashtable StaticItems { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public long AvatarId { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemData, IsOptional = false)]
        public byte[] Skills { get; set; }
    }
}