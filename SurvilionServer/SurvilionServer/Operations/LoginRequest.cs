﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    class LoginRequest:Operation
    {
        public LoginRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        #region Properties

        [DataMember(Code = (byte)GameParameterKeys.SocialId, IsOptional = false)]
        public long SocialId { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.GameVersion,IsOptional = false)]
        public string GameVersion { get; set; }

        #endregion
    }
}
