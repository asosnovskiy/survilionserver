﻿using System.Collections;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class GetCharacterListResponse
    {
        [DataMember(Code = (byte)GameParameterKeys.CharacterList, IsOptional = false)]
        public Hashtable Characters { get; set; }
    }
}
