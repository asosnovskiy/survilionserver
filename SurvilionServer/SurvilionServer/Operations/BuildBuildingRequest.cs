﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class BuildBuildingRequest : Operation
    {
        public BuildBuildingRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte)GameParameterKeys.ItemType, IsOptional = false)]
        public short BuildingType { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.Rotation, IsOptional = false)]
        public float BuildingRot { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.Position,IsOptional = false)]
        public float[] BuildingPos { get; set; }
    }
}
