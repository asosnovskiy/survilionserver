﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class LoginResponse
    {
        [DataMember(Code = (byte)GameParameterKeys.ErrorCode)]
        public ReturnCode ErrorCode { get; set; }
    }
}