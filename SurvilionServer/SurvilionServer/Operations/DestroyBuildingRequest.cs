﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class DestroyBuildingRequest: Operation
    {
        public DestroyBuildingRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public long BuildingId { get; set; }
    }
}
