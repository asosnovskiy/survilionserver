﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class DeleteCharacterRequest:Operation
    {
        public DeleteCharacterRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte)GameParameterKeys.CharacterId, IsOptional = false)]
        public long Id { get; set; }
    }
}
