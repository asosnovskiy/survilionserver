﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class PlayerMineRequest:Operation
    {
        public PlayerMineRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte) GameParameterKeys.ItemId, IsOptional = false)]
        public long ItemId { get; set; }
    }
}
