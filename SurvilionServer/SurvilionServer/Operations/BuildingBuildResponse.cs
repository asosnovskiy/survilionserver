﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class BuildingBuildResponse
    {
        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public long BuildingId { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ResourceType, IsOptional = true)]
        public short ResourceType { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ResourceCount, IsOptional = true)]
        public byte ResourceCount { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemData, IsOptional = true)]
        public byte[] Data { get; set; }
    }
}
