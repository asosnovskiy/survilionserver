﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;

namespace SurvGameServer.Operations
{
    class GetCharacterListRequest:Operation
    {
        public GetCharacterListRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }
    }
}
