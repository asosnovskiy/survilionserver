﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class ChatMessageRequest : Operation
    {
        public ChatMessageRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte) GameParameterKeys.ChatMessage, IsOptional = false)]
        public string Message { get; set; }
    }
}
