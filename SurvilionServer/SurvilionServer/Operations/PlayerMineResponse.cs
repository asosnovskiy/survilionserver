﻿using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class PlayerMineResponse
    {
        [DataMember(Code = (byte)GameParameterKeys.ItemId, IsOptional = false)]
        public long ItemId { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemType, IsOptional = false)]
        public short ItemType { get; set; }

        [DataMember(Code = (byte)GameParameterKeys.ItemCount,IsOptional = false)]
        public byte ItemCount { get; set; }
    }
}
