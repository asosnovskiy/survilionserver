﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using SurvCommon;

namespace SurvGameServer.Operations
{
    public class ItemDropRequest:Operation
    {
        public ItemDropRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }

        [DataMember(Code = (byte) GameParameterKeys.ItemId, IsOptional = false)]
        public long ItemId { get; set; }

        [DataMember(Code =(byte)GameParameterKeys.ItemCount,IsOptional = false)]
        public byte ItemCount { get; set; }
    }
}

