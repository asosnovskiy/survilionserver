﻿using Photon.SocketServer;
using Photon.SocketServer.Rpc;

namespace SurvGameServer.Operations
{
    class ExitLevelRequest:Operation
    {
        public ExitLevelRequest(IRpcProtocol protocol, OperationRequest operationRequest)
            : base(protocol, operationRequest)
        {
        }
    }
}
