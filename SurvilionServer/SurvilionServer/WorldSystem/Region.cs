﻿using System.Collections.Generic;
using Photon.SocketServer.Concurrency;
using SurvGameServer.Messages;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    public class Region : MessageChannel<RegionMessage>
    {
        public readonly Rect Rectangle;

        public readonly List<MapObject> Objects = new List<MapObject>(32);

        public Region(Rect rect)
            : base(MessageCounters.CounterSend)
        {
            Rectangle = rect;
        }

        public void Add(MapObject mapObject)
        {
            lock (Objects)
            {
                Objects.Add(mapObject);
            }
        }

        public void Remove(MapObject mapObject)
        {
            lock (Objects)
            {
                Objects.Remove(mapObject);
            }
        }
    }
}
