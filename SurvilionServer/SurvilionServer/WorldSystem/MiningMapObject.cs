﻿using System;
using System.Collections.Generic;
using System.Linq;
using SurvCommon;
using SurvCommon.Helpers;
using SurvGameServer.Caches;
using SurvGameServer.Database.Data;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    public class MiningMapObject : MapObject
    {
        private readonly List<DropItem> _drop;  //сколько осталось
        private MiningData MiningData { get { return MiningDataCache.GetItem(Type); } }

        //Есть ли дроп
        public bool CanDrop
        {
            get { return _drop.Count > 0; }
        }

        public MiningMapObject(ObjectsDic type, Vector3 position, float rotY,
            List<DropItem> drop) :
            base(type, position, rotY)
        {
            _drop = drop;
        }

        /// <summary>
        /// Получаем дроп
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public DropItem GetDrop(Player player)
        {
            //int currentMiningCount = 0;

            if (Log.IsDebugEnabled)
                Log.Debug(Id + ": possible drop count:" + _drop.Count);

            //формула для получения дропа
            for (int i = _drop.Count - 1; i >= 0; i--)
            {
                var dropData = _drop[i];

                //уже выпало  максимум ресурсов
                //if (currentMiningCount == GameConfig.MAXIMUM_MINIG_COUNT)
                //{
                //    Logger.Log(Id + ": max drop count!");
                //    break;
                //}

                //выпадет ли
                var randChance = RandomHelper.Next(1, GameConfig.MAX_DROP_CHANCE);
                int dropChance;
                int maxDropCount;
                GetDropForItem(player, dropData, out dropChance, out maxDropCount);

                bool isChance = randChance <= dropChance;

                if (Log.IsDebugEnabled)
                    Log.Debug("rand chance:" + randChance + ", chance:" + dropChance);

                if (!isChance) //не выпало
                {
                    continue;
                }

                //сколько выпало
                var dropCount = RandomHelper.Next(1, maxDropCount + 1);

                if (Log.IsDebugEnabled)
                    Log.Debug("chance:" + isChance + ", dropCount:" + dropCount + ", name:" + dropData.ItemType);

                if (dropCount <= 0)
                    throw new Exception("Drop count <= 0!");

                //currentMiningCount += dropCount;

                //отнимаем дроп
                dropData.Count -= dropCount;

                if (dropData.Count < 0)
                    throw new Exception("Dropdata count < 0!");

                if (dropData.Count == 0)
                {
                    _drop.RemoveAt(i);
                }

                return new DropItem
                {
                    ItemType = dropData.ItemType,
                    Count = dropCount
                };
            }

            return null;
        }

        /// <summary>
        /// Возвращает True, если одет инструмент для добычи
        /// </summary>
        /// <returns>True - одет инструмент</returns>
        public bool CheckAccessInstrument(Player player)
        {
            if (MiningData.GetAllowInstruments().Count == 0)
                return true;

            return MiningData.GetAllowInstruments().Contains(player.CharacterData.UsedItem.Type);
        }

        private void GetDropForItem(Player player, DropItem item, out int chance, out int count)
        {
            var itemDropData = MiningData.GetDrop().FirstOrDefault(n => n.ItemType == item.ItemType);
            if (itemDropData == null)
                throw new Exception("No drop item in MiningData DB!");

            chance = itemDropData.Chance;
            count = Mathf.Min(item.Count, itemDropData.MaxDropCount);

            //если есть что-то в руке,то проверяем модификатор
            var handItem = player.CharacterData.UsedItem;

            if (handItem.Type != ObjectsDic.Hands)
            {
                //используем модификатор для данного ресурса если он есть
                var modificator = handItem.Data.LoadModificators().FirstOrDefault(n => n.Resource == item.ItemType);
                if (modificator != null)
                {
                    //chance
                    chance = itemDropData.Chance + modificator.Chance;
                    if (chance > GameConfig.MAX_DROP_CHANCE)
                        chance = GameConfig.MAX_DROP_CHANCE;
                    else if (chance < 1)
                        chance = 0;

                    //count
                    if (modificator.Count > 0)
                        count = Mathf.Min(item.Count, itemDropData.MaxDropCount + modificator.Count);
                    else if (modificator.Count < 0)
                    {
                        var maxDropCount = itemDropData.MaxDropCount + modificator.Count;
                        if (maxDropCount < 1)
                            maxDropCount = 1;

                        count = Mathf.Min(item.Count, maxDropCount);
                    }
                }
            }

            //ONLY ONE ITEM
            count = 1;
        }
    }
}
