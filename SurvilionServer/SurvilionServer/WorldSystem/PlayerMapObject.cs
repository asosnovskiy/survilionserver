﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SurvCommon;
using SurvCommon.Network;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    public class PlayerMapObject : MapObject
    {
        private readonly CharacterData _characterData;

        public PlayerMapObject(CharacterData characterData, Vector3 position, float rotY)
            : base(ObjectsDic.PlayerPref, position, rotY)
        {
            _characterData = characterData;
        }

        public override byte[] GetSpawnData()
        {
            return GetEquippedItems();
        }

        public byte[] GetEquippedItems()
        {
            var packet = new Packet();
            foreach (var equippedItem in _characterData.EquippedItems.Values)
            {
                if (equippedItem != null)
                {
                    packet.Addint16((short)equippedItem.Type);
                }
            }
            packet.Addint16(-1);
            return packet.Buffer;
        }
    }
}
