﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SurvGameServer.WorldSystem
{
    public class WorldCache
    {
        public static WorldCache Instance = new WorldCache();

        private World _cachedFirstWorld;

        public readonly Dictionary<long,World> Worlds = new Dictionary<long, World>();  

        public void AddWorld(World world)
        {
            lock (Worlds)
            {
                if (!Worlds.ContainsKey(world.Id))
                    Worlds.Add(world.Id, world);
            }
        }

        public void RemoveWorld(World world)
        {
            lock (Worlds)
            {
                if (Worlds.ContainsKey(world.Id))
                    Worlds.Remove(world.Id);
            }
        }
    
        public World World()
        {
            if (_cachedFirstWorld != null)
                return _cachedFirstWorld;

            lock (Worlds)
            {
                if (Worlds.Count > 0)
                {
                    _cachedFirstWorld = Worlds.First().Value;
                    return _cachedFirstWorld;
                }

                throw new Exception("Not exist world!");
            }
        }

        public World FindWorldById(long id)
        {
            lock (Worlds)
            {
                return Worlds.FirstOrDefault(n => n.Key == id).Value;
            }
        }
    }
}
