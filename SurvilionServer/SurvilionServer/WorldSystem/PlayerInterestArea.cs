﻿using System.Linq;
using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Extensions;
using SurvCommon.Network;
using SurvGameServer.Events;

namespace SurvGameServer.WorldSystem
{
    public class PlayerInterestArea : InterestArea
    {
        public readonly Player Peer;

        public PlayerInterestArea(Player peer)
            : base(peer.RequestFiber)
        {
            Peer = peer;
        }

        public override void UpdateInterestManagement()
        {
            lock (SyncRoot)
            {
                if (AttachedItem != null)
                    Position = AttachedItem.Position;

                var regions = World.GetIntersactRegions(Position, ViewGridSize);

                var newRegions = regions.Where(region => !subscribedWorldRegions.Keys.Contains(region)).ToList();

                //сообщаем, чтобы все предметы в новых регионах были спавнуты для текущего игрока
                var packet = new Packet(1200);
                foreach (var newRegion in newRegions)
                {
                    lock (newRegion.Objects)
                    {
                        foreach (var obj in newRegion.Objects)
                        {
                            packet.Addint64(obj.Id);
                            packet.Addint16((short)obj.Type);
                            packet.Addfloat(obj.Position.x);
                            packet.Addfloat(obj.Position.y);
                            packet.Addfloat(obj.RotY);
                            var spawnData = obj.GetSpawnData();
                            if (spawnData != null)
                                packet.Addbytes(spawnData);
                        }
                    }

                    //newRegion.Publish(AttachedItem.GetSpawnMessage());
                }
                packet.Addint64(-1);
                if (packet.Buffer.Length > 1200)
                {
                    Log.InfoFormat("Packet spawnObject.length = " + packet.Buffer.Length);
                }
                var eventData = new EventData((byte)EventCode.ItemSpowned, new ItemSpawned
                {
                    Data = packet.Buffer
                });

                Peer.SendEvent(eventData,
                               new SendParameters { Encrypted = false, ChannelId = 0, Unreliable = false });

                SubscribeRegions(regions);

                var unsubscribeRegions = subscribedWorldRegions.Keys.Where(region => !regions.Contains(region)).ToList();

                packet = new Packet(1200);
                foreach (var unsubscribeRegion in unsubscribeRegions)
                {
                    lock (unsubscribeRegion.Objects)
                    {
                        foreach (var obj in unsubscribeRegion.Objects)
                        {
                            if (obj.Id == Peer.CharacterData.Avatar.Id)
                                packet.Addint64(0);
                            else
                                packet.Addint64(obj.Id);
                        }
                    }
                }

                packet.Addint64(-1);
                eventData = new EventData((byte)EventCode.ItemRemoved, new ItemRemoved
                {
                    Ids = packet.Buffer
                });

                Peer.SendEvent(eventData,
                               new SendParameters {Encrypted = false, ChannelId = 0, Unreliable = false});

                UnsubscribeRegions(unsubscribeRegions);
            }
        }

        protected override void Region_OnReceive(Messages.RegionMessage message)
        {
            message.OnInterestAreaReceive(this);
        }
    }
}
