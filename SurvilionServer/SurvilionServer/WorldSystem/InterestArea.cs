﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Concurrency.Fibers;
using ExitGames.Logging;
using Photon.SocketServer;
using Photon.SocketServer.Concurrency;
using SurvGameServer.Messages;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    public class InterestArea : IDisposable
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        #region Constants and Fields

        protected float ViewGridSize = 95;

        public readonly object SyncRoot = new object();
        protected readonly Dictionary<Region, IDisposable> subscribedWorldRegions;
        private readonly IFiber subscriptionManagementFiber;
        public Dictionary<Region, IDisposable> WorldRegions { get { return subscribedWorldRegions; } }

        protected readonly World World = WorldCache.Instance.World();

        #endregion

        public InterestArea(IFiber fiber)
        {
            subscribedWorldRegions = new Dictionary<Region, IDisposable>();
            subscriptionManagementFiber = fiber;
        }

        ~InterestArea()
        {
            Dispose(false);
        }

        #region Properties

        public MapObject AttachedItem { get; private set; }

        public Vector3 Position { get; protected set; }

        #endregion

        #region Public Methods

        public void AttachToItem(MapObject item)
        {
            if (AttachedItem != null)
            {
                throw new InvalidOperationException();
            }

            AttachedItem = item;
            Position = item.Position;
        }

        public void Detach()
        {
            AttachedItem = null;
        }

        public  virtual void UpdateInterestManagement()
        {
            lock (SyncRoot)
            {
                if (AttachedItem != null)
                    Position = AttachedItem.Position;

                var regions = World.GetIntersactRegions(Position, ViewGridSize);

                SubscribeRegions(regions);

                var unsubscribeRegions = subscribedWorldRegions.Keys.Where(region => !regions.Contains(region)).ToList();

                UnsubscribeRegions(unsubscribeRegions);
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

        #region Methods

        protected void ClearRegionSubscriptions()
        {
            foreach (var subscription in subscribedWorldRegions.Values)
            {
                subscription.Dispose();
            }

            subscribedWorldRegions.Clear();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                AttachedItem = null;
                ClearRegionSubscriptions();
            }
        }

        //что-то пришло от региона
        protected virtual void Region_OnReceive(RegionMessage message)
        {

        }

        protected void SubscribeRegions(IEnumerable<Region> regions)
        {
            foreach (Region region in regions)
            {
                if (subscribedWorldRegions.ContainsKey(region) == false)
                {
                    var subscription = region.Subscribe(subscriptionManagementFiber, Region_OnReceive);
                    subscribedWorldRegions.Add(region, subscription);
                    //region.Publish(snapShotRequest);
                }
            }
        }

        protected void UnsubscribeRegions(IEnumerable<Region> regions)
        {
            foreach (Region region in regions)
            {
                IDisposable subscription;
                if (subscribedWorldRegions.TryGetValue(region, out subscription))
                {
                    subscription.Dispose();
                    subscribedWorldRegions.Remove(region);
                }
            }
        }

        #endregion
    }
}
