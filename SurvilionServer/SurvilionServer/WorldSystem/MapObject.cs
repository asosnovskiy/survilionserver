﻿using System;
using System.Collections.Generic;
using System.Threading;
using ExitGames.Concurrency.Fibers;
using ExitGames.Logging;
using Photon.SocketServer;
using Photon.SocketServer.Concurrency;
using SurvCommon;
using SurvCommon.Extensions;
using SurvCommon.Serialization;
using SurvCommon.WorldSystem;
using SurvGameServer.Messages;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    /// <summary>
    /// Что-то в на острове.
    /// Специфические данные находятся в наследниках
    /// 
    /// </summary>
    public class MapObject : GSSimpleObject, IDisposable
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        private float _rotY;

        public bool Disposed { get; private set; }

        public float RotY
        {
            get { return _rotY; }
            set
            {
                _rotY = value;
            }
        }

        public Region CurrentWorldRegion { get; private set; }
        public int PropertiesRevision { get; set; }

        protected World World { get { return WorldCache.Instance.World(); } }

        protected readonly IFiber Fiber;
        private IDisposable CurrentWorldRegionSubscription;

        public MapObject(ObjectsDic type, Vector3 position, float rotY)
            : base(type, position)
        {
            _rotY = rotY;

            Fiber = new PoolFiber();
            Fiber.Start();

            Log.InfoFormat("Create item id:{0}, type:{1}", Id, Type);
        }

        #region disposing

        ~MapObject()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                SetCurrentWorldRegion(null);
                //disposeChannel.Publish(new ItemDisposedMessage(this));
                //eventChannel.Dispose();
                //disposeChannel.Dispose();
                //positionUpdateChannel.Dispose();
                Fiber.Dispose();
                Disposed = true;
            }
        }

        #endregion

        public MapObjectItem BuildMapObjectItem()
        {
            return new MapObjectItem
                {
                    Id = Id,
                    Type = (short)Type,
                    Position = Position.ToArray(),
                    Rotation = RotY
                };
        }

        public virtual void UpdateInterestManagement()
        {
            var oldRegion = CurrentWorldRegion;

            var newRegion = World.GetIntersactRegion(Position);


            if (oldRegion != null && oldRegion != newRegion)
            {
                //var unsubscribeMessage = GetUnsubscribeMessage();
                //_fiber.Schedule(() => UnsubscribeRegion(unsubscribeMessage), 1000);
                oldRegion.Publish(GetUnsubscribeMessage(newRegion));
            }

            if (SetCurrentWorldRegion(newRegion))
            {
                //newRegion.Publish(GetSpawnMessage());
                newRegion.Publish(GetSubscribeMessage(oldRegion));
            }
        }

        protected bool SetCurrentWorldRegion(Region newRegion)
        {
            // out of bounds
            if (newRegion == null)
            {
                // was not out of bounce before
                if (CurrentWorldRegion != null)
                {
                    CurrentWorldRegion.Remove(this);
                    CurrentWorldRegion = null;
                    CurrentWorldRegionSubscription.Dispose();
                    CurrentWorldRegionSubscription = null;
                }

                return false;
            }

            // was out of bounce before
            if (CurrentWorldRegion == null)
            {
                CurrentWorldRegionSubscription = newRegion.Subscribe(Fiber, Region_OnReceive);
                CurrentWorldRegion = newRegion;
                CurrentWorldRegion.Add(this);
                return true;
            }

            // current region changed
            if (newRegion != CurrentWorldRegion)
            {
                CurrentWorldRegion.Remove(this);
                IDisposable newSubscription = newRegion.Subscribe(Fiber, Region_OnReceive);
                CurrentWorldRegionSubscription.Dispose();
                CurrentWorldRegionSubscription = newSubscription;
                CurrentWorldRegion = newRegion;
                CurrentWorldRegion.Add(this);
                return true;
            }

            return false;
        }

        //private void UnsubscribeRegion(ItemUnsubscribeMessage message)
        //{
        //    if (disposed)
        //        return;

        //    if (CurrentWorldRegion != message.OldRegion)
        //    {
        //        if (message.OldRegion != null)
        //        {
        //            message.OldRegion.Publish(message);
        //        }
        //    }
        //}

        public ItemPositionMessage GetPositionUpdateMessage()
        {
            return new ItemPositionMessage(this, Position);
        }
        public ItemSpawnMessage GetSpawnMessage()
        {
            return new ItemSpawnMessage(this);
        }
        public ItemDestroyMessage GetDestroyMessage()
        {
            return new ItemDestroyMessage(this);
        }

        public ItemSubscribeMessage GetSubscribeMessage(Region oldRegion)
        {
            return new ItemSubscribeMessage(this, oldRegion);
        }
        public ItemUnsubscribeMessage GetUnsubscribeMessage(Region newRegion)
        {
            return new ItemUnsubscribeMessage(this, CurrentWorldRegion, newRegion);
        }

        private void Region_OnReceive(RegionMessage message)
        {
            if (Disposed)
            {
                return;
            }

            message.OnItemReceive(this);
        }

        public virtual byte[] GetSpawnData()
        {
            return null;
        }
    }
}
