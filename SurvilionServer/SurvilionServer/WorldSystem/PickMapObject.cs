﻿using SurvCommon.Items;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    /// <summary>
    /// То,  что можно поднять с земли. Может быть только один тип.
    /// </summary>
    public class PickMapObject:MapObject
    {
        //ссылка на предмет
        public readonly GameItem GameItem;

        public PickMapObject(Vector3 position, float rotY, GameItem item) : 
            base(item.Type, position, rotY)
        {
            GameItem = item;
        }
    }
}
