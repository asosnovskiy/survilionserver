﻿using System.Collections.Generic;
using System.Linq;
using SurvCommon;
using SurvCommon.Network;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    public class BuildingMapObject : MapObject
    {
        public readonly Dictionary<ObjectsDic, int> RemainResources;
        public readonly long OwnerId;
        public int Durability;

        private string _ownerName = "admin";

        public bool Finished
        {
            get
            {
                lock (RemainResources)
                {
                    return RemainResources.All(n => n.Value == 0);
                }
            }
        }

        //можно ли использовать
        public virtual bool IsReady{get { return Finished; }}

        public BuildingMapObject(ObjectsDic type, Vector3 position, float rotY, Dictionary<ObjectsDic, int> remainResources, long ownerId)
            : base(type, position, rotY)
        {
            RemainResources = remainResources;
            OwnerId = ownerId;
        }

        public bool Contains(ObjectsDic resource)
        {
            lock (RemainResources)
            {
                return RemainResources.ContainsKey(resource);
            }
        }

        public void Build(ObjectsDic resource)
        {
            lock (RemainResources)
            {
                if (!Contains(resource))
                    return;

                RemainResources[resource] -= 1;

                if (RemainResources[resource] == 0)
                {
                    RemainResources.Remove(resource);
                }
            }
        }

        public override byte[] GetSpawnData()
        {
            var owner = World.GetPlayerByCharacterId(OwnerId);
            if (owner != null && owner.CharacterData != null)
                _ownerName = owner.CharacterData.Name;

            var packet = new Packet();

            lock (RemainResources)
            {
                packet.Addbool(Finished);
                packet.Addstring(_ownerName);

                if (!Finished)
                {
                    var remainResources = RemainResources.Where(n => n.Value != 0).ToDictionary(k => k.Key, v => v.Value);
                    packet.Addbyte((byte)remainResources.Count);
                    foreach (var remainResource in remainResources)
                    {
                        packet.Addint16((short)remainResource.Key);
                        packet.Addbyte((byte)remainResource.Value);
                    }
                }
            }

            return packet.Buffer;
        }

        //Передаем что-то при открытии здания
        public byte[] GetHandleData()
        {
            //var packet = new Packet();
            //packet.Addbool(Finished);
            //packet.Addint64(OwnerId);
            //return packet.Buffer;
            return null;
        }

        public byte[] GetBuildingProcessInfo()
        {
            var packet = new Packet();

            lock (RemainResources)
            {
                packet.Addbool(Finished);

                if (!Finished)
                {
                    var remainResources = RemainResources.Where(n => n.Value != 0).ToDictionary(k => k.Key, v => v.Value);
                    packet.Addbyte((byte)remainResources.Count);
                    foreach (var remainResource in remainResources)
                    {
                        packet.Addint16((short)remainResource.Key);
                        packet.Addbyte((byte)remainResource.Value);
                    }
                }
            }

            return packet.Buffer;
        }
    }
}
