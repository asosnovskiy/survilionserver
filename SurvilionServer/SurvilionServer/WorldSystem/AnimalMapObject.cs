﻿using System;
using System.Collections.Generic;
using System.Linq;
using SurvCommon;
using SurvCommon.Extensions;
using SurvCommon.Helpers;
using SurvCommon.Network;
using SurvGameServer.Caches;
using SurvGameServer.Database.Data;
using UnityEngine;
using Random = System.Random;

namespace SurvGameServer.WorldSystem
{
    public class AnimalMapObject : MiningMapObject
    {
        private float _hp;
        private readonly Rect _area;

        public float Hp { get { return _hp; } }
        public bool IsDie { get { return Hp == 0f; } }
        public bool IsNeedRemoveFromMap { get { return _dieTime < DateTime.Now; } }
        public MobData Data { get { return MobDataCache.GetItem(Type); } }

        private InterestArea _interestArea;

        private MapObject _target;

        private Vector2 _nextPosition;

        private int BeforeNewPositionTimer = 1000;

        // private DateTime _lastStopTime;
        private DateTime _lastAttackTime;
        private DateTime _dieTime;

        public AnimalMapObject(ObjectsDic type, List<DropItem> drop, Rect spawnArea)
            : base(type, Vector3.zero, 0f, drop)
        {
            _interestArea = new InterestArea(Fiber);
            _interestArea.AttachToItem(this);
            _area = spawnArea;
            ResetData();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _interestArea.Dispose();
            }

            base.Dispose(disposing);
        }

        public void ResetData()
        {
            _hp = Data.MaxHp;

            CreateNewNextPosition();
            Position = _nextPosition;
        }

        public void Update(float elapsedTime)
        {
            if (IsDie)
            {
                return;
            }

            _interestArea.UpdateInterestManagement();

            if (Data.CanAttack)
            {
                if (_target != null)
                {
                    if (_target.Disposed)
                    {
                        _target = null;
                        CreateNewNextPosition();
                        return;
                    }

                    var distance = GetDistance(Position, _target.Position);
                    //цель недосягаема
                    if (distance > Data.AgroMaxDistance)
                    {
                        _target = null;
                        CreateNewNextPosition();
                    }
                    else
                    {
                        if (distance < Data.AttackDistance && _lastAttackTime < DateTime.Now)
                        {
                            _lastAttackTime = DateTime.Now.AddMilliseconds(Data.AttackSpeed);

                            var player = World.GetPlayerByAvatarId(_target.Id);
                            var dmg = Data.GetRandomDmg;
                            //player.CharacterData.AdjHurt();
                            if (Log.IsInfoEnabled)
                            {
                                Log.InfoFormat("Mob {0} attack {1}(dmg: {2})", Type, player.CharacterData.Name,
                                               dmg);
                            }
                            //send that player attack
                            //player.SendCharacterParameters();
                        }
                        else
                        {
                            CreateNewNextPosition(_target.Position);
                        }
                    }
                }
                else
                {
                    MapObject newTarget;
                    if (TryGetTarget(out newTarget))
                    {
                        _target = newTarget;
                        CreateNewNextPosition(_target.Position);
                    }
                }
            }

            Move(elapsedTime);
        }

        public bool ApplyDmg(float dmg)
        {
            if (IsDie)
                return false;

            _hp -= dmg;

            //Die
            if (_hp <= 0f)
            {
                _hp = 0f;
                _dieTime = DateTime.Now.AddMilliseconds(Data.DieTimer);
            }

            return IsDie;
        }

        //public void SetTraget(Player player)
        //{
        //    if (player == null)
        //    {
        //        if (_target != null)
        //        {
        //            _target = null;
        //            CreateNewNextPosition();
        //        }
        //        return;
        //    }

        //    _target = player;
        //    _nextPosition = _target.CharacterData.Avatar.Position;
        //}

        private void Move(float elapsedTime)
        {
            var dir = (_nextPosition - Position).normalized;
            Position += dir * Data.Speed * (elapsedTime / 1000f);

            if (Vector2.Distance(Position, _nextPosition) <= 0.1f)
            {
                Position = _nextPosition;

                if (_target == null)
                    CreateNewNextPosition();
            }
        }

        private void CreateNewNextPosition(Vector2? nextPos = null)
        {
            var oldPos = Position;
            if (!nextPos.HasValue)
                _nextPosition = new Vector2(RandomHelper.Next(_area.xMin, _area.xMax),
                                            RandomHelper.Next(_area.yMin, _area.yMax));
            else
                _nextPosition = nextPos.Value;

            var dir = _nextPosition - oldPos;
            RotY = Vector2.Angle(Vector2.up, dir);//???Up

            if (dir.x < 0)
            {
                if (dir.y > 0)
                {
                    RotY -= 90f;
                }
                else
                {
                    RotY += 90f;
                }
            }
            //Log.InfoFormat("Creanewposition for {0}(id:{1}), rotY:{2}", Type, Id, RotY);
        }

        private bool TryGetTarget(out MapObject target)
        {
            target = null;

            var regions = _interestArea.WorldRegions.Keys;

            var allPlayers = new List<MapObject>();

            foreach (var region in regions)
            {
                allPlayers.AddRange(region.Objects.Where(n => n.Type.IsPlayer()));
            }

            if (!allPlayers.Any())
                return false;

            target = allPlayers.OrderBy(n => Vector2.Distance(Position, n.Position)).First();

            if (GetDistance(Position,target.Position) > Data.AgroMinDistance)
            {
                target = null;
                return false;
            }

            return true;
        }

        public override byte[] GetSpawnData()
        {
            var packet = new Packet();
            packet.Addfloat(Hp);
            return packet.Buffer;
        }
    
        private float GetDistance(Vector2 v1, Vector2 v2)
        {
            return Vector2.Distance(v1, v2);
        }
    }
}
