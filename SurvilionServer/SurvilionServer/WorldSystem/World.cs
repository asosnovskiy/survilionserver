﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ExitGames.Concurrency.Fibers;
using ExitGames.Logging;
using JsonFx.Json;
using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Extensions;
using SurvCommon.WorldSystem;
using SurvGameServer.ChatSystem;
using SurvGameServer.Database;
using SurvGameServer.WorldSystem.Weather;
using UnityEngine;

namespace SurvGameServer.WorldSystem
{
    /// <summary>
    /// Игровой мир. Содержит всех игроков и загруженные острова.
    /// </summary>
    public class World : IDisposable
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        private const float MAX_WORLD_SIZE = 10000f;
        private const int GRID_COUNT = 100; //X x X (10x10)
        private Region[][] _grid;
        private readonly float _sizeGrid;

        private int UpdateIMInterval = 1000;//ms

        //все игроки на сервере
        private readonly Dictionary<long, Player> _players = new Dictionary<long, Player>();

        private readonly Chat _chat;
        public Chat Chat { get { return _chat; } }

        private readonly WeatherManager _weather;
        public WeatherManager Weather { get { return _weather; } }
        //Объект в игре. Каждый объект имеет уникальный номер на острове и в базе.
        public readonly Dictionary<long, MapObject> Items = new Dictionary<long, MapObject>();

        //ид
        public readonly long Id;
        public readonly string Name;
        public double WorldTime { get; private set; }
        private readonly PoolFiber _fiber;
        private readonly PoolFiber _updateRegionFiber;

        private DateTime _lastSendWorldTime;
        private const int SendWorldTimeInterval = 30 * 1000;

        public World(long id, string name, double worldTime, IEnumerable<MapObject> objs)
        {
            Id = id;
            Name = name;
            WorldTime = worldTime;

            _chat = new Chat(this);
            _weather = new WeatherManager(this);

            _sizeGrid = (MAX_WORLD_SIZE / GRID_COUNT);

            _grid = new Region[GRID_COUNT][];
            for (int i = 0; i < GRID_COUNT; i++)
                _grid[i] = new Region[GRID_COUNT];

            for (int i = 0; i < GRID_COUNT; i++)
                for (int j = 0; j < GRID_COUNT; j++)
                {
                    _grid[i][j] = new Region(new Rect(i * _sizeGrid, j * _sizeGrid, _sizeGrid, _sizeGrid));
                }

            foreach (var mapObject in objs)
            {
                Items.Add(mapObject.Id, mapObject);
                GSGrid.AddObject(mapObject);
            }

            Log.InfoFormat("Create {0} items on world {1}", Items.Count, Id);

            _fiber = new PoolFiber();
            _fiber.Start();
            _fiber.ScheduleOnInterval(Update, 50, 50);

            _updateRegionFiber = new PoolFiber();
            _updateRegionFiber.Start();
            _updateRegionFiber.Enqueue(UpdateRegion);
        }

        /// <summary>
        /// Update region and IM every X ms
        /// </summary>
        private void UpdateRegion()
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            //foreach (var mapObject in objects)
            Parallel.ForEach(_players.Values, player =>
            {
                if (player.InterestArea != null && player.InterestArea.AttachedItem != null)
                {
                    player.InterestArea.UpdateInterestManagement();
                }
            });

            stopwatch.Stop();

            if (stopwatch.ElapsedMilliseconds < UpdateIMInterval)
            {
                _updateRegionFiber.Schedule(UpdateRegion, UpdateIMInterval - (int)stopwatch.ElapsedMilliseconds);
            }
            else
                _updateRegionFiber.Enqueue(UpdateRegion);
        }

        public void Update()
        {
            //Log.InfoFormat("Start WorldThread: {0}", _worldThread.Name);
            //_lastUpdateTime = DateTime.Now;
            //while (true)
            //{
            try
            {
                //var startTime = DateTime.Now;

                //all calculation execute this

                UpdateWorldTime(50);

                lock (Items)
                {
                    foreach (var mapObject in Items)
                    {
                        var animal = mapObject.Value as AnimalMapObject;
                        if (animal == null)
                            continue;

                        animal.Update(50);
                        animal.CurrentWorldRegion.Publish(animal.GetPositionUpdateMessage());

                        if (animal.IsDie && animal.IsNeedRemoveFromMap)
                        {
                            RemoveItem(animal);
                            animal.ResetData();
                            SpawnItem(animal, animal.Data.RespawnTimer);
                        }
                    }
                }

                _weather.Update();
                //end executions

                //var endTime = DateTime.Now - startTime;

                //double elapsed = TimeStep - endTime.TotalMilliseconds;
                //if (elapsed < 0)
                //    throw new Exception("ERROR!");

                //Thread.Sleep(TimeSpan.FromMilliseconds(elapsed));
            }
            catch (Exception ex)
            {
                Log.DebugFormat("Error occured: {0}", ex.ToString());
            }
            //}
        }

        public void Dispose()
        {
            _fiber.Stop();
            _fiber.Dispose();

            _updateRegionFiber.Stop();
            _updateRegionFiber.Dispose();

            lock (_players)
            {
                foreach (var player in _players.Values)
                {
                    LeaveMap(player);
                }

                _players.Clear();
            }

            SaveWorld();

            lock (Items)
            {
                foreach (var item in Items.Values)
                {
                    //save items to db

                    //dispose
                    item.Dispose();
                }

                Items.Clear();
            }
            //Log.InfoFormat("Stop WorldThread: {0}", _worldThread.Name);
        }

        private double oldWorldTime = 0;

        private void UpdateWorldTime(float elapsedTime)
        {
            var worldElapsedTime = GameConfig.WorldMsInRealSecond * elapsedTime / 1000f;

            WorldTime += worldElapsedTime;

            if (WorldTime > oldWorldTime)
            {
                var timeSpan = TimeSpan.FromMilliseconds(WorldTime);
                Log.DebugFormat("worldTime: D:{0} H:{1} M:{2}", System.Math.Floor(timeSpan.TotalDays), timeSpan.Hours, timeSpan.Minutes);
                oldWorldTime = WorldTime + GameConfig.WorldMsInRealSecond * 30;
            }

            if (_lastSendWorldTime < DateTime.Now)
            {
                _lastSendWorldTime = DateTime.Now.AddMilliseconds(SendWorldTimeInterval);
                SendWorldTime();
            }
        }

        private string GetWorldObjectsString()
        {
            var objs = new Dictionary<string, object>();

            var array = new List<object>();

            foreach (var item in Items.Values)
            {
                if (item.Type == ObjectsDic.PlayerPref)
                    continue;

                if (item.Type.IsAnimal() || item.Type.IsPicked())
                    continue;

                var jsonItem = new Dictionary<string, object>();
                jsonItem.Add("type", (int)item.Type);
                jsonItem.Add("posX", item.Position.x);
                jsonItem.Add("posZ", item.Position.y);
                jsonItem.Add("rotY", item.RotY);

                if (item.Type.IsBuilding())
                {
                    var remainResArray = new List<object>();

                    var building = item as BuildingMapObject;
                    if (building != null)
                    {
                        foreach (var remainResource in building.RemainResources)
                        {
                            var res = new Dictionary<string, object>();
                            res.Add("type", (int)remainResource.Key);
                            res.Add("count", remainResource.Value);
                            remainResArray.Add(res);
                        }

                        jsonItem.Add("ownerId", building.OwnerId);
                    }

                    jsonItem.Add("remainResources", remainResArray);
                }
                //else if (item.Type.IsPicked())
                //{
                //    var pick = item as PickMapObject;
                //    if (pick != null)
                //    {
                //        if (pick.GameItem == null || pick.GameItem.Count == 0)
                //            continue;
                //        jsonItem["count"] = pick.GameItem.Count;
                //        jsonItem["dur"] = pick.GameItem.Durability;
                //    }
                //}

                array.Add(jsonItem);
            }

            objs.Add("objs", array);
            var serialized = JsonWriter.Serialize(objs);

            if (Log.IsDebugEnabled)
                Log.DebugFormat("objstring:{0}", serialized);

            return serialized;
        }
        private void SaveWorld()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var world = session.Get<Database.Data.World>(Id);

                    world.WorldTime = WorldTime;
                    world.Data = GetWorldObjectsString();

                    trans.Commit();

                    var timeSpan = TimeSpan.FromMilliseconds(WorldTime);

                    if (Log.IsInfoEnabled)
                        Log.InfoFormat("World {0} was saved! WorldTime D:{1} H:{2} M:{3}",
                                       Name,
                                       timeSpan.Days,
                                       timeSpan.Hours,
                                       timeSpan.Minutes);
                }
            }
        }

        public bool TryJoinGame(Player player)
        {
            lock (_players)
            {
                if (_players.ContainsKey(player.Id))
                    return false;

                _players[player.Id] = player;

                Log.InfoFormat("User {0} enter in world {1}", player.Id, Id);
                return true;
            }
        }

        public void ExitGame(Player player)
        {
            lock (_players)
            {
                if (_players.ContainsKey(player.Id))
                {
                    _players.Remove(player.Id);

                    Log.InfoFormat("User {0} exit world {1}", player.Id, Id);
                }

            }
        }

        public void SendWorldTime(Player player = null)
        {
            var eventData = new EventData
                {
                    Code = (byte)EventCode.WorldTime,
                    Parameters = new Dictionary<byte, object>
                        {
                            {(byte) GameParameterKeys.WorldTime, WorldTime}
                        }
                };

            if (player != null)
            {
                player.SendEvent(eventData, new SendParameters
                    {
                        ChannelId = (byte)MessageChannel.Default,
                        Encrypted = false,
                        Unreliable = false
                    });
            }
            else
            {
                SendToAll(eventData, false);
            }

        }

        //enter in map
        public void JoinMap(Player player)
        {
            lock (_players)
            {
                if (player.PlayerStatus == PlayerStatus.Lobby)
                {
                    SpawnItem(player.CharacterData.Avatar);

                    player.SetStatus(PlayerStatus.Game);
                }
            }
        }

        public void LeaveMap(Player player)
        {
            lock (_players)
            {
                if (player.PlayerStatus == PlayerStatus.Game)
                {
                    player.SetStatus(PlayerStatus.Lobby);
                    RemoveItem(player.CharacterData.Avatar);
                }
            }
        }

        public Player GetPlayerByCharacterId(long id)
        {
            lock (_players)
            {
                return _players.Values.FirstOrDefault(player => player.CharacterData != null && player.CharacterData.CharacterId == id);
            }
        }
        
        public Player GetPlayerByAvatarId(long id)
        {
            lock (_players)
            {
                return
                    _players.Values.FirstOrDefault(
                        player => player.CharacterData != null && player.CharacterData.Avatar.Id == id);
            }
        }
        /// <summary>
        /// добавляем в массив предметов и отсылаем событие игрокам
        /// </summary>
        /// <param name="item"></param>
        public void SpawnItem(MapObject item, long delay = 0)
        {
            //if (delay != 0)
            //{
                _fiber.Schedule(() =>
                    {
                        lock (Items)
                        {
                            if (Items.ContainsKey(item.Id))
                                throw new Exception("Item with id " + item.Id + " already been on island!");

                            Items.Add(item.Id, item);
                            GSGrid.AddObject(item);
                            item.UpdateInterestManagement();
                        }
                    }, delay);
            //    return;
            //}

            //lock (Items)
            //{
            //    if (Items.ContainsKey(item.Id))
            //        throw new Exception("Item with id " + item.Id + " already been on island!");

            //    Items.Add(item.Id, item);
            //    GSGrid.AddObject(item);
            //    item.UpdateInterestManagement();
            //}
        }

        /// <summary>
        /// удаляем из массива предметов и отсылаем событие всем игрокам
        /// </summary>
        /// <param name="item"></param>
        public void RemoveItem(MapObject item)
        {
            lock (Items)
            {
                if (Items.ContainsKey(item.Id))
                {
                    Items.Remove(item.Id);
                    GSGrid.RemoveObject(item);
                    item.CurrentWorldRegion.Publish(item.GetDestroyMessage());

                    item.Dispose();
                }
            }
        }

        public MapObject GetItem(long id)
        {
            lock (Items)
            {
                if (Items.ContainsKey(id))
                    return Items[id];
                return null;
            }
        }

        public Region GetIntersactRegion(Vector3 pos)
        {
            var x = (int)System.Math.Floor(pos.x / _sizeGrid);
            var y = (int)System.Math.Floor(pos.z / _sizeGrid);

            if (x >= GRID_COUNT || x < 0 || y >= GRID_COUNT || y < 0)
                return null;

            return _grid[x][y];
        }

        public IEnumerable<Region> GetIntersactRegions(Vector3 centerPos, float width)
        {
            return GetIntersactRegions(centerPos, width, width);
        }
        public IEnumerable<Region> GetIntersactRegions(Vector3 centerPos, float width, float height)
        {
            var regions = new HashSet<Region>();

            var leftUp = centerPos + new Vector3(-width / 2f, 0, -height / 2f);

            var rect = new Rect(leftUp.x, leftUp.z, width, height);

            foreach (var region in GetRegionEnumerable(rect))
            {
                regions.Add(region);
            }

            return regions;
        }

        private IEnumerable<Region> GetRegionEnumerable(Rect boundArea)
        {
            var rectArea = new Rect(0, 0, MAX_WORLD_SIZE, MAX_WORLD_SIZE);
            var overlap = rectArea.IntersectWith(boundArea);

            var current = new Vector3(overlap.xMin, 0, overlap.yMin);

            while (current.z <= overlap.yMax)
            {
                foreach (Region region in GetRegionsForY(overlap, current))
                {
                    yield return region;
                }

                // go stepwise to the bottom
                current.z += _sizeGrid;
            }

            if (current.z > overlap.yMax)
            {
                current.z = overlap.yMax;
                foreach (Region region in GetRegionsForY(overlap, current))
                {
                    yield return region;
                }
            }

            yield break;
        }
        private IEnumerable<Region> GetRegionsForY(Rect overlap, Vector3 current)
        {
            // start on left side
            current.x = overlap.xMin;
            while (current.x <= overlap.xMax)
            {
                yield return GetIntersactRegion(current);

                // go stepwise to the right
                current.x += _sizeGrid;
            }

            if (current.x > overlap.xMax)
            {
                current.x = overlap.xMax;
                yield return GetIntersactRegion(current);
            }
        }

        public void SendToAll(EventData ev, bool unreliable = true)
        {
            lock (_players)
            {
                ev.SendTo(_players.Values, new SendParameters
                    {
                        ChannelId = unreliable ? (byte)MessageChannel.Unreliable : (byte)MessageChannel.Default,
                        Encrypted = false,
                        Unreliable = unreliable
                    });

                if (Log.IsDebugEnabled)
                    if ((EventCode)ev.Code != EventCode.ItemMoved)
                        Log.DebugFormat("Send event {0}", ((EventCode)ev.Code).ToString());
            }

        }

        public void Send(Player target, EventData ev, bool unreliable = true)
        {
            target.SendEvent(ev, new SendParameters
                {
                    ChannelId = unreliable ? (byte)MessageChannel.Unreliable : (byte)MessageChannel.Default,
                    Encrypted = false,
                    Unreliable = unreliable
                });

            if (Log.IsDebugEnabled)
                Log.DebugFormat("Send event {0} to user {1}", ((EventCode)ev.Code).ToString(), target.SocialId);
        }
    }
}
