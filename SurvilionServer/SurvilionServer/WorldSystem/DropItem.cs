﻿using SurvCommon;

namespace SurvGameServer.WorldSystem
{
    public class DropItem
    {
        public ObjectsDic ItemType { get; set; }
        public int Count { get; set; }
    }
}
