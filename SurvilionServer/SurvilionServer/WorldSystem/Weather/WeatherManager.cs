﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExitGames.Logging;
using Photon.SocketServer;
using SurvCommon;

namespace SurvGameServer.WorldSystem.Weather
{
    public class WeatherManager
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        private readonly World _world;
        private readonly List<WeatherConfig> _weatherConfigs = new List<WeatherConfig>();

        private WeatherType _weather;

        private DateTime _endWeatherTime;

        public WeatherManager(World world)
        {
            _world = world;

            _weatherConfigs.Add(new WeatherConfig
                {
                    Type = WeatherType.Normal,
                    MinTime = 1 * 60 * 60 * 1000,
                    MaxTime = 5 * 60 * 60 * 1000
                });

            _weatherConfigs.Add(new WeatherConfig
            {
                Type = WeatherType.Rainy,
                MinTime = 1 * 60 * 60 * 1000,
                MaxTime = 5 * 60 * 60 * 1000
            });
        }

        public void SendWeather(Player player = null)
        {
            var eventData = new EventData
            {
                Code = (byte)EventCode.Weather,
                Parameters = new Dictionary<byte, object>
                        {
                            {(byte) GameParameterKeys.ItemType, (byte) _weather}
                        }
            };

            if (player != null)
            {
                player.SendEvent(eventData, new SendParameters
                {
                    ChannelId = (byte)MessageChannel.Default,
                    Encrypted = false,
                    Unreliable = false
                });
            }
            else
            {
                _world.SendToAll(eventData, false);
            }

        }

        public void Update()
        {
            if (_endWeatherTime < DateTime.Now)
            {
                GenerateWeather();
                SendWeather();
            }
        }

        public void GenerateWeather(WeatherType? w = null)
        {
            WeatherConfig newWeather;

            if (!w.HasValue)
            {
                newWeather = _weatherConfigs.First(n => n.Type != _weather);
            }
            else
            {
                newWeather = _weatherConfigs.First(n => n.Type == w.Value);
            }

            var time = newWeather.GetTime;
            _endWeatherTime = DateTime.Now.AddMilliseconds(time);
            _weather = newWeather.Type;

            if (Log.IsInfoEnabled)
            {
                Log.InfoFormat("New weather: {0}, duration: {1} mins", _weather, time);
            }
        }
    }
}
