﻿using SurvCommon;
using SurvCommon.Helpers;

namespace SurvGameServer.WorldSystem.Weather
{
    public class WeatherConfig
    {
        public WeatherType Type { get; set; }
        public int MinTime { get; set; }
        public int MaxTime { get; set; }

        public int GetTime { get { return RandomHelper.Next(MinTime, MaxTime); } }
    }
}
