﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using ExitGames.Logging;
using ExitGames.Logging.Log4Net;
using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Caches;
using SurvCommon.Extensions;
using SurvCommon.Items;
using SurvCommon.WorldSystem;
using SurvGameServer.Caches;
using SurvGameServer.Database;
using SurvGameServer.Database.Data;
using SurvGameServer.WorldSystem;
using UnityEngine;
using log4net.Config;
using World = SurvGameServer.WorldSystem.World;

namespace SurvGameServer
{
    public class SurvilionApplication : ApplicationBase
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        protected override PeerBase CreatePeer(InitRequest initRequest)
        {
            if (initRequest.LocalPort == 4531)
            {
                return new TcpPeer(initRequest.Protocol, initRequest.PhotonPeer);
            }

            return new Player(initRequest.Protocol, initRequest.PhotonPeer);
        }

        /// <summary>
        /// Application initializtion.
        /// </summary>
        protected override void Setup()
        {
            log4net.GlobalContext.Properties["Photon:ApplicationLogPath"] = Path.Combine(this.ApplicationRootPath, "log");

            // log4net
            string path = Path.Combine(this.BinaryPath, "log4net.config");
            var file = new FileInfo(path);
            if (file.Exists)
            {
                LogManager.SetLoggerFactory(Log4NetLoggerFactory.Instance);
                XmlConfigurator.ConfigureAndWatch(file);
            }

            Log.InfoFormat("Created application Instance: type={0}", Instance.GetType());

            Initialize();
            LoadGSData();
            LoadItemsData();
            LoadMiningData();
            LoadBuildData();
            LoadSkillData();
            LoadCraftData();
            LoadMobData();
            LoadWorld();

            LoadMobSpawns();
        }

        private void LoadGSData()
        {
            var gsAreaFile = new FileInfo(Path.Combine(BinaryPath, "GSArea.xml"));
            var gsSizeOfObjectsFile = new FileInfo(Path.Combine(BinaryPath, "GSSizeOfObjects.xml"));

            if (gsAreaFile.Exists && gsSizeOfObjectsFile.Exists)
            {
                var document = new XmlDocument();

                using (var reader = gsAreaFile.OpenText())
                    document.LoadXml(reader.ReadToEnd());
                GSData.LoadAreas(document);
                Log.InfoFormat("{0} GSArea loaded!", GSData.Areas.Count);

                document = new XmlDocument();
                using (var reader = gsSizeOfObjectsFile.OpenText())
                    document.LoadXml(reader.ReadToEnd());
                GSData.LoadSizes(document);
                Log.InfoFormat("{0} GSObjectsSizes loaded!", GSData.SizeOfObjects.Count);
            }
            else
            {
                throw new Exception("No config file for GSData!");
            }
        }

        private void LoadItemsData()
        {
            //from db or files? db is more flexible and easible
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var items = session.QueryOver<GameItemData>().List();
                    GameItemDataCache.Items.Clear();
                    foreach (var item in items)
                    {
                        GameItemDataCache.Add(item.Type, item);
                        item.LoadModificators();
                    }

                    trans.Commit();

                    Log.InfoFormat("Load {0} items' configurations", items.Count);
                }
            }
        }

        private void LoadMiningData()
        {
            //from db or files? db is more flexible and easible
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var items = session.QueryOver<MiningData>().List();
                    MiningDataCache.Items.Clear();
                    foreach (var item in items)
                    {
                        MiningDataCache.Add(item.Type, item);
                        //for init
                        item.GetAllowInstruments();
                        item.GetDrop();
                    }

                    trans.Commit();

                    Log.InfoFormat("Load {0} miningData' configurations", items.Count);
                }
            }
        }

        private void LoadBuildData()
        {
            //from db or files? db is more flexible and easible
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var items = session.QueryOver<BuildData>().List();
                    BuildDataCache.Items.Clear();
                    foreach (var item in items)
                    {
                        BuildDataCache.Add(item.Type, item);
                        //for init
                        item.GetReqData();
                    }

                    trans.Commit();

                    Log.InfoFormat("Load {0} buildData' configurations", items.Count);
                }
            }
        }

        private void LoadSkillData()
        {
            //from db or files? db is more flexible and easible
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var items = session.QueryOver<SkillData>().List();
                    SkillDataCache.Items.Clear();
                    foreach (var item in items)
                    {
                        SkillDataCache.Add(item.Type, item);
                        //for init
                        item.ParseSkillData();
                    }

                    trans.Commit();

                    Log.InfoFormat("Load {0} skillData' configurations", items.Count);
                }
            }
        }

        private void LoadCraftData()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var items = session.QueryOver<CraftData>().List();
                    CraftDataCache.Items.Clear();
                    foreach (var item in items)
                    {
                        CraftDataCache.Add(item.Type, item);
                        //for init
                        item.GetReqData();
                    }

                    trans.Commit();

                    Log.InfoFormat("Load {0} craftData' configurations", items.Count);
                }
            }
        }

        private void LoadMobData()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var items = session.QueryOver<MobData>().List();
                    MobDataCache.Items.Clear();
                    foreach (var item in items)
                    {
                        MobDataCache.Add(item.Type, item);
                    }

                    trans.Commit();

                    Log.InfoFormat("Load {0} mobData' configurations", items.Count);
                }
            }
        }

        private void LoadMobSpawns()
        {
            string path = Path.Combine(BinaryPath, "MobSpawns.txt");
            var file = new FileInfo(path);
            if (file.Exists)
            {
                using (var reader = new StreamReader(path))
                {
                    int counter = 0;
                    string str;
                    while ((str = reader.ReadLine()) != null)
                    {
                        str = str.Replace('\t', ' ').Trim();
                        if (str.StartsWith("//") || str == "")
                            continue;

                        var type = int.Parse(str);
                        while ((str = reader.ReadLine()) != "end")
                        {
                            str = str.Replace('\t', ' ').Trim();
                            if (str.StartsWith("//") || str == "")
                                continue;

                            var pars = str.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                            var mobType = (ObjectsDic)int.Parse(pars[0].Trim());

                            var x1 = float.Parse(pars[1].Trim());
                            var y1 = float.Parse(pars[2].Trim());
                            var x2 = float.Parse(pars[3].Trim());
                            var y2 = float.Parse(pars[4].Trim());

                            var x = Math.Min(x1, x2);
                            var y = Math.Min(y1, y2);
                            var width = Math.Max(x1, x2) - x;
                            var height = Math.Max(y1, y2) - y;


                            var count = int.Parse(pars[5].Trim());

                            if (type == 0)
                            {
                                width = height = 0f;
                                count = 1;
                            }

                            var spawnRect = new Rect(x, y, width, height);

                            for (int i = 0; i < count; i++)
                            {
                                var mob = new AnimalMapObject(mobType,
                                                             MiningDataCache.GetItem(mobType)
                                                                            .GetDrop()
                                                                            .Select(
                                                                                n =>
                                                                                new DropItem
                                                                                    {
                                                                                        ItemType = n.ItemType,
                                                                                        Count = n.MaxCount
                                                                                    })
                                                                            .ToList(), spawnRect);
                                WorldCache.Instance.World().SpawnItem(mob);
                            }

                            counter++;
                        }
                    }

                    Log.InfoFormat("Created {0} spawns", counter);
                }
            }
        }

        protected override void TearDown()
        {
            lock (WorldCache.Instance.Worlds)
            {
                foreach (var world in WorldCache.Instance.Worlds.Values)
                {
                    world.Dispose();
                }

                WorldCache.Instance.Worlds.Clear();
            }
        }

        private void Initialize()
        {
            // counters for the photon dashboard
            //CounterPublisher.DefaultInstance.AddStaticCounterClass(typeof(Counter), "Lite");

            Protocol.AllowRawCustomValues = true;
        }

        private void LoadWorld()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var world = session.QueryOver<Database.Data.World>().SingleOrDefault();

                    if (world == null)
                    {
                        world = new Database.Data.World
                            {
                                Name = "My World",
                                WorldTime = 0,
                                Data = "{\"objs\":[]}"
                            };

                        session.Save(world);
                    }

                    trans.Commit();

                    var gameWorld = new World(world.Id, world.Name, world.WorldTime, world.GetMapObjects());
                    WorldCache.Instance.AddWorld(gameWorld);

                    foreach (var mapObject in gameWorld.Items.Values)
                    {
                        mapObject.UpdateInterestManagement();
                    }
                }
            }


        }
    }
}
