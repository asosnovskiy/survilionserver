﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Serialization;
using SurvGameServer.Events;
using SurvGameServer.WorldSystem;

namespace SurvGameServer
{
    public partial class Player
    {
        #region Fields

        private CharacterData _characterData;
        private const long RegenTime = 1000;
        private IDisposable _regenSheduler;

        private InterestArea _interestArea;

        #endregion

        #region Properties

        public CharacterData CharacterData { get { return _characterData; } }

        public World World
        {
            get { return WorldCache.Instance.World(); }
        }

        public InterestArea InterestArea { get { return _interestArea; } }
        #endregion

        #region Methods

        private void Regeneration()
        {
            if (_characterData == null)
                return;

            if (_characterData.IsDie)
            {
                Die();
                return;
            }

            if (_characterData.IsMaxTiredness)
            {
                _characterData.AdjHurt(-GameConfig.DamageFromTiredness);

            }

            if (_characterData.IsMaxHunger)
            {
                _characterData.AdjHurt(-GameConfig.DamageFromHunger);
            }

            if (_characterData.IsMaxDrought)
            {
                _characterData.AdjHurt(-GameConfig.DamageFromDrought);
            }

            _characterData.AdjEat(-GameConfig.RegEat);
            _characterData.AdjWater(-GameConfig.RegWater);
            _characterData.AdjTiredness(-GameConfig.RegTiredness);

            SendCharacterParameters();
        }

        private void StartRegen()
        {
            if (_regenSheduler != null)
            {
                _regenSheduler.Dispose();
            }

            _regenSheduler = RequestFiber.ScheduleOnInterval(Regeneration, RegenTime, RegenTime);
        }

        private void StopRegen()
        {
            if (_regenSheduler != null)
            {
                _regenSheduler.Dispose();
                _regenSheduler = null;
            }
        }

        #endregion

        #region send

        public void SendInventoryItems()
        {
            var eventObject = new InventoryItems();

            eventObject.Items = _characterData.GetSendInventoryList();

            var eventData = new EventData((byte)EventCode.InventoryItems, eventObject);

            SendEvent(eventData, new SendParameters
                {
                    ChannelId = (byte)MessageChannel.Default,
                    Encrypted = false,
                    Unreliable = false
                });
        }

        public void SendEquippedItems()
        {
            var eventObject = new InventoryItems();

            eventObject.Items = _characterData.GetSendEquippedList();

            var eventData = new EventData((byte)EventCode.EquippedItems, eventObject);

            SendEvent(eventData, new SendParameters
            {
                ChannelId = (byte)MessageChannel.Default,
                Encrypted = false,
                Unreliable = false
            });
        }

        #endregion

    }
}
