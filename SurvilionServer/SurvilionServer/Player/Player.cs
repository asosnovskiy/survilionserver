﻿using System;
using System.Collections.Generic;
using ExitGames.Logging;
using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using PhotonHostRuntimeInterfaces;
using SurvCommon;
using SurvCommon.Network;
using SurvGameServer.Database;
using SurvGameServer.Database.Data;
using SurvGameServer.Events;
using SurvGameServer.Providers;
using SurvGameServer.WorldSystem;
using UnityEngine;

namespace SurvGameServer
{
    public partial class Player : Peer
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        public long Id { get; private set; }
        public long SocialId { get; private set; }
        public PlayerStatus PlayerStatus { get; private set; }

        public Player(IRpcProtocol rpcProtocol, IPhotonPeer photonPeer)
            : base(rpcProtocol, photonPeer)
        {
            SetStatus(PlayerStatus.NotAuth);
            if (Log.IsInfoEnabled)
                Log.InfoFormat("Connected new peer:{0}", photonPeer.GetRemoteIP() + ":" + photonPeer.GetRemotePort());
        }

        protected override void OnDisconnect(DisconnectReason reasonCode, string reasonDetail)
        {
            Log.InfoFormat("Player.OnDisconnect");

            World.ExitGame(this);

            CurrentOperationHandler.OnDisconnect(this);
        }


        /// <summary>
        ///   Diposes the <see cref = "MmoRadarSubscription" /> and the <see cref = "CounterSubscription" />.
        /// </summary>
        /// <param name = "disposing">
        ///   The disposing.
        /// </param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_regenSheduler != null)
                {
                    _regenSheduler.Dispose();
                    _regenSheduler = null;
                }

                if (_characterData != null)
                {
                    World.LeaveMap(this);

                    if (_characterData.Avatar != null)
                    {
                        SaveData();

                        _characterData.Avatar.Dispose();
                    }

                    _characterData = null;
                }

                if (_interestArea != null)
                    _interestArea.Dispose();
                _interestArea = null;
            }

            base.Dispose(disposing);
        }

        public void SetStatus(PlayerStatus status)
        {
            PlayerStatus = status;
        }

        /// <summary>
        /// Убиваем игрока. Отнимаем скиллы, выбрасываем предметы и т.п.
        /// </summary>
        private void Die()
        {
            if (Log.IsInfoEnabled)
                Log.InfoFormat("Player {0} is die!", _characterData.Name);

            StopRegen();

            _characterData.DieTime = DateTime.Now.AddSeconds(GameConfig.DieTime);
            //_characterData.ClearEquippedAndInventory();

            SendPlayerDie();
            SendCharacterParameters();
        }

        private void SaveData()
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var account = AccountProvider.GetAccount(Id);

                    //exists account, get data
                    if (account != null)
                    {
                        var character =
                            session.QueryOver<Character>().Where(n => n.Account == account && n.Id == _characterData.CharacterId).SingleOrDefault();

                        if (character != null)
                        {
                            character.Money = _characterData.Money;

                            character.Hp = _characterData.Hp;
                            character.Hunger = _characterData.Hunger;
                            character.Drought = _characterData.Drought;
                            character.Tiredness = _characterData.Tiredness;

                            character.PosX = _characterData.Avatar.Position.x;
                            character.PosY = _characterData.Avatar.Position.y;
                            character.RotY = _characterData.Avatar.RotY;

                            character.Equipped = _characterData.GetEquippedSaveString();
                            character.Inventory = _characterData.GetInventorySaveString();
                            character.Learning = _characterData.GetLearnedSaveString();
                            character.DieTime = _characterData.DieTime;

                            session.Update(character);

                            Log.InfoFormat("Character {0} saved!", _characterData.CharacterId);
                        }
                    }

                    trans.Commit();
                }
            }
        }

        private void SendPlayerDie()
        {
            var eventData = new EventData((byte)EventCode.PlayerDie, new PlayerDie
            {
                AvatarId = _characterData.Avatar.Id
            });

            _characterData.Avatar.CurrentWorldRegion.Publish(new Messages.PlayerDie(eventData));
        }

        private void SendPlayerEquippedItems()
        {
            var eventData = new EventData((byte)EventCode.PlayerEquippedItems, new PlayerEquippedItems
            {
                AvatarId = _characterData.Avatar.Id,
                Data = _characterData.Avatar.GetEquippedItems()
            });

            _characterData.Avatar.CurrentWorldRegion.Publish(new Messages.PlayerEquippedItems(eventData));
        }

        private void CheckLearnObj(ObjectsDic type)
        {
            if (_characterData.IsLearned(type))
                return;

            _characterData.Learn(type);

            var data = new EventData((byte)EventCode.ObjLearned,
                                     new Dictionary<byte, object>
                                         {
                                             {(byte) GameParameterKeys.ItemType, (short) type},
                                             {(byte) GameParameterKeys.ItemCount, _characterData.FreeLearningPoints}
                                         });

            SendEvent(data, new SendParameters());
        }

        private void SendMoney()
        {
            var data = new EventData((byte)EventCode.BalanceUpdated,
                                     new Dictionary<byte, object>
                                         {
                                             {(byte) GameParameterKeys.ItemCount, _characterData.Money}
                                         });

            SendEvent(data, new SendParameters());
        }

        private void SendSkills()
        {
            var data = new EventData((byte)EventCode.SkillsList,
                                    new Dictionary<byte, object>
                                         {
                                             {(byte) GameParameterKeys.ItemData, _characterData.GetLearnedSkills()}
                                         });

            SendEvent(data, new SendParameters());
        }

        public void SendCharacterParameters()
        {
            var packet = new Packet(16);
            packet.Addfloat(_characterData.Hp);
            packet.Addfloat(_characterData.Tiredness);
            packet.Addfloat(_characterData.Hunger);
            packet.Addfloat(_characterData.Drought);

            var data = new EventData((byte)EventCode.CharacterParameters,
                                     new Dictionary<byte, object>
                                         {
                                             {(byte) GameParameterKeys.ItemData, packet.Buffer}
                                         });

            SendEvent(data,
                      new SendParameters
                          {
                              ChannelId = (byte)MessageChannel.Default,
                              Encrypted = false,
                              Unreliable = false
                          });
        }
    }
}
