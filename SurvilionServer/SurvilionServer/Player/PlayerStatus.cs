﻿using System;

namespace SurvGameServer
{
    
    public enum PlayerStatus:byte
    {
        NotAuth = 0x1,
        Lobby = 0x2,
        //Loading = 0x4,
        Game = 0x8,
    }
}
