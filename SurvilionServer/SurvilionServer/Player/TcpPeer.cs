﻿using System.Collections;
using System.IO;
using System.Xml.Serialization;
using ExitGames.Logging;
using Photon.SocketServer;
using Photon.SocketServer.Rpc;
using PhotonHostRuntimeInterfaces;
using SurvCommon;
using SurvCommon.Caches;
using SurvCommon.Items;
using SurvGameServer.Operations;

namespace SurvGameServer
{
    public class TcpPeer:Peer
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        private static GetConfigsResponse _getConfigsResponse;

        public TcpPeer(IRpcProtocol rpcProtocol, IPhotonPeer photonPeer) : base(rpcProtocol, photonPeer)
        {
            if (Log.IsInfoEnabled)
                Log.InfoFormat("Connected new tcpPeer:{0}", photonPeer.GetRemoteIP() + ":" + photonPeer.GetRemotePort());
        }

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters)
        {
            base.OnOperationRequest(operationRequest, sendParameters);

            switch ((TcpOperationCode) operationRequest.OperationCode)
            {
                case TcpOperationCode.GetConfigs:
                    GetConfigsHandler(operationRequest, sendParameters);
                    break;
            }
        }

        private void GetConfigsHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            if (_getConfigsResponse != null)
            {
                SendOperationResponse(new OperationResponse(operationRequest.OperationCode, _getConfigsResponse),
                      sendParameters);
                Log.DebugFormat("Send cached getConfigs!");
                return;
            }

            var response = new GetConfigsResponse();

            var xmlSerializer = new XmlSerializer(typeof(GameItemData));
            var itemsList = new Hashtable();

            foreach (var item in GameItemDataCache.Items)
            {
                var gameItem = item.Value;

                if (gameItem != null)
                {
                    var outString = new StringWriter();
                    xmlSerializer.Serialize(outString, gameItem);
                    itemsList.Add(gameItem.Id, outString.ToString());
                }
            }

            response.ItemsConfig = itemsList;

            xmlSerializer = new XmlSerializer(typeof(BuildData));
            itemsList = new Hashtable();

            foreach (var item in BuildDataCache.Items)
            {
                var gameItem = item.Value;

                if (gameItem != null)
                {
                    var outString = new StringWriter();
                    xmlSerializer.Serialize(outString, gameItem);
                    itemsList.Add(gameItem.Id, outString.ToString());
                }
            }

            response.BuildsConfig = itemsList;

            xmlSerializer = new XmlSerializer(typeof(SkillData));
            itemsList = new Hashtable();

            foreach (var item in SkillDataCache.Items)
            {
                var gameItem = item.Value;

                if (gameItem != null)
                {
                    var outString = new StringWriter();
                    xmlSerializer.Serialize(outString, gameItem);
                    itemsList.Add(gameItem.Id, outString.ToString());
                }
            }

            response.SkillsConfig = itemsList;

            xmlSerializer = new XmlSerializer(typeof(CraftData));
            itemsList = new Hashtable();

            foreach (var item in CraftDataCache.Items)
            {
                var gameItem = item.Value;

                if (gameItem != null)
                {
                    var outString = new StringWriter();
                    xmlSerializer.Serialize(outString, gameItem);
                    itemsList.Add(gameItem.Id, outString.ToString());
                }
            }

            response.CraftConfig = itemsList;

            _getConfigsResponse = response;

            SendOperationResponse(new OperationResponse(operationRequest.OperationCode, _getConfigsResponse),
                                  sendParameters);
        }
    }
}
