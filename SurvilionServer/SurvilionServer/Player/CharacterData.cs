﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using JsonFx.Json;
using SurvCommon;
using SurvCommon.Caches;
using SurvCommon.Items;
using SurvCommon.Network;
using SurvCommon.Serialization;
using SurvGameServer.Database.Data;
using SurvGameServer.Factories;
using SurvGameServer.WorldSystem;
using UnityEngine;
using MapObject = SurvGameServer.WorldSystem.MapObject;
using System.Linq;

namespace SurvGameServer
{
    public class CharacterData
    {
        private readonly long _characterId;

        private string _name;
        private int _money;

        private float _hp;
        private float _hunger;
        private float _drought;
        private float _tiredness;

        private readonly PlayerMapObject _avatar;
        private UserGroup _group;

        //inventory
        private readonly List<GameItem> _inventory;
        private readonly Dictionary<ItemSlot, GameItem> _equippedItems;

        public Dictionary<ItemSlot,GameItem> EquippedItems{get { return _equippedItems; }} 

        private static readonly GameItem _hands = GameItemFactory.Instance.BuildGameItem(ObjectsDic.Hands, 0, 0);

        public GameItem UsedItem
        {
            get
            {
                if (_equippedItems[ItemSlot.Weapon] == null)
                    return _hands;
                return _equippedItems[ItemSlot.Weapon];
            }
            private set
            {
                _equippedItems[ItemSlot.Weapon] = value;
            }
        }

        //learning
        private readonly List<ObjectsDic> _learnedObj;
        private readonly List<SkillType> _learnedSkills;
        private int _freeLearningPoints;
        public int FreeLearningPoints { get { return _freeLearningPoints; } }
        //other data
        public long CharacterId { get { return _characterId; } }
        public PlayerMapObject Avatar { get { return _avatar; } }
        public bool IsDie
        {
            get { return Hp <= 0f; }
        }

        public bool IsMaxHunger
        {
            get { return Hunger <= 0f; }
        }

        public bool IsMaxDrought
        {
            get { return Drought <= 0f; }
        }

        public bool IsMaxTiredness
        {
            get { return Tiredness <= 0f; }
        }

        public UserGroup Group { get { return _group; } }

        public string Name { get { return _name; } }

        public int Money { get { return _money; } }

        public float Hp { get { return _hp; } }
        public float Hunger { get { return _hunger; } }
        public float Drought { get { return _drought; } }
        public float Tiredness { get { return _tiredness; } }

        public CharacterData(Character character)
        {
            _characterId = character.Id;

            _name = character.Name;

            _money = character.Money;

            _hp = character.Hp;
            _hunger = character.Hunger;
            _drought = character.Drought;
            _tiredness = character.Tiredness;

            _inventory = character.GetInventoryItems();
            _equippedItems = character.GetEquippedItems();

            _learnedObj = character.GetLearnedObjs();
            _learnedSkills = character.GetLearnedSkills();
            DieTime = character.DieTime;
            CalculateFreePoints();

            if (IsDie && DateTime.Now > DieTime)
            {
                ResetCharacterParameters();
            }

            _avatar = new PlayerMapObject(this, new Vector3(character.PosX, character.PosY, character.PosZ), character.RotY);
        }

        private void ResetCharacterParameters()
        {
            AdjHurt(1);
            AdjEat(1);
            AdjWater(1);
            AdjTiredness(1);
        }

        public void AdjHurt(float amount)
        {
            _hp = AdjParameter(Hp, amount);
        }

        public void AdjEat(float amount)
        {
            _hunger = AdjParameter(Hunger, amount);
        }

        public void AdjWater(float amount)
        {
            _drought = AdjParameter(Drought, amount);
        }

        public void AdjTiredness(float amount)
        {
            _tiredness = AdjParameter(Tiredness, amount);
        }

        private float AdjParameter(float parValue, float amount)
        {
            parValue += amount;

            if (parValue < 0)
                parValue = 0;
            else if (parValue > 1f)
                parValue = 1f;

            return parValue;
        }

        //inventory
        #region
        public GameItem AddItem(GameItem item)
        {
            var posibleCount = item.Count;

            if (item.Data.IsMulticount)
            {
                int freeSlot = -1;

                for (int i = 0; i < _inventory.Count; i++)
                {
                    var gameItem = _inventory[i];

                    if (gameItem == null)
                    {
                        if (freeSlot == -1)
                        {
                            freeSlot = i;
                        }
                    }
                    else if (gameItem.Type == item.Type)
                    {
                        gameItem.AdjCount(posibleCount);
                        return gameItem;
                    }
                }

                if (freeSlot != -1)
                {
                    _inventory[freeSlot] = item;
                    return item;
                }

                //not free cell in inventory!
                return null;
            }
            else
            {
                for (int i = 0; i < _inventory.Count; i++)
                {
                    var gameItem = _inventory[i];

                    if (gameItem == null)
                    {
                        _inventory[i] = item;
                        return item;
                    }
                }

                return null;
            }
        }

        public void DropItem(int slot, int count)
        {
            var item = _inventory[slot];

            if (item == null)
            {
                throw new Exception(string.Format("Item slot {0} not at character {1}", slot, Name));
            }

            if (item.Count < count)
                throw new ArgumentException("No another count items in inventory for drop!");

            //change item count
            ChangeItemCount(slot, -count);
        }

        public bool IsMaxCount
        {
            get { return _inventory.All(n => n != null); }
        }

        public DateTime DieTime { get; set; }

        public GameItem ItemById(long id)
        {
            return _inventory.FirstOrDefault(n => n != null && n.Id == id);
        }
        private void ChangeItemCount(int slot, int amount)
        {
            var item = _inventory[slot];
            if (item == null)
                throw new ArgumentException(string.Format("No item slot {0} in character's inventory {1}", slot, Name));

            ChangeItemCount(item, amount);
        }
        public void ChangeItemCount(GameItem item, int amount)
        {
            if (item == null)
                throw new Exception("item is null!");

            if (item.Count + amount < 0)
                throw new Exception("GameStorage.ChangePickItemCount item.Count less then Zero!");

            item.AdjCount(amount);

            if (item.Count == 0)
            {
                RemoveItemById(item.Id);

                if (UsedItem.Id == item.Id)
                {
                    UsedItem = null;
                }
            }
        }
        public void RemoveItemById(long id)
        {
            for (int i = 0; i < _inventory.Count; i++)
            {
                var gameItem = _inventory[i];
                if (gameItem != null && gameItem.Id == id)
                {
                    _inventory[i] = null;
                    return;
                }
            }
        }

        public GameItem ReplaceInventoryItem(GameItem oldItem, GameItem newItem)
        {
            //если старой нет, то ищем пустой слот или складываем в кучу
            if (oldItem == null)
            {
                AddItem(newItem);
                return null;
            }
            else
            {
                for (int i = 0; i < _inventory.Count; i++)
                {
                    var gameItem = _inventory[i];
                    if (gameItem == oldItem)
                    {
                        _inventory[i] = newItem;
                        return oldItem;
                    }
                }

                return null;
            }
        }

        //equip
        public GameItem EquipById(long id)
        {
            return _equippedItems.Where(n => n.Value != null && n.Value.Id == id).Select(n => n.Value).FirstOrDefault();
        }

        public GameItem EquipByType(ObjectsDic type)
        {
            var pair = _equippedItems.FirstOrDefault(n => n.Value != null && n.Value.Type == type);
            return pair.Value;
        }
        public GameItem ReplaceEquipItem(GameItem newItem, ItemSlot slot)
        {
            GameItem oldItem = _equippedItems[slot];
            _equippedItems[slot] = newItem;
            return oldItem;
        }

        public GameItem GetResource(ObjectsDic type)
        {
            return _inventory.FirstOrDefault(gameItem => gameItem != null && gameItem.Type == type);
        }

        public void ClearEquippedAndInventory()
        {
            for (int i = 0; i < _inventory.Count; i++)
                _inventory[i] = null;

            for (int i = 1; i < _equippedItems.Count; i++)
                ReplaceEquipItem(null, (ItemSlot)i);
        }

        public string GetInventorySaveString()
        {
            var objs = new Dictionary<string, object>();
            var array = new List<object>();

            for (int i = 0; i < _inventory.Count; i++)
            {
                var gameItem = _inventory[i];
                if (gameItem != null)
                {
                    var jsonItem = new Dictionary<string, object>();
                    jsonItem.Add("type", (int)gameItem.Type);
                    jsonItem.Add("count", gameItem.Count);
                    jsonItem.Add("dur", gameItem.Durability);
                    jsonItem.Add("index", i);
                    array.Add(jsonItem);
                }
            }

            objs.Add("items", array);
            var serialized = JsonWriter.Serialize(objs);

            return serialized;
        }
        public string GetEquippedSaveString()
        {
            var objs = new Dictionary<string, object>();
            var array = new List<object>();

            foreach (var item in _equippedItems)
            {
                var gameItem = item.Value;

                if (gameItem != null)
                {
                    var jsonItem = new Dictionary<string, object>();
                    jsonItem.Add("type", (int)gameItem.Type);
                    jsonItem.Add("dur", gameItem.Durability);
                    jsonItem.Add("index", (int)item.Key);
                    array.Add(jsonItem);
                }
            }

            objs.Add("equipped", array);
            var serialized = JsonWriter.Serialize(objs);

            return serialized;
        }
        public string GetLearnedSaveString()
        {
            var objs = new Dictionary<string, object>();

            var arrayObjs = _learnedObj.Select(item => (int)item).ToArray();
            var arraySkills = _learnedSkills.Select(item => (int)item).ToArray();

            objs.Add("learned", arrayObjs);
            objs.Add("skills", arraySkills);
            var serialized = JsonWriter.Serialize(objs);

            return serialized;
        }

        public Hashtable GetSendInventoryList()
        {
            var xmlSerializer = new XmlSerializer(typeof(InventoryItem));
            var itemsList = new Hashtable();

            for (short i = 0; i < _inventory.Count; i++)
            {
                var gameItem = _inventory[i];
                if (gameItem != null)
                {
                    var outString = new StringWriter();
                    xmlSerializer.Serialize(outString, gameItem.BuildInventoryItem(i));
                    itemsList.Add(gameItem.Id, outString.ToString());
                }
            }

            return itemsList;
        }

        public Hashtable GetSendEquippedList()
        {
            var xmlSerializer = new XmlSerializer(typeof(InventoryItem));
            var itemsList = new Hashtable();

            foreach (var item in _equippedItems)
            {
                var gameItem = item.Value;
                if (gameItem != null)
                {
                    var outString = new StringWriter();
                    xmlSerializer.Serialize(outString, gameItem.BuildInventoryItem((short)item.Key));
                    itemsList.Add(gameItem.Id, outString.ToString());
                }
            }

            return itemsList;
        }
        #endregion

        #region learning
        public bool IsLearned(ObjectsDic obj)
        {
            return _learnedObj.Any(n => n == obj);
        }

        public bool IsLearnedSkill(SkillType skill)
        {
            return _learnedSkills.Any(n => n == skill);
        }

        public bool IsPossibleLearnSkill(SkillType skill)
        {
            var skillData = SkillDataCache.GetItem(skill);

            return _freeLearningPoints >= skillData.ReqPoints &&
                   skillData.ReqSkills.All(r => _learnedSkills.Any(n => n == r));
        }

        public void Learn(ObjectsDic obj)
        {
            _learnedObj.Add(obj);
            CalculateFreePoints();
        }

        public void LearnSkill(SkillType skill)
        {
            _learnedSkills.Add(skill);
            CalculateFreePoints();
        }

        private void CalculateFreePoints()
        {
            var all = _learnedObj.Count * GameConfig.LearnedObjPoints;
            var skillPoints = _learnedSkills.Sum(n => SkillDataCache.GetItem(n).ReqPoints);
            _freeLearningPoints = all - skillPoints;
        }

        public byte[] GetLearnedSkills()
        {
            var packet = new Packet();
            packet.Addint32(_learnedSkills.Count);
            foreach (var skill in _learnedSkills)
            {
                packet.Addbyte((byte)skill);
            }
            packet.Addint32(FreeLearningPoints);
            return packet.Buffer;
        }
        #endregion
    }
}
