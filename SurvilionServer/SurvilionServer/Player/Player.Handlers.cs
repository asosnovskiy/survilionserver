﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Photon.SocketServer;
using SurvCommon;
using SurvCommon.Caches;
using SurvCommon.Extensions;
using SurvCommon.Helpers;
using SurvCommon.Items;
using SurvCommon.Serialization;
using SurvGameServer.Caches;
using SurvGameServer.Database;
using SurvGameServer.Database.Data;
using SurvGameServer.Events;
using SurvGameServer.Factories;
using SurvGameServer.Messages;
using SurvGameServer.Operations;
using SurvGameServer.Providers;
using SurvGameServer.WorldSystem;
using UnityEngine;

namespace SurvGameServer
{
    public partial class Player
    {
        private DateTime _lastUpdateAvatarIm = DateTime.UtcNow;
        private const int UpdateAvatarImInterval = 100;//ms

        protected override void OnOperationRequest(OperationRequest operationRequest, SendParameters sendParameters)
        {
            if (Log.IsDebugEnabled)
            {
                Log.DebugFormat("Player.OnOperationRequest. Code={0}", operationRequest.OperationCode);
            }

            switch ((OperationCode)operationRequest.OperationCode)
            {
                #region login and selection character
                case OperationCode.Login:
                    HandleLoginOperation(operationRequest, sendParameters);
                    break;
                case OperationCode.GetCharacterList:
                    HandleGetCharactersList(operationRequest, sendParameters);
                    break;
                case OperationCode.CreateCharacter:
                    HandleCreateCharacter(operationRequest, sendParameters);
                    break;
                case OperationCode.DeleteCharacter:
                    HandleDeleteCharacter(operationRequest, sendParameters);
                    break;
                case OperationCode.SelectCharacter:
                    HandleSelectCharacter(operationRequest, sendParameters);
                    break;
                case OperationCode.ByeCharacter:
                    HandleByeCharacter(operationRequest, sendParameters);
                    break;
                case OperationCode.LoadLevel:
                    HandleLoadLevel(operationRequest, sendParameters);
                    break;
                #endregion

                //world
                case OperationCode.LevelLoaded:
                    LevelLoadedHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.ExitLevel:
                    ExitLevelHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.Move:
                    MoveHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.PlayerMine:
                    PlayerMineHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.PlayerAttack:
                    PlayerAttackHandler(operationRequest, sendParameters);
                    break;
                //chat
                case OperationCode.ChatMessage:
                    ChatMessageHandler(operationRequest, sendParameters);
                    break;
                //inventory
                case OperationCode.ItemDropFromInventory:
                    ItemDropInventoryHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.ItemPick:
                    ItemPickHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.ItemEquip:
                    ItemEquipHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.ItemUnequip:
                    ItemUnequipHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.ItemUse:
                    ItemUseHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.LearnSkill:
                    LearnSkillHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.CraftItem:
                    CraftItemHandler(operationRequest, sendParameters);
                    break;
                //building
                case OperationCode.BuildBuilding:
                    BuildBuildingHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.BuilingHandle:
                    BuilingHandleHandler(operationRequest, sendParameters);
                    break;
                case OperationCode.DestroyBuilding:
                    DestroyBuildingHandler(operationRequest, sendParameters);
                    break;

                //admins
                case OperationCode.SpawnItem:
                    SpawnItemHandler(operationRequest, sendParameters);
                    break;
            }
        }

        private void SendOperationInvalid(OperationRequest operationRequest, ReturnCode returnCode = ReturnCode.OperationInvalid, string debugMessage = "Operation invalid!")
        {
            var operation = new OperationResponse
            {
                OperationCode = operationRequest.OperationCode,
                ReturnCode = (short)returnCode,
                DebugMessage = debugMessage
            };

            SendOperationResponse(operation, new SendParameters());
        }

        #region handlers

        private void HandleLoginOperation(OperationRequest operationRequest, SendParameters sendParameters)
        {
            // create login operation
            var request = new LoginRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            SocialId = request.SocialId;

            var loginResponse = new LoginResponse { ErrorCode = ReturnCode.Ok };

            //check version
            if (request.GameVersion != "1.0")
            {
                loginResponse.ErrorCode = ReturnCode.OldVersion;
            }
            else
            {
                //check social id
                Id = AccountProvider.Login(SocialId);

                if (!World.TryJoinGame(this))
                    loginResponse.ErrorCode = ReturnCode.MultiPlatform;
            }

            if (loginResponse.ErrorCode == ReturnCode.Ok)
            {
                SetStatus(PlayerStatus.Lobby);
            }

            SendOperationResponse(new OperationResponse(operationRequest.OperationCode, loginResponse),
                                  new SendParameters());
        }

        private void HandleGetCharactersList(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var getCharactersRequest = new GetCharacterListRequest(Protocol, operationRequest);

            var getCharactersResponse = new GetCharacterListResponse();

            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var account = AccountProvider.GetAccount(Id);

                    if (account != null)
                    {
                        var characters = session.QueryOver<Character>().Where(n => n.Account == account).List();

                        var xmlSerializer = new XmlSerializer(typeof(CharacterItem));
                        var characterList = new Hashtable();

                        foreach (var character in characters)
                        {
                            var outString = new StringWriter();

                            xmlSerializer.Serialize(outString, character.BuildCharacterItem());

                            characterList.Add(character.Id, outString.ToString());
                        }

                        //load from db
                        getCharactersResponse.Characters = characterList;
                        SendOperationResponse(new OperationResponse(getCharactersRequest.OperationRequest.OperationCode,
                                                                    getCharactersResponse), new SendParameters());
                    }
                    else
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Account not found!");
                    }

                    trans.Commit();

                }
            }
        }

        private void HandleCreateCharacter(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new CreateCharacterRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var account = AccountProvider.GetAccount(Id);

                    if (account != null)
                    {
                        var nameExists =
                            session.QueryOver<Character>().Where(n => n.Name == request.Name).SingleOrDefault();

                        if (nameExists != null)
                        {
                            SendOperationInvalid(operationRequest, ReturnCode.NameAlreadyUse, "Name alreay exists!");
                        }
                        else
                        {
                            var isFirstCharacter =
                                session.QueryOver<Character>().Where(n => n.Account == account).SingleOrDefault() ==
                                null;

                            var character = new Character
                            {
                                Account = account,
                                Name = request.Name,
                                Sex = request.Sex,
                                Money = 1000,
                                Selected = isFirstCharacter,
                                Hp = 1f,
                                Tiredness = 1f,
                                Drought = 1f,
                                Hunger = 1f,
                                Equipped = "{\"equipped\":[]}",
                                Inventory = "{\"items\":[]}",
                                Learning = "{\"learned\":[], \"skills\":[]}"
                            };

                            session.Save(character);

                            Log.InfoFormat("Create character " + request.Name);

                            SendOperationResponse(new OperationResponse(request.OperationRequest.OperationCode),
                                                  new SendParameters());
                        }
                    }
                    else
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Account not found!");
                    }

                    trans.Commit();
                }
            }
        }

        private void HandleSelectCharacter(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new SelectCharacterRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var account = AccountProvider.GetAccount(Id);

                    if (account != null)
                    {
                        var characters =
                            session.QueryOver<Character>()
                                   .Where(n => n.Account == account).List();

                        foreach (var character in characters)
                        {
                            var selection = character.Id == request.Id;
                            if (character.Selected != selection)
                            {
                                character.Selected = selection;
                                session.Update(character);
                            }
                        }

                        SendOperationResponse(new OperationResponse(request.OperationRequest.OperationCode),
                                              new SendParameters());
                    }
                    else
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Account not found!");
                    }

                    trans.Commit();
                }
            }
        }

        private void HandleDeleteCharacter(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new DeleteCharacterRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var account = AccountProvider.GetAccount(Id);

                    if (account != null)
                    {
                        var character =
                            session.QueryOver<Character>()
                                   .Where(n => n.Id == request.Id && n.Account == account)
                                   .SingleOrDefault();

                        if (character != null)
                        {
                            session.Delete(character);

                            Log.InfoFormat("Remove characted " + character.Name);

                            SendOperationResponse(new OperationResponse(request.OperationRequest.OperationCode),
                                                  new SendParameters());
                        }
                        else
                        {
                            SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Character not found!");
                        }

                    }
                    else
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Account not found!");
                    }

                    trans.Commit();
                }
            }
        }

        private void HandleByeCharacter(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new DeleteCharacterRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var account = AccountProvider.GetAccount(Id);

                    if (account != null)
                    {
                        var character =
                            session.QueryOver<Character>()
                                   .Where(n => n.Id == request.Id && n.Account == account)
                                   .SingleOrDefault();

                        if (character != null)
                        {
                            if (character.Hp <= 0f && character.DieTime > DateTime.Now)
                            {
                                if (character.Money >= GameConfig.ByeCharacterPrice)
                                {
                                    character.Money -= GameConfig.ByeCharacterPrice;

                                    character.DieTime = DateTime.Now.AddSeconds(-1);

                                    session.Update(character);

                                    SendOperationResponse(new OperationResponse(request.OperationRequest.OperationCode),
                                                          sendParameters);
                                }
                                else
                                {
                                    SendOperationInvalid(operationRequest, ReturnCode.DontHaveEnoughtMoney, "Not enougth money!");
                                }
                            }
                            else
                            {
                                SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Character not found!");
                            }
                        }
                        else
                        {
                            SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Character not found!");
                        }

                    }
                    else
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Account not found!");
                    }

                    trans.Commit();
                }
            }
        }

        private void HandleLoadLevel(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new LoadLevelRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            using (var session = NHibernateHelper.OpenSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    var account = AccountProvider.GetAccount(Id);

                    if (account != null)
                    {
                        var character =
                            session.QueryOver<Character>()
                                   .Where(n => n.Account == account && n.Selected)
                                   .SingleOrDefault();

                        if (character != null)
                        {
                            _characterData = new CharacterData(character);

                            var response = new LoadLevelResponse
                                {
                                    Position = _characterData.Avatar.Position.ToArray(),
                                    RotY = _characterData.Avatar.RotY,
                                    CharacterParameters = new[]
                                        {
                                            _characterData.Hp,
                                            _characterData.Hunger,
                                            _characterData.Drought,
                                            _characterData.Tiredness
                                        },
                                    StaticData = "",
                                    AvatarId = _characterData.Avatar.Id,
                                    Skills = _characterData.GetLearnedSkills()
                                };

                            Log.InfoFormat("User {0} joined to world {1}", SocialId, World.Id);

                            SendOperationResponse(
                                new OperationResponse(request.OperationRequest.OperationCode, response),
                                new SendParameters());

                            SendInventoryItems();
                            SendEquippedItems();
                            SendMoney();
                        }
                        else
                        {
                            SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Character not found!");
                        }
                    }
                    else
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.OperationInvalid, "Account not found!");
                    }

                    trans.Commit();
                }
            }
        }

        //inventory
        private void ItemUseHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var pars = operationRequest.Parameters;
            if (!pars.ContainsKey((byte)GameParameterKeys.ItemId))
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var itemId = (long)pars[(byte)GameParameterKeys.ItemId];

            var gameItem = _characterData.ItemById(itemId);
            if (gameItem == null)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist in inventory {1}", itemId, _characterData.Name);
                }
                return;
            }

            if (gameItem.Count == 0)
            {
                _characterData.RemoveItemById(gameItem.Id);
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist in inventory {1}", itemId, _characterData.Name);
                }
                return;
            }

            var itemData = gameItem.Data;

            if (!itemData.CanUse)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} cannot use for character {1}", itemId, _characterData.Name);
                }
                return;
            }

            //use it
            _characterData.ChangeItemCount(gameItem, -1);

            _characterData.AdjHurt(itemData.RegenHealth);
            _characterData.AdjEat(itemData.RegenHunger);
            _characterData.AdjWater(itemData.RegenDrought);
            _characterData.AdjTiredness(itemData.RegenTiredness);

            SendInventoryItems();
        }

        private void ItemUnequipHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var pars = operationRequest.Parameters;
            if (!pars.ContainsKey((byte)GameParameterKeys.ItemId))
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var itemId = (long)pars[(byte)GameParameterKeys.ItemId];

            var gameItem = _characterData.EquipById(itemId);
            if (gameItem == null)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't equipped {1}", itemId, _characterData.Name);
                }
                return;
            }

            var newInventoryItem = _characterData.AddItem(gameItem);
            //если нет места
            if (newInventoryItem == null)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist free space in inventory {1}", itemId, _characterData.Name);
                }
                SendOperationInvalid(operationRequest, ReturnCode.DontHaveFreeSpace);
                return;
            }

            var itemData = gameItem.Data;

            //unequip it
            _characterData.ReplaceEquipItem(null, itemData.ItemSlot);

            SendInventoryItems();
            SendEquippedItems();
            SendPlayerEquippedItems();
        }

        private void ItemEquipHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var pars = operationRequest.Parameters;
            if (!pars.ContainsKey((byte)GameParameterKeys.ItemId))
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var itemId = (long)pars[(byte)GameParameterKeys.ItemId];

            var gameItem = _characterData.ItemById(itemId);
            if (gameItem == null)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist in inventory {1}", itemId, _characterData.Name);
                }
                return;
            }

            if (gameItem.Count == 0)
            {
                _characterData.RemoveItemById(gameItem.Id);
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist in inventory {1}", itemId, _characterData.Name);
                }
                return;
            }

            var itemData = gameItem.Data;

            if (itemData.ItemSlot == ItemSlot.None)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} cannot equip {1}", itemId, _characterData.Name);
                }
                return;
            }

            //equip it
            _characterData.ReplaceInventoryItem(gameItem, null);
            var oldEquippedItem = _characterData.ReplaceEquipItem(gameItem, itemData.ItemSlot);
            if (oldEquippedItem != null)
                _characterData.AddItem(oldEquippedItem);

            SendInventoryItems();
            SendEquippedItems();
            SendPlayerEquippedItems();
        }

        private void ItemPickHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var pars = operationRequest.Parameters;
            if (!pars.ContainsKey((byte)GameParameterKeys.ItemId))
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var itemId = (long)pars[(byte)GameParameterKeys.ItemId];

            GameItem gameItem;

            lock (World.Items)
            {
                var mapObject = World.GetItem(itemId) as PickMapObject;
                if (mapObject == null)
                {
                    //уже подняли
                    return;
                }

                World.RemoveItem(mapObject);

                gameItem = mapObject.GameItem;
            }

            if (gameItem == null)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} is null", itemId);
                }
                return;
            }

            var newInventoryItem = _characterData.AddItem(gameItem);

            //если нет места
            if (newInventoryItem == null)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist free space in inventory {1}", itemId, _characterData.Name);
                }
                SendOperationInvalid(operationRequest, ReturnCode.DontHaveFreeSpace);
                return;
            }

            CheckLearnObj(newInventoryItem.Type);

            SendInventoryItems();
        }

        private void ItemDropInventoryHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var pars = operationRequest.Parameters;
            if (!pars.ContainsKey((byte)GameParameterKeys.ItemId))
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var itemId = (long)pars[(byte)GameParameterKeys.ItemId];

            var gameItem = _characterData.ItemById(itemId) ?? _characterData.EquipById(itemId);

            if (gameItem == null)
            {
                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist in {1}", itemId, _characterData.Name);
                }
                return;
            }

            if (gameItem.Count == 0)
            {
                if (_characterData.ItemById(gameItem.Id) != null)
                    _characterData.RemoveItemById(gameItem.Id);
                else
                    _characterData.ReplaceEquipItem(null, gameItem.Data.ItemSlot);

                if (Log.IsErrorEnabled)
                {
                    Log.ErrorFormat("Item with id {0} doesn't exist in {1}", itemId, _characterData.Name);
                }
                return;
            }

            if (_characterData.ItemById(gameItem.Id) != null)
            {
                _characterData.RemoveItemById(gameItem.Id);
                SendInventoryItems();
            }
            else
            {
                _characterData.ReplaceEquipItem(null, gameItem.Data.ItemSlot);
                SendEquippedItems();
                SendPlayerEquippedItems();
            }

            World.SpawnItem(new PickMapObject(_characterData.Avatar.Position, 0f, gameItem));
        }

        private void LearnSkillHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var skill = (SkillType)((byte)operationRequest.Parameters[(byte)GameParameterKeys.ItemType]);
            if (skill == SkillType.None)
                return;

            if (_characterData.IsLearnedSkill(skill))
                return;

            if (!_characterData.IsPossibleLearnSkill(skill))
                return;

            _characterData.LearnSkill(skill);

            SendOperationResponse(new OperationResponse(operationRequest.OperationCode), sendParameters);

            SendSkills();
        }

        private void CraftItemHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var item = (ObjectsDic)((short)operationRequest.Parameters[(byte)GameParameterKeys.ItemType]);
            if (item == ObjectsDic.None)
                return;

            var craftData = CraftDataCache.GetItem(item);
            var reqData = craftData.GetReqData();

            var instruments = reqData.ReqResources.Where(n => n.Value == 0);
            if (instruments.Any())
            {
                bool haveInstrument = instruments.Any(n => _characterData.EquipByType(n.Key) != null);

                if (!haveInstrument)
                {
                    SendOperationInvalid(operationRequest, ReturnCode.DontHaveRequiredInstrument);
                    return;
                }
            }

            var resources = reqData.ReqResources.Where(n => n.Value != 0);
            foreach (var resource in resources)
            {
                var res = _characterData.GetResource(resource.Key);

                if (res == null || res.Count < resource.Value)
                {
                    SendOperationInvalid(operationRequest, ReturnCode.DontHaveRequiredResources);
                    return;
                }
            }

            foreach (var reqSkill in reqData.ReqSkills)
            {
                if (!_characterData.IsLearnedSkill(reqSkill))
                {
                    SendOperationInvalid(operationRequest, ReturnCode.DontHaveRequiredSkill);
                    return;
                }
            }

            //TODO: check building
            if (reqData.ReqBuild != ObjectsDic.None)
            {
                lock (World.Items)
                {
                    var allBuilds =
                        World.Items.Values.Where(n => n.Type == reqData.ReqBuild)
                             .Cast<BuildingMapObject>()
                             .Where(n => n.IsReady);

                    if (!allBuilds.Any())
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.DontHaveRequiredBuilding);
                        return;
                    }

                    bool founded = false;
                    foreach (var build in allBuilds)
                    {
                        if (Vector3.Distance(build.Position, _characterData.Avatar.Position) < GameConfig.BuildDistanceForCraft)
                        {
                            founded = true;
                            break;
                        }
                    }

                    if (!founded)
                    {
                        SendOperationInvalid(operationRequest, ReturnCode.DontHaveRequiredBuilding);
                        return;
                    }
                }
            }

            var itemData = GameItemDataCache.GetItem(item);
            var newItem = GameItemFactory.Instance.BuildGameItem(item, 1, itemData.MaxDurability);

            foreach (var resource in resources)
            {
                for (int i = 0; i < resource.Value; i++)
                {
                    var res = _characterData.GetResource(resource.Key);
                    if (res == null)
                    {
                        throw new Exception("No res in inventory!");
                    }

                    _characterData.ChangeItemCount(res, -1);
                }
            }

            _characterData.AddItem(newItem);

            SendOperationResponse(new OperationResponse(operationRequest.OperationCode), sendParameters);
            SendInventoryItems();
        }
        //world
        private void PlayerMineHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new PlayerMineRequest(Protocol, operationRequest);

            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var miningObj = World.GetItem(request.ItemId) as MiningMapObject;

            if (miningObj == null)
            {
                Log.ErrorFormat("No found miningObj {0} on world {1}", request.ItemId, World.Id);
                return;
            }

            //HACK
            if (miningObj is AnimalMapObject)
                return;

            if (!miningObj.CheckAccessInstrument(this))
            {
                SendOperationInvalid(operationRequest, ReturnCode.DontHaveRequiredInstrument, "You haven't required instrument!");
                return;
            }

            if (_characterData.IsMaxCount)
            {
                SendOperationInvalid(operationRequest, ReturnCode.DontHaveFreeSpace);
                return;
            }

            var dropItem = miningObj.GetDrop(this);

            var response = new PlayerMineResponse();

            //если что-то добыли
            if (dropItem != null)
            {
                var gameItem = GameItemFactory.Instance.BuildGameItem(dropItem.ItemType,
                                                                      dropItem.Count,
                                                                      GameItemDataCache.GetItem(dropItem.ItemType)
                                                                                       .MaxDurability);

                var addedItem = _characterData.AddItem(gameItem);

                if (addedItem != null)
                {
                    //add item to inventory
                    Log.InfoFormat("Add item {0} x {1} to player {2}", dropItem.ItemType, dropItem.Count,
                                   _characterData.Name);
                    //send response
                    response.ItemType = (short)dropItem.ItemType;
                    response.ItemCount = (byte)dropItem.Count;
                    response.ItemId = addedItem.Id;

                    CheckLearnObj(addedItem.Type);
                }

                if (!miningObj.CanDrop)
                {
                    World.RemoveItem(miningObj);
                }
            }

            //TryCrashUsedItem();

            SendOperationResponse(new OperationResponse(operationRequest.OperationCode, response), new SendParameters());
        }

        private void PlayerAttackHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new PlayerMineRequest(Protocol, operationRequest);

            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var animalObj = World.GetItem(request.ItemId) as AnimalMapObject;

            if (animalObj == null)
            {
                Log.ErrorFormat("No found animalObj {0} on world {1}", request.ItemId, World.Id);
                return;
            }

            if (!animalObj.IsDie)
            {
                var weapon = _characterData.UsedItem;

                //если убил
                if (animalObj.ApplyDmg(weapon.Data.MinDmg))
                {
                    if (Log.IsInfoEnabled)
                    {
                        Log.InfoFormat("Player {0} killed {1}(id:{2})", _characterData.Name, animalObj.Type,
                                       animalObj.Id);
                    }
                }

                var eventData = new EventData((byte)EventCode.PlayerAttack, new Dictionary<byte, object>
                    {
                        {(byte) GameParameterKeys.ItemId, _characterData.Avatar.Id},
                        {(byte) GameParameterKeys.ItemType, animalObj.Id},
                        {(byte) GameParameterKeys.ItemData, animalObj.Hp}
                    });

                _characterData.Avatar.CurrentWorldRegion.Publish(new PlayerAttackMessage(eventData));

                return;
            }
            else if (animalObj.IsNeedRemoveFromMap)
                return;

            if (!animalObj.CheckAccessInstrument(this))
            {
                SendOperationInvalid(operationRequest, ReturnCode.DontHaveRequiredInstrument, "You haven't required instrument!");
                return;
            }

            if (_characterData.IsMaxCount)
            {
                SendOperationInvalid(operationRequest, ReturnCode.DontHaveFreeSpace);
                return;
            }

            var dropItem = animalObj.GetDrop(this);

            var response = new PlayerMineResponse();

            //если что-то добыли
            if (dropItem != null)
            {
                var gameItem = GameItemFactory.Instance.BuildGameItem(dropItem.ItemType,
                                                                      dropItem.Count,
                                                                      GameItemDataCache.GetItem(dropItem.ItemType)
                                                                                       .MaxDurability);

                var addedItem = _characterData.AddItem(gameItem);

                if (addedItem != null)
                {
                    //add item to inventory
                    Log.InfoFormat("Add item {0} x {1} to player {2}", dropItem.ItemType, dropItem.Count,
                                   _characterData.Name);
                    //send response
                    response.ItemType = (short)dropItem.ItemType;
                    response.ItemCount = (byte)dropItem.Count;
                    response.ItemId = addedItem.Id;

                    CheckLearnObj(addedItem.Type);
                }
            }

            SendOperationResponse(new OperationResponse((byte)OperationCode.PlayerMine, response), sendParameters);
        }
        private void LevelLoadedHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            _interestArea = new PlayerInterestArea(this);
            _interestArea.AttachToItem(CharacterData.Avatar);
            _interestArea.UpdateInterestManagement();

            World.JoinMap(this);
            World.SendWorldTime(this);
            World.Chat.SendHistory(this);
            World.Weather.SendWeather(this);
            if (_regenSheduler != null)
            {
                _regenSheduler.Dispose();
            }

            StartRegen();

            //SetStatus(CharacterStatus.Game);
        }

        private void ExitLevelHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new ExitLevelRequest(Protocol, operationRequest);

            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            if (_regenSheduler != null)
            {
                _regenSheduler.Dispose();
                _regenSheduler = null;
            }

            World.LeaveMap(this);

            Log.InfoFormat("User {0} left world {1}", _characterData.CharacterId, World.Id);

            if (_characterData.Avatar != null)
            {
                SaveData();

                _characterData.Avatar.Dispose();
            }

            _characterData = null;

            if (_interestArea != null)
                _interestArea.Dispose();
            _interestArea = null;

            SendOperationResponse(new OperationResponse(request.OperationRequest.OperationCode), sendParameters);
        }

        private void MoveHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new ItemMoveRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            _characterData.Avatar.Position = request.Position.ToVector2();
            _characterData.Avatar.RotY = request.RotY;

            //if (_lastUpdateAvatarIm < DateTime.UtcNow)
            {
                _characterData.Avatar.UpdateInterestManagement();

                //_lastUpdateAvatarIm = DateTime.UtcNow.AddMilliseconds(UpdateAvatarImInterval);
            }

            _characterData.Avatar.CurrentWorldRegion.Publish(_characterData.Avatar.GetPositionUpdateMessage());
        }

        private void ChatMessageHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new ChatMessageRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var message = request.Message;
            if (StringFormater.CheckChatMessage(ref message))
            {
                //check, if it command
                if (message.StartsWith("/"))
                {
                    World.Chat.HandleCommand(message, this);
                }
                else
                {
                    //else, send to all or priv
                    var chatMessage = new ChatSystem.ChatMessage(World.WorldTime, _characterData.Name, _characterData.Group, message);
                    World.Chat.AddMessage(chatMessage);
                    World.Chat.SendMessageToAll(chatMessage);
                }
            }
        }

        private void BuildBuildingHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new BuildBuildingRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var buildingType = (ObjectsDic)request.BuildingType;

            //check access for building this build

            //

            var buildMapObject = new BuildingMapObject(buildingType,
                                                       request.BuildingPos.ToVector2(),
                                                       request.BuildingRot,
                                                       BuildDataCache.GetItem(buildingType)
                                                                     .GetReqData()
                                                                     .ReqResources.ToDictionary(k => k.Key, v => v.Value),
                                                       _characterData.CharacterId);
            World.SpawnItem(buildMapObject);
        }

        private void BuilingHandleHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new BuildingBuildRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var buildingId = request.BuildingId;

            var building = World.GetItem(buildingId) as BuildingMapObject;
            if (building == null)
            {
                Log.ErrorFormat("Building id {0} not contains on world {1}", buildingId, World.Id);
            }
            else
            {
                //first need build
                if (!building.Finished)
                {
                    var instruments = building.RemainResources.Where(n => n.Value == 0);
                    var resources = building.RemainResources.Where(n => n.Value != 0);

                    if (instruments.Any(n => _characterData.GetResource(n.Key) == null))
                    {
                        var response = new OperationResponse(operationRequest.OperationCode, new BuildingBuildResponse
                            {
                                BuildingId = buildingId
                            }) { ReturnCode = (short)ReturnCode.DontHaveRequiredInstrument };

                        SendOperationResponse(response, sendParameters);
                    }
                    else
                    {
                        foreach (var resource in resources)
                        {
                            var res = _characterData.GetResource(resource.Key);
                            byte howManyBuild = 1;
                            if (res != null && res.Count >= howManyBuild)
                            {
                                _characterData.ChangeItemCount(res, -howManyBuild);
                                building.Build(res.Type);

                                SendOperationResponse(
                                    new OperationResponse(operationRequest.OperationCode, new BuildingBuildResponse
                                        {
                                            BuildingId = buildingId,
                                            ResourceCount = howManyBuild,
                                            ResourceType = (short)res.Type,
                                        }), sendParameters);

                                var eventData = new EventData((byte)EventCode.BuildingProccessInfoUpdate,
                                                              new BuildingInfoUpdated
                                                                  {
                                                                      BuildingId = buildingId,
                                                                      BuildingData = building.GetBuildingProcessInfo()
                                                                  });

                                building.CurrentWorldRegion.Publish(new BuildingProccessInfoUpdate(eventData));

                                return;
                            }
                        }

                        var response = new OperationResponse(operationRequest.OperationCode, new BuildingBuildResponse
                            {
                                BuildingId = buildingId
                            }) { ReturnCode = (short)ReturnCode.DontHaveRequiredResources };

                        SendOperationResponse(response, sendParameters);
                    }
                }
                //process this build
                else
                {
                    var response = new OperationResponse(operationRequest.OperationCode, new BuildingBuildResponse
                        {
                            BuildingId = buildingId
                        });

                    SendOperationResponse(response, sendParameters);
                }
            }
        }

        private void DestroyBuildingHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new DestroyBuildingRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            var buildingId = request.BuildingId;

            var building = World.GetItem(buildingId) as BuildingMapObject;
            if (building == null)
            {
                Log.ErrorFormat("Building id {0} not contains on world {1}", buildingId, World.Id);
                return;
            }

            if (building.OwnerId != _characterData.CharacterId)
            {
                SendOperationInvalid(operationRequest, ReturnCode.AccessDenied, "This build not you!");
                return;
            }

            World.RemoveItem(building);

            if (Log.IsInfoEnabled)
            {
                Log.InfoFormat("Player {0} destroy building {1}", _characterData.Name, building.Type);
            }
        }

        private void SpawnItemHandler(OperationRequest operationRequest, SendParameters sendParameters)
        {
            var request = new SpawnItemRequest(Protocol, operationRequest);
            if (!request.IsValid)
            {
                SendOperationInvalid(operationRequest);
                return;
            }

            World.SpawnItem(new MiningMapObject((ObjectsDic)request.Type,
                                                CharacterData.Avatar.Position,
                                                CharacterData.Avatar.RotY,
                                                MiningDataCache.GetItem((ObjectsDic)request.Type)
                                                               .GetDrop()
                                                               .Select(
                                                                   n =>
                                                                   new DropItem
                                                                       {
                                                                           ItemType = n.ItemType,
                                                                           Count = n.MaxCount
                                                                       })
                                                               .ToList()));
        }
        #endregion
    }
}
