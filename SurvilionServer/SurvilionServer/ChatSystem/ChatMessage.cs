﻿using SurvCommon;

namespace SurvGameServer.ChatSystem
{
    public class ChatMessage
    {
        public double Time { get; private set; }
        public string Nick { get; private set; }
        public UserGroup UserGroup { get; private set; }
        public string Message { get; private set; }

        public ChatMessage(double time, string nick, UserGroup group, string message)
        {
            Time = time;
            Nick = nick;
            UserGroup = group;
            Message = message;
        }

        public string GetMessageForSend()
        {
            return (long)Time + GameConfig.ChatDevider + Nick + GameConfig.ChatDevider + Message;
        }
    }
}
