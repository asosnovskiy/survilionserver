﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExitGames.Logging;
using Photon.SocketServer;
using SurvCommon;
using SurvGameServer.WorldSystem;

namespace SurvGameServer.ChatSystem
{
    public class Chat
    {
        protected static readonly ILogger Log = LogManager.GetCurrentClassLogger();

        private const int HistoryMessagesCount = 10;

        private readonly World _world;

        private readonly List<ChatMessage> _messages = new List<ChatMessage>(); 
        public List<ChatMessage> Messages { get { return _messages; }} 

        public Chat(World world)
        {
            _world = world;
        }

        private string[] GetHistory()
        {
            lock (_messages)
            {
                return Messages.Select(n => n.GetMessageForSend()).ToArray();
            }
        }

        public void Clear()
        {
            lock(_messages)
                _messages.Clear();
        }

        public void AddMessage(ChatMessage message)
        {
            lock (_messages)
            {
                _messages.Add(message);

                if (_messages.Count > HistoryMessagesCount)
                {
                    _messages.RemoveAt(0);
                }

                if (Log.IsInfoEnabled)
                {
                    Log.InfoFormat("[CHAT][world:{0}][{1}]{2}: {3}",
                                   _world.Id,
                                   message.Time,
                                   message.Nick,
                                   message.Message);
                }
            }
        }

        public void SendMessage(ChatMessage message, Player player)
        {
            var eventData = new EventData((byte)EventCode.ChatMessage, new Events.ChatMessage
            {
                Message = message.GetMessageForSend()
            });

            _world.Send(player, eventData, false);
        }
        
        //to all
        public void SendMessageToAll(ChatMessage message)
        {
            var eventData = new EventData((byte) EventCode.ChatMessage, new Events.ChatMessage
                {
                    Message = message.GetMessageForSend()
                });

            _world.SendToAll(eventData, false);
        }

        public void SendHistory(Player player)
        {
            var eventData = new EventData((byte) EventCode.ChatHistory, new Events.ChatHistory
                {
                    Messages = GetHistory()
                });

            _world.Send(player, eventData, false);
        }
        
        public bool HandleCommand(string message, Player player)
        {
            if (!message.StartsWith("/"))
                return false;

            message = message.Remove(0, 1);

            var mas = message.Split(new [] {' '}, StringSplitOptions.RemoveEmptyEntries);
            var command = mas[0];

            switch (command)
            {
                case "weather":
                    var newWeather = (WeatherType)int.Parse(mas[1]);
                    _world.Weather.GenerateWeather(newWeather);
                    _world.Weather.SendWeather();
                    break;
            }

            return true;
        }
    }
}