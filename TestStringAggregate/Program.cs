﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace TestStringAggregate
{
    class Program
    {
        static void Main(string[] args)
        {
            var world = new World();
            world.Run();

            Console.ReadKey();
        }
    }

    public struct Rectangle
    {
        public static readonly Rectangle Zero = new Rectangle(0, 0, 0, 0);

        public float X;
        public float Y;
        public float Width;
        public float Height;

        public Rectangle(float x, float y, float width, float height)
        {
            X = x;
            Y = y;
            Width = width;
            Height = height;
        }

        public Rectangle(float x,float y,float size):this(x,y,size,size){}
        public Rectangle(float x,float y):this(x,y,0f,0f){}

        public bool Intersact(Vector3 pos)
        {
            return pos.X >= X && pos.X <= X + Width && pos.Y >= Y && pos.Y <= Y + Height;
        }
    }

    public struct Vector3
    {
        public float X;
        public float Y;
        public float Z;

        public Vector3(float x, float y, float z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Vector3(float x, float y)
            : this(x, y, 0f)
        {
        }

        //public double Distance(Vector3 pos)
        //{
        //    return Math.Sqrt(Math.Pow(pos.X - X, 2) + Math.Pow(pos.Y - Y, 2));
        //}

        public static double Distance(Vector3 pos1,Vector3 pos2)
        {
            return Approximate.Sqrt((pos1.X - pos2.X * (pos1.X - pos2.X) + (pos1.Y - pos2.Y) * (pos1.Y - pos2.Y)));
        }
    }
}
