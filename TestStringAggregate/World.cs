﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestStringAggregate
{

    public class World
    {
        private const float MAX_WORLD_SIZE = 2000f;
        private const int GRID_COUNT = 100; //X x X (10x10)
        private Region[][] _grid;

        public readonly Dictionary<long, MapObject> MapObjects = new Dictionary<long, MapObject>(CountObjects);
        private readonly List<MapObject> Players = new List<MapObject>(10000); 

        private const int CountObjects = 1000000;
        private const float ViewDistance = 2f;

        private readonly static Random _rand = new Random();

        private static int counter;

        private readonly float _sizeGrid;

        public World()
        {
            _sizeGrid = (MAX_WORLD_SIZE / GRID_COUNT);

            _grid = new Region[GRID_COUNT][];
            for (int i = 0; i < GRID_COUNT; i++)
                _grid[i] = new Region[GRID_COUNT];

            for (int i = 0; i < GRID_COUNT; i++)
                for (int j = 0; j < GRID_COUNT; j++)
                {
                    _grid[i][j] = new Region(new Rectangle(i * _sizeGrid, j * _sizeGrid, _sizeGrid));
                }

            for (int i = 1; i <= CountObjects; i++)
            {
                var mapObject = new MapObject(i, new Vector3((float)_rand.NextDouble() * (MAX_WORLD_SIZE - 1), (float)_rand.NextDouble() * (MAX_WORLD_SIZE - 1)));
                MapObjects.Add(mapObject.Id, mapObject);

                var region = GetIntersactRegion(mapObject.Position);
                region.AddObject(mapObject);
                mapObject.Region = region;

                if (i <= 10001)
                    Players.Add(mapObject);
            }

            Console.WriteLine("init " + MapObjects.Count + " objects!");
            Console.WriteLine("Regions count: " + GRID_COUNT * GRID_COUNT);
            Console.WriteLine("gridSize:" + _sizeGrid + "km");

            var col = new List<int>(GRID_COUNT * GRID_COUNT);
            foreach (var result in _grid.Select(n => n.Select(n2 => n2.Objects.Count)))
            {
                col.AddRange(result);
            }

            col = col.OrderByDescending(n => n).Take(5).ToList();
            foreach (var num in col)
            {
                Console.WriteLine("region objs count:" + num);
            }
        }

        public void Run()
        {
            Console.WriteLine("Start test...");

            ThreadPool.QueueUserWorkItem(n =>
            {
                var objects = MapObjects.Values;
                while (true)
                {
                    Console.WriteLine("Step:" + counter++);
                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    //foreach (var mapObject in objects)
                    Parallel.ForEach(objects, mapObject =>
                        {
                            mapObject.OtherObjects.Clear();
                            var pos = mapObject.Position;

                            foreach (var obj in mapObject.Region.Objects)
                            {
                                if (obj == mapObject || (!mapObject.IsPlayer && !obj.IsPlayer))
                                    continue;

                                if (Vector3.Distance(pos, obj.Position) < ViewDistance)
                                {
                                    mapObject.OtherObjects.Add(obj);
                                }
                            }
                        });

                    foreach (var mapObject in Players)
                    {
                        mapObject.Position = new Vector3((float)_rand.NextDouble() * (MAX_WORLD_SIZE - 1), (float)_rand.NextDouble() * (MAX_WORLD_SIZE - 1));
                        var region = GetIntersactRegion(mapObject.Position);
                        if (region != mapObject.Region)
                        {
                            mapObject.Region.Objects.Remove(mapObject);
                            mapObject.Region = region;
                            region.AddObject(mapObject);
                        }
                    }

                    stopwatch.Stop();

                    Console.WriteLine("stopwatch time:" + stopwatch.ElapsedTicks + "ticks, " + stopwatch.ElapsedMilliseconds + "ms");
                    Thread.Sleep(1000);
                }
            });
        }

        public Region GetIntersactRegion(Vector3 pos)
        {
            var x = (int)Math.Floor(pos.X / _sizeGrid);
            var y = (int)Math.Floor(pos.Y / _sizeGrid);

            if (x >= GRID_COUNT || x < 0 || y >= GRID_COUNT || y < 0)
                return null;

            return _grid[x][y];
        }
    }

    public class Approximate
    {
        public static float Sqrt(float z)
        {
            if (z == 0) return 0;
            FloatIntUnion u;
            u.tmp = 0;
            u.f = z;
            u.tmp -= 1 << 23; /* Subtract 2^m. */
            u.tmp >>= 1; /* Divide by 2. */
            u.tmp += 1 << 29; /* Add ((b + 1) / 2) * 2^m. */
            return u.f;
        }

        [StructLayout(LayoutKind.Explicit)]
        private struct FloatIntUnion
        {
            [FieldOffset(0)]
            public float f;

            [FieldOffset(0)]
            public int tmp;
        }
    }

}
