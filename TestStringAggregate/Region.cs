﻿using System.Collections.Generic;

namespace TestStringAggregate
{
    public class Region
    {
        public Rectangle Rectangle;

        public readonly List<MapObject> Objects = new List<MapObject>();

        public Region(Rectangle rect)
        {
            Rectangle = rect;
        }

        public void ClearList()
        {
            Objects.Clear();
        }

        public void AddObject(MapObject obj)
        {
            Objects.Add(obj);
        }
    }
}
