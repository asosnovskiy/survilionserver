﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestStringAggregate
{

    public class MapObject
    {
        public long Id { get; set; }
        public short Type { get; private set; }
        public Vector3 Position { get; set; }

        public readonly List<MapObject> OtherObjects = new List<MapObject>(50);

        public Region Region { get; set; }

        public bool IsPlayer { get { return Id <= 10001; } }

        public MapObject(long id, Vector3 pos)
        {
            Id = id;
            Position = pos;
        }
    }

}
